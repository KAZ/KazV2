#!/bin/bash

# Attention à cause des scripts pas de ["'/] dans les mot de passe

####################
# ethercalc
ethercalc_REDIS_PORT_6379_TCP_ADDR="redis"
ethercalc_REDIS_PORT_6379_TCP_PORT="6379"

####################
# etherpad
etherpad_MYSQL_ROOT_PASSWORD="--clean_val--"
etherpad_MYSQL_DATABASE="--clean_val--"
etherpad_MYSQL_USER="--clean_val--"
etherpad_MYSQL_PASSWORD="--clean_val--"

# Share with etherpadDB
etherpad_DB_NAME="${etherpad_MYSQL_DATABASE}"
etherpad_DB_USER="${etherpad_MYSQL_USER}"
etherpad_DB_PASS="${etherpad_MYSQL_PASSWORD}"

etherpad_DB_TYPE="mysql"
etherpad_DB_HOST="padDB"
etherpad_DB_PORT="3306"
#etherpad_DB_CHARSET="utf8"
#user: admin
etherpad_ADMIN_PASSWORD="--clean_val--"
etherpad_PAD_OPTIONS_LANG="fr"
etherpad_TITLE="KazPad"
etherpad_TRUST_PROXY="true"

####################
# framadate
framadate_MYSQL_ROOT_PASSWORD="--clean_val--"
framadate_MYSQL_DATABASE="--clean_val--"
framadate_MYSQL_USER="--clean_val--"
framadate_MYSQL_PASSWORD="--clean_val--"

framadate_HTTPD_USER="--clean_val--"
framadate_HTTPD_PASSWORD="--clean_val--"

##################
# Gandi
# à supprimer et à replacer par dns_gandi_api_key
gandi_GANDI_KEY="xxx"
gandi_GANDI_API="https://api.gandi.net/v5/livedns/domains/${domain}"
gandi_dns_gandi_api_key="${gandi_GANDI_KEY}"

####################
# mattermost
mattermost_MYSQL_ROOT_PASSWORD="--clean_val--"
mattermost_MYSQL_DATABASE="--clean_val--"
mattermost_MYSQL_USER="--clean_val--"
mattermost_MYSQL_PASSWORD="--clean_val--"

# Share with mattermostDB
mattermost_MM_DBNAME="${mattermost_MYSQL_DATABASE}"
mattermost_MM_USERNAME="${mattermost_MYSQL_USER}"
mattermost_MM_PASSWORD="${mattermost_MYSQL_PASSWORD}"

mattermost_DB_PORT_NUMBER="3306"
mattermost_DB_HOST="db"
mattermost_MM_SQLSETTINGS_DRIVERNAME="mysql"
mattermost_MM_ADMIN_EMAIL="admin@kaz.bzh"

# mattermost_MM_SQLSETTINGS_DATASOURCE = "MM_USERNAME:MM_PASSWORD@tcp(DB_HOST:DB_PORT_NUMBER)/MM_DBNAME?charset=utf8mb4,utf8&readTimeout=30s&writeTimeout=30s"
# Don't forget to replace all entries (beginning by MM_ and DB_) in MM_SQLSETTINGS_DATASOURCE with the real variables values.
mattermost_MM_SQLSETTINGS_DATASOURCE="${mattermost_MYSQL_USER}:${mattermost_MYSQL_PASSWORD}@tcp(${mattermost_DB_HOST}:${mattermost_DB_PORT_NUMBER})/${mattermost_MM_DBNAME}?charset=utf8mb4,utf8&readTimeout=30s&writeTimeout=30s"
# sinon avec postgres
# mattermost_MM_SQLSETTINGS_DATASOURCE = "postgres://${MM_USERNAME}:${MM_PASSWORD}@db:5432/${MM_DBNAME}?sslmode=disable&connect_timeout=10"

# pour envoyer des messages sur l'agora avec mmctl
mattermost_user="admin-mattermost"
mattermost_pass="--clean_val--"
mattermost_token="xxx-private"

##################
# Openldap
ldap_LDAP_ADMIN_USERNAME="--clean_val--"
ldap_LDAP_ADMIN_PASSWORD="--clean_val--"
ldap_LDAP_CONFIG_ADMIN_USERNAME="--clean_val--"
ldap_LDAP_CONFIG_ADMIN_PASSWORD="--clean_val--"
ldap_LDAP_POSTFIX_PASSWORD="--clean_val--"
ldap_LDAP_LDAPUI_PASSWORD="--clean_val--"
ldap_LDAP_MATTERMOST_PASSWORD="--clean_val--"
ldap_LDAP_CLOUD_PASSWORD="--clean_val--"
ldap_LDAP_MOBILIZON_PASSWORD="--clean_val--"

ldap_LDAPUI_URI=ldap://ldap
ldap_LDAPUI_BASE_DN=${ldap_root}
ldap_LDAPUI_REQUIRE_STARTTLS=FALSE
ldap_LDAPUI_ADMINS_GROUP=admins
ldap_LDAPUI_ADMIN_BIND_DN=cn=ldapui,ou=applications,${ldap_root}
ldap_LDAPUI_ADMIN_BIND_PWD=${ldap_LDAP_LDAPUI_PASSWORD}
ldap_LDAPUI_IGNORE_CERT_ERRORS=TRUE
ldap_LDAPUI_PASSWORD="--clean_val--"
ldap_LDAPUI_MM_ADMIN_TOKEN=${mattermost_token}

###################
# gitea
gitea_MYSQL_ROOT_PASSWORD="--clean_val--"
gitea_MYSQL_DATABASE="--clean_val--"
gitea_MYSQL_USER="--clean_val--"
gitea_MYSQL_PASSWORD="--clean_val--"

# on ne peut pas utiliser le login "admin"
gitea_user_admin="admin_gitea"
gitea_pass_admin="--clean_val--"
gitea_admin_email="admin@kaz.bzh"

####################
# jirafeau
jirafeau_HTTPD_PASSWORD="--clean_val--"
jirafeau_DATA_DIR="--clean_val--"


####################
# nexcloud
nextcloud_MYSQL_ROOT_PASSWORD="${mattermost_MYSQL_ROOT_PASSWORD}"
nextcloud_MYSQL_DATABASE="--clean_val--"
nextcloud_MYSQL_USER="--clean_val--"
nextcloud_MYSQL_PASSWORD="--clean_val--"

nextcloud_NEXTCLOUD_ADMIN_USER="admin"
nextcloud_NEXTCLOUD_ADMIN_PASSWORD="--clean_val--"
nextcloud_MYSQL_HOST="db"

#user: admin
nextcloud_RAIN_LOOP="--clean_val--"

####################
# collabora
office_username="admin"
office_password="--clean_val--"

####################
# roundcube
roundcube_MYSQL_ROOT_PASSWORD="--clean_val--"
roundcube_MYSQL_DATABASE="--clean_val--"
roundcube_MYSQL_USER="--clean_val--"
roundcube_MYSQL_PASSWORD="--clean_val--"

# Share with roundcubeDB
roundcube_ROUNDCUBEMAIL_DB_TYPE="mysql"
roundcube_ROUNDCUBEMAIL_DB_NAME="${roundcube_MYSQL_DATABASE}"
roundcube_ROUNDCUBEMAIL_DB_USER="${roundcube_MYSQL_USER}"
roundcube_ROUNDCUBEMAIL_DB_PASSWORD="${roundcube_MYSQL_PASSWORD}"
roundcube_ROUNDCUBEMAIL_UPLOAD_MAX_FILESIZE="1G"

####################
# postfix LDAP
mail_LDAP_BIND_DN=cn=postfix,ou=applications,${ldap_root}
mail_LDAP_BIND_PW=${ldap_LDAP_POSTFIX_PASSWORD}

####################
# sympa
sympa_MYSQL_ROOT_PASSWORD="--clean_val--"
sympa_MYSQL_DATABASE="sympa"
sympa_MYSQL_USER="sympa"
sympa_MYSQL_PASSWORD="--clean_val--"

sympa_KEY="/etc/letsencrypt/live/${domain}/privkey.pem"
sympa_CERT="/etc/letsencrypt/live/${domain}/fullchain.pem"
sympa_LISTMASTERS="listmaster@${domain_sympa}"
sympa_ADMINEMAIL="listmaster@${domain_sympa}"
sympa_SOAP_USER="sympa"
sympa_SOAP_PASSWORD="--clean_val--"

# pour inscrire des users sur des listes sympa avec soap
#il faut que le user soit admin de sympa
sympa_user="a@${domain}"
sympa_pass="--clean_val--"

##################
# vigilo
vigilo_MYSQL_ROOT_PASSWORD="--clean_val--"
vigilo_MYSQL_USER="--clean_val--"
vigilo_MYSQL_PASSWORD="--clean_val--"
vigilo_MYSQL_DATABASE="--clean_val--"
vigilo_MYSQL_HOST="db"
#vigilo_BIND=

####################
# wordpress
wp_MYSQL_ROOT_PASSWORD="${mattermost_MYSQL_ROOT_PASSWORD}"
wp_MYSQL_DATABASE="--clean_val--"
wp_MYSQL_USER="--clean_val--"
wp_MYSQL_PASSWORD="--clean_val--"

# Share with wpDB
wp_WORDPRESS_DB_HOST="db:3306"
wp_WORDPRESS_DB_NAME="${wp_MYSQL_DATABASE}"
wp_WORDPRESS_DB_USER="${wp_MYSQL_USER}"
wp_WORDPRESS_DB_PASSWORD="${wp_MYSQL_PASSWORD}"

wp_WORDPRESS_ADMIN_USER="admin"
wp_WORDPRESS_ADMIN_PASSWORD="--clean_val--"

##################
#qui envoi le mail d'inscription ?
EMAIL_CONTACT="toto@kaz.bzh"


##################
# Paheko
paheko_API_USER="admin-api"
paheko_API_PASSWORD="--clean_val--"

##################
# La nas de Kaz chez Grifon
nas_admin1="admin"
nas_password1="--clean_val--"
nas_admin2="kaz"
nas_password1="--clean_val--"
# compte mail pour les notifications du nas
nas_email_account="admin-nas@${domain}"
nas_email_password="--clean_val--"

##################
#Compte sur outlook.com
outlook_user="kaz-user@outlook.fr"
outlook_pass="--clean_val--"

##################
#Borg
BORG_REPO="/mnt/backup-nas1/BorgRepo"
BORG_PASSPHRASE="--clean_val--"
VOLUME_SAUVEGARDES="/mnt/backup-nas1"
MAIL_RAPPORT="a@${domain};b@${domain};c@${domain}"
BORGMOUNT="/mnt/disk-nas1/tmp/repo_mount"


###################
# mobilizon
mobilizon_POSTGRES_USER="--clean_val--"
mobilizon_POSTGRES_PASSWORD="--clean_val--"
mobilizon_POSTGRES_DB=mobilizon
mobilizon_MOBILIZON_DATABASE_USERNAME="${mobilizon_POSTGRES_USER}"
mobilizon_MOBILIZON_DATABASE_PASSWORD="${mobilizon_POSTGRES_PASSWORD}"
mobilizon_MOBILIZON_DATABASE_DBNAME=mobilizon

mobilizon_MOBILIZON_INSTANCE_REGISTRATIONS_OPEN=false
mobilizon_MOBILIZON_INSTANCE_NAME="Mobilizon"
mobilizon_MOBILIZON_INSTANCE_HOST="${mobilizonHost}.${domain}"

mobilizon_MOBILIZON_INSTANCE_SECRET_KEY_BASE=changeme
mobilizon_MOBILIZON_INSTANCE_SECRET_KEY=changeme

mobilizon_MOBILIZON_INSTANCE_EMAIL=noreply@${domain}
mobilizon_MOBILIZON_REPLY_EMAIL=contact@${domain_sympa}
mobilizon_MOBILIZON_ADMIN_EMAIL=admin@${domain_sympa}

mobilizon_MOBILIZON_SMTP_SERVER="${smtpHost}.${domain}"
mobilizon_MOBILIZON_SMTP_PORT=25
mobilizon_MOBILIZON_SMTP_HOSTNAME="${smtpHost}.${domain}"
mobilizon_MOBILIZON_SMTP_USERNAME=noreply@${domain}
mobilizon_MOBILIZON_SMTP_PASSWORD=
mobilizon_MOBILIZON_SMTP_SSL=false

mobilizon_MOBILIZON_LDAP_BINDUID=cn=mobilizon,ou=applications,${ldap_root}
mobilizon_MOBILIZON_LDAP_BINDPASSWORD=${ldap_LDAP_MOBILIZON_PASSWORD}


#####################
# Vaultwarden

vaultwarden_MYSQL_ROOT_PASSWORD="--clean_val--"
vaultwarden_MYSQL_DATABASE="vaultwarden"
vaultwarden_MYSQL_USER="vaultwarden"
vaultwarden_MYSQL_PASSWORD="--clean_val--"

vaultwarden_DATABASE_URL="mysql://${vaultwarden_MYSQL_USER}:${vaultwarden_MYSQL_PASSWORD}@db/${vaultwarden_MYSQL_DATABASE}"
vaultwarden_ADMIN_TOKEN="--clean_val--"

#####################
#Traefik

traefik_DASHBOARD_USER="admin"
traefik_DASHBOARD_PASSWORD="--clean_val--"


#####################
# dokuwiki

dokuwiki_WIKI_ROOT=Kaz
dokuwiki_WIKI_EMAIL=wiki@kaz.local
dokuwiki_WIKI_PASSWORD="--clean_val--"

#####################
# Castopod
castopod_MYSQL_ROOT_PASSWORD="--clean_val--"
castopod_MYSQL_DATABASE="--clean_val--"
castopod_MYSQL_USER="--clean_val--"
castopod_MYSQL_PASSWORD="--clean_val--"
castopod_CP_REDIS_PASSWORD="${castopodRedisPassword}"
castopod_ADMIN_USER=adminKaz
castopod_ADMIN_MAIL=admin@${domain}
castopod_ADMIN_PASSWORD="--clean_val--"
castopod_CP_EMAIL_SMTP_HOST="${smtpHost}.${domain}"
castopod_CP_EMAIL_SMTP_PORT=25
castopod_CP_EMAIL_SMTP_USERNAME=noreply@${domain}
castopod_CP_EMAIL_SMTP_PASSWORD=
castopod_CP_EMAIL_FROM=noreply@${domain}
castopod_CP_EMAIL_SMTP_CRYPTO=tls

 ######################
# SNAPPYMAIL
# Url  https://snappymail.${domain}/?admin
# au premier lancement un mot de passe est généré en aut par l' appli dans le
# volume Data : /var/lib/docker/volumes/snappymail_data/_data/_data_/_default_
# le fichier s' appelle admin_password.txt
# une fois le mot de passe changé dans le Gui de l' admin, ce fichier est automatiquement supprimé
snappymail_TZ="Europe/Paris"
snappymail_UPLOAD_MAX_SIZE="100M"

