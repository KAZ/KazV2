Mise à jour des mots de passe

L'idée c'est d'extraire la gestion des mots de passe de l'installation.

Tous les mots de passe sont dans un fichier "SetAllPass.sh" que des scripts vont chercher.

updateDockerPassword.sh met à jours les fichiers d'environnement de mots de passe utilisé par docker-compose.

(Il y a un problème pour mettre à jour le mot de passe d'une BD si son conteneur n'est pas en route)

Les modifications sont prises en compte que lors de la création de nouveaux conteneurs (les données permanentes (mot de passe) dans les volumes ne sont pas changées)
