# kaz

[Kaz](https://kaz.bzh/) est un CHATONS du Morbihan. Nous proposons ici un moyen de le répliquer dans d'autres lieux. Il y a des éléments de configuration à définir avant d'initialiser ce simulateur.


Il est possible de simuler notre CHATONS dans une VirtualBox pour mettre au point nos différents services (voir [kaz-vagrant](https://git.kaz.bzh/KAZ/kaz-vagrant)).

## Pré-requis

Dans la suite, on imagine que vous disposer d'une machine sous Linux (par exemple dernière version de [Debian](https://fr.wikipedia.org/wiki/Debian)) avec un minimum de paquets.

```bash
DEBIAN_FRONTEND=noninteractive apt-get -y upgrade
DEBIAN_FRONTEND=noninteractive apt-get -y dist-upgrade
DEBIAN_FRONTEND=noninteractive apt-get install -y apg curl git unzip rsync net-tools libnss3-tools
DEBIAN_FRONTEND=noninteractive apt-get remove --purge -y exim4-base exim4-config exim4-daemon-light
```

## Installation

* Télécharger le dépôt kaz-vagrant ou utilisez la commande :
```bash
git clone https://git.kaz.bzh/KAZ/kaz.git # pour essayer
git clone git+ssh://git@git.kaz.bzh:2202/KAZ/kaz.git # pour contribuer
cd kaz/
```
* Personalisez votre simulateur avec la commande (au besoin ajustez la mémoire et les cpus utilisés dans Vagrantfile) :
```bash
./bin/init.sh
```

Pour terminer la création et tous les dockers, il faut lancer la commande
```bash
./bin/install.sh web jirafeau ethercalc etherpad postfix roundcube proxy framadate paheko dokuwiki > >(tee stdout.log) 2> >(tee stderr.log >&2)
```

* Une fois la commande de création de l'univers, il faut patienter...

* Après il faut patienter encore un peu si on a pas la fibre ...

## Utilisation
