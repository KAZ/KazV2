#!/bin/bash

KAZ_ROOT=$(cd "$(dirname $0)/../.."; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars
. "${DOCKERS_ENV}"
. "${KAZ_ROOT}/secret/SetAllPass.sh"

printKazMsg "\n  *** Proxy update config"

DOCKER_TMPL=docker-compose.tmpl.yml
DOCKER_DIST=docker-compose.tmpl.yml.dist
DOCKER_CONF=docker-compose.yml
PASSFILE=conf/passfile

cd $(dirname $0)

[[ -f "${DOCKER_TMPL}" ]] || cp "${DOCKER_DIST}" "${DOCKER_TMPL}"
[[ -f "${PASSFILE}" ]] || printf "${traefik_DASHBOARD_USER}:$( echo ${traefik_DASHBOARD_PASSWORD} | openssl passwd -apr1 -stdin)\n" >> ${PASSFILE}
"${APPLY_TMPL}" -time "${DOCKER_TMPL}" "${DOCKER_CONF}"
