#!/bin/bash

SERV_DIR=$(cd $(dirname $0); pwd)
KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

cd $(dirname $0)
. "${DOCKERS_ENV}"


printKazMsg "\n  *** Premier lancement de Traefik : Mise en place"

[[ -f "conf/allow_ip.yml" ]] || cp "conf/allow_ip.yml.dist" "conf/allow_ip.yml"
cp /etc/letsencrypt/live/${domain}/{fullchain.pem,privkey.pem} conf/
[[ -f "conf/cert.yml" ]] || cp "conf/cert.yml.dist" "conf/cert.yml"