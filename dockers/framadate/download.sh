#!/bin/bash

SERV_DIR=$(cd $(dirname $0); pwd)
KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

SRC_FRAMADATE="https://github.com/framasoft/framadate.git"
FRAMDATE_VER="1.1.19"

printKazMsg "\n  *** Download framadate"

mkdir -p "${KAZ_GIT_DIR}"
cd "${KAZ_GIT_DIR}"
if [ ! -d "framadate" ]; then
    git clone "${SRC_FRAMADATE}" --branch ${FRAMDATE_VER}
fi

cd "${KAZ_GIT_DIR}/framadate"
if [ -z "$(git branch | grep "${FRAMDATE_VER}")" ]; then
    printKazMsg "  checkout branch ${FRAMDATE_VER}"
    git reset --hard
    git checkout ${FRAMADATE_VER}
fi
