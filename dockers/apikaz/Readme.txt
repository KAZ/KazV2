yo, ceci est l'api de kaz !

https://apikaz.DEV/

Je pars de ça: python api + docker-compose: https://dev.to/alissonzampietro/the-amazing-journey-of-docker-compose-17lj

Pour la doc:
module Flask + flask-restful + flasgger
https://stackoverflow.com/questions/75840827/how-to-properly-generate-a-documentation-with-swagger-for-flask
https://medium.com/@DanKaranja/building-api-documentation-in-flask-with-swagger-a-step-by-step-guide-59a453509e2f
https://apidog.com/blog/what-is-flasgger/

autre piste: abandonnée pour l'instant. trop jeune ?
Documentation (OpenApi remplace swagger)
https://pypi.org/project/flask-openapi3/

autre piste ? https://github.com/fastapi/fastapi (mais y a du node :( )

TODO: 
* sécurisation de l'API : un token ? otp ?
* sécurité: comment différencier les rôles admin (tout faire) et kaznaute (changer uniquement ses coordonnées, relancer son site propre web, ...)
* pourquoi pas une seule classe pour plusieurs api ? (par exemple LDAP, pas mal de factorisation possible)
* pourquoi pas de donnée dans le body ? (au lieu des variables dans l'url). par ex, pour le message du mail.
