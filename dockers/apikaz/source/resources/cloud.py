 
from resources.common_imports import *

#les variables globales minimum
from resources.config import cloud_ident, cloud_pass, cloud_url
 

#TODO: pas réussi à faire une seule classe Cloud_user avec 2 méthodes get/delete

class Cloud_user(Resource):
  
    @jwt_required()
    def get(self, email):
        """
        Existe dans le cloud général ?
        ---
        tags:
          - Cloud
        security:
          - Bearer: []                
        parameters:
          - in: path
            name: email
            type: string
            required: true
        responses:
            200:
                description: Succès
            404:
                description: L'utilisateur n'existe pas
            500:
                description: Erreur interne du serveur
        """    

        global cloud_ident, cloud_pass, cloud_url

        try:
            auth = (cloud_ident, cloud_pass)
            api_url = f"{cloud_url}/ocs/v1.php/cloud/users?search={email}"
            headers = {"OCS-APIRequest": "true"}
            response = requests.get(api_url, auth=auth, headers=headers)

            if response.status_code == 200:
              if re.search(r'<element>.*</element>', response.text):
                return 200
              else:
                return 404

        except Exception as e:
            return jsonify({'error': str(e)}), 500
          
    
#*************************************************
class Cloud_user_delete(Resource):
  
    @jwt_required()
    def delete(self, email):
        """
        Supprime le compte dans le cloud général
        QUESTION: A PRIORI INUTILE CAR LIE AU LDAP
        ---
        tags:
          - Cloud
        security:
          - Bearer: []                
        parameters:
          - in: path
            name: email
            type: string
            required: true
        responses:
            200:
                description: Succès, l'utilisateur a été supprimé du cloud général
            404:
                description: Oops, l'utilisateur n'a pas été supprimé du cloud général
            500:
                description: Erreur interne du serveur
        """    

        global cloud_ident, cloud_pass, cloud_url

        try:
            auth = (cloud_ident, cloud_pass)
            api_url = f"{cloud_url}/ocs/v1.php/cloud/users?search={email}"
            headers = {"OCS-APIRequest": "true"}
            response = requests.delete(api_url, auth=auth, headers=headers)

            if response.status_code == 200:
              if re.search(r'<element>.*</element>', response.text):
                return 200
              else:
                return 404

        except Exception as e:
            return jsonify({'error': str(e)}), 500
          

#*************************************************
# class Cloud_user_change(Resource):
#     @jwt_required()
#     def put(self, email, new_password):
#         """
#         Modifie le mot de passe d'un Utilisateur dans le cloud général: QUESTION: A PRIORI INUTILE CAR LIE AU LDAP
#         ---
#         tags:
#           - Cloud
#        security:
#          - Bearer: []        
#         parameters:
#           - in: path
#             name: email
#             type: string
#             required: true
#           - in: path
#             name: new_password
#             type: string
#             required: true
#         responses:
#             200:
#                 description: Succès, mot de passe changé
#             404:
#                 description: Oops, mot de passe NON changé
#             500:
#                 description: Erreur interne du serveur
#         """    
# 
#         global cloud_ident, cloud_pass, cloud_url
# 
#         try:
#             auth = (cloud_ident, cloud_pass)
#             api_url = f"{cloud_url}/ocs/v1.php/cloud/users?search={email}"
#             headers = {"OCS-APIRequest": "true"}
#             data = {
#               "key": "password",
#               "value": new_password
#             }
#             response = requests.put(api_url, auth=auth, headers=headers)
# 
#             if response.status_code == 200:
#               if re.search(r'<element>.*</element>', response.text):
#                 return 200
#               else:
#                 return 404
# 
#         except Exception as e:
#             return jsonify({'error': str(e)}), 500
#           
