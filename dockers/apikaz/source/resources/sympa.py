 
from resources.common_imports import *

#les variables globales minimum
from resources.config import sympa_ident, sympa_pass, sympa_url, sympa_liste_info, MAIL_USERNAME


class Sympa_user(Resource):
    def __init__(self):
        global sympa_ident, sympa_pass, sympa_url,MAIL_USERNAME
        self.sympa_ident = sympa_ident
        self.sympa_pass = sympa_pass
        self.sympa_url = sympa_url

    def _execute_sympa_command(self, email, liste, service):
        try:
            if validate_email(email) and validate_email(liste):
                cmd = f'export PERL5LIB=/usr/src/app/:$PERL5LIB && /usr/src/app/Sympa/sympa_soap_client.pl --soap_url={self.sympa_url}/sympasoap --trusted_application={self.sympa_ident} --trusted_application_password={self.sympa_pass} --proxy_vars=USER_EMAIL={MAIL_USERNAME} --service={service} --service_parameters="{liste},{email}" && echo $?'
                output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
                return output.decode("utf-8"), 200  # Retourne la sortie et un code de succès
        except EmailNotValidError as e:
            return str(e), 400  # Retourne le message d'erreur et un code d'erreur 400
        except subprocess.CalledProcessError as e:
            return e.output.decode("utf-8"), 400  # Retourne la sortie de la commande et un code d'erreur 400

    @jwt_required()
    def post(self, email, liste):
        """
        Ajouter un email dans une liste sympa
        ---
        tags:
          - Sympa
        security:
          - Bearer: []                
        parameters:
          - in: path
            name: email
            type: string
            required: true
          - in: path
            name: liste
            type: string
            required: true
        responses:
            200:
                description: Succès, email ajouté dans la liste
            400:
                description: Oops, email non ajouté dans la liste
        """
        output, status_code = self._execute_sympa_command(email, liste, 'add')
        return output, status_code

    @jwt_required()
    def delete(self, email, liste):
        """
        Supprimer un email dans une liste sympa
        ---
        tags:
          - Sympa
        security:
          - Bearer: []                
        parameters:
          - in: path
            name: email
            type: string
            required: true
          - in: path
            name: liste
            type: string
            required: true
        responses:
            200:
                description: Succès, email supprimé dans la liste
            400:
                description: Oops, email non supprimé dans la liste
        """
        output, status_code = self._execute_sympa_command(email, liste, 'del')
        return output, status_code


 
