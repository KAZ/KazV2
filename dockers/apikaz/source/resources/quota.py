from resources.common_imports import *

#les variables globales minimum
from resources.config import serveur_imap, mot_de_passe_mail


class Quota(Resource):
  
#https://doc.dovecot.org/configuration_manual/authentication/master_users/

#https://blog.debugo.fr/serveur-messagerie-dovecot/
# sur DEV, j'ai modifié /etc/dovecot/conf.d/20-lmtp.conf
#mail_plugins = $mail_plugins sieve quota

    @jwt_required()
    def get(self, email):
        """
        Récupérer la place prise par une BAL (EN COURS)
        ---
        tags:
          - Quota
        security:
          - Bearer: []                
        parameters:
          - in: path
            name: email
            type: string
            required: true
        responses:
            200:
                description: Succès, taille d'une BAL'
            400:
                description: Oops, échec dans l'affichage de la taille d'une BAL
        """
        global serveur_imap, mot_de_passe_mail
                
        try:
            if validate_email(email):
                # Connexion au serveur IMAP
                mail = imaplib.IMAP4_SSL(serveur_imap)
                mail.login(email, mot_de_passe_mail)
                
                #res, data = mail.select("INBOX")
                #return data[0].decode("utf-8"), 200

                # Requête pour obtenir le quota de la boîte aux lettres
                # res, data = mail.getquota("INBOX")
                # return str(data[0]).split()
                
                res, data = mail.getquotaroot("INBOX")
                return str(data[1]).split()[3]+" KB"
              
                # Fermeture de la connexion
                mail.close()
                mail.logout()
                                          
            else:
                return "Email non valide", 400

        except imaplib.IMAP4.error as e:
            return str(e), 400  # Retourne le message d'erreur et un code d'erreur 400
        except EmailNotValidError as e:
            return str(e), 400  # Retourne le message d'erreur et un code d'erreur 400
                
