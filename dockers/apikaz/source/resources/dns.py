from resources.common_imports import *

#les variables globales minimum
from resources.config import gandi_key, gandi_url_api

 
class Dns_serveurs(Resource):

    def __init__(self):
        global gandi_key, gandi_url_api
        self.gandi_key = gandi_key
        self.gandi_url_api = gandi_url_api

    @jwt_required()
    def get(self):
        """
        Renvoie tous les serveurs kaz de la zone dns
        ---
        tags:
          - Dns
        security:
          - Bearer: []                
        responses:
            200:
                description: Succès, liste des serveurs
            404:
                description: Oops, soucis...
        """  
        url = f"{self.gandi_url_api}/records/srv/TXT"
        headers = { "Authorization": f"Apikey {self.gandi_key}" }
        response = requests.get(url, headers=headers)           
        
        if response.status_code != 200:
          return response.json(),response.status_code

        # Extraire la liste des serveurs de la réponse JSON
        rrset_values = response.json()["rrset_values"]
        # Nettoyer la liste
        serveurs = [serveur.strip('"') for serveur in rrset_values[0].split(";")]
        return serveurs, 200
                              

#*************************************************

class Dns(Resource):

    def __init__(self):
        global gandi_key, gandi_url_api
        self.gandi_key = gandi_key
        self.gandi_url_api = gandi_url_api
        self.dns_serveurs_resource = Dns_serveurs()

#*************************************************
    @jwt_required()
    def get(self,sdomaine):
        """
        Le sous-domaine existe t-il dans la zone dns avec un enreg CNAME ?
        ---
        tags:
          - Dns
        security:
          - Bearer: []                
        parameters:
          - in: path
            name: sdomaine
            type: string
            required: true
        responses:
            200:
                description: Succès, sdomaine existe déjà avec un CNAME
            404:
                description: Oops, sdomaine non trouvé
        """        
        url = f"{self.gandi_url_api}/records/{sdomaine}/CNAME"
        headers = { "Authorization": f"Apikey {self.gandi_key}" }
        response = requests.get(url, headers=headers)           
        return response.json(),response.status_code


#*************************************************

    @jwt_required() 
    def delete(self,sdomaine):
        """
        suppression du sdomaine 
        ---
        tags:
          - Dns
        security:
          - Bearer: []                
        parameters:
          - in: path
            name: sdomaine
            type: string
            required: true
        responses:
            204:
                description: Succès, sdomaine supprimé
            404:
                description: Oops,
        """       
        url = f"{self.gandi_url_api}/records/{sdomaine}"
        headers = { "Authorization": f"Apikey {self.gandi_key}" }
        response = requests.delete(url, headers=headers)
        return response.text,response.status_code
                                
#*************************************************

    @jwt_required()
    def post(self,sdomaine,serveur):
        """
        Créé le sous-domaine de type CNAME qui pointe sur serveur
        ---
        tags:
          - Dns
        security:
          - Bearer: []                
        parameters:
          - in: path
            name: sdomaine
            type: string
            required: true
          - in: path
            name: serveur
            type: string
            required: true
            description: Le serveur doit être l'un des serveurs disponibles dans la zone DNS
        responses:
            201:
                description: Succès, sdomaine créé comme CNAME
            400:
                description: Oops, serveur inconnu, sdomaine non créé
            404:
                description: Oops, sdomaine non créé
            200,409:
                description: Oops, sdomaine déjà créé
        """

        
        # Récupérer la liste des serveurs disponibles
        serveurs_disponibles, status_code = self.dns_serveurs_resource.get()
      
        if status_code != 200:
            return serveurs_disponibles, status_code
        
        if serveur not in serveurs_disponibles:
            return f"Erreur: Le serveur {serveur} n'est pas disponible", 400
                  
        url = f"{self.gandi_url_api}/records/{sdomaine}/CNAME"
        headers = { "Authorization": f"Apikey {self.gandi_key}" }
        payload = f'{{"rrset_values":["{serveur}"]}}'
        
        response = requests.post(url, data=payload, headers=headers)
        return response.json(),response.status_code
                                
#*************************************************


#*************************************************
