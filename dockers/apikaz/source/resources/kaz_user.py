from resources.common_imports import *

#les variables globales minimum
from resources.config import site_url, webmail_url, mdp_url


class Kaz_user(Resource):

    def __init__(self):
        global sympa_liste_info
        
        self.paheko_users_action_resource = Paheko_users_action()
        self.paheko_user_resource=Paheko_user()
        self.ldap_user_resource = Ldap_user()
        self.password_create_resource = Password_create()        
        self.mattermost_message_resource=Mattermost_message()
        self.mattermost_user_resource=Mattermost_user()
        self.mattermost_user_team_resource=Mattermost_user_team()
        self.mattermost_user_channel_resource=Mattermost_user_channel()
        self.mattermost_team_resource=Mattermost_team()                
        self.sympa_user_resource=Sympa_user()
        

#********************************************************************************************
    @jwt_required()
    def delete(self):
        """
        Utile pour les tests de createUser. Avant le POST de /kaz/create/users. Ça permet de supprimer/maj les comptes.
        ---
        tags:
          - Kaz User
        security:
          - Bearer: []                
        parameters: []
        responses:
            201:
                description: Succès        
            401: 
                description: Oops, un soucis quelconque
        """      
        #verrou pour empêcher de lancer en même temps la même api          
        try:
          prefixe="del_user_lock_"
          if glob(f"{tempfile.gettempdir()}/{prefixe}*"): raise ValueError("ERREUR 0 : api déjà en cours d'utilisation !")
          lock_file = tempfile.NamedTemporaryFile(prefix=prefixe,delete=True)
        
          #TODO à remplir à la main
          liste_emails=["",""]
          email_secours=""
          liste_sympa=""
          
          for email in liste_emails:
            res, status_code = self.ldap_user_resource.delete(email)
            res, status_code = self.mattermost_user_resource.delete(email)
            nom_orga=''.join(random.choice(string.ascii_lowercase) for _ in range(8))                        
            res, status_code = self.paheko_user_resource.put(email,"nom_orga",nom_orga)
            res, status_code = self.paheko_user_resource.put(email,"action_auto","A créer")
            res, status_code = self.paheko_user_resource.put(email,"email_secours",email_secours)
            res, status_code = self.sympa_user_resource.delete(email,liste_sympa)
            res, status_code = self.sympa_user_resource.delete(email_secours,liste_sympa)
            msg=f"**POST AUTO** suppression de {email} ok"
            self.mattermost_message_resource.post(message=msg)          
          return "OK", 200

        except ValueError as e:
          msg=f"(classe: {__class__.__name__} : {e}"
          self.mattermost_message_resource.post(message=msg)
          return str(msg), 401
        
#********************************************************************************************

    @jwt_required()
    def post(self):
        """
        Créé un nouveau kaznaute: inscription sur MM / Cloud / email + msg sur MM + email à partir de action="a créer" sur paheko
        ---
        tags:
          - Kaz User
        security:
          - Bearer: []                
        parameters: []
        responses:
            201:
                description: Succès, kaznaute créé        
            400: 
                description: Oops, rien à créer
            401: 
                description: Oops, un soucis quelconque
        """
        
        try:

          #verrou pour empêcher de lancer en même temps la même api          
          prefixe="create_user_lock_"
          if glob(f"{tempfile.gettempdir()}/{prefixe}*"): raise ValueError("ERREUR 0 : api déjà en cours d'utilisation !")
          lock_file = tempfile.NamedTemporaryFile(prefix=prefixe,delete=True)
         
          #qui sont les kaznautes à créer ?   
          liste_kaznautes, status_code = self.paheko_users_action_resource.get("A créer")
          
          if liste_kaznautes=="pas de résultat": return "ERREUR: paheko non joignable",401
          
          count=liste_kaznautes['count']
          if count==0: return "aucun nouveau kaznaute à créer",400
          
          #au moins un kaznaute à créer
          for tab in liste_kaznautes['results']:          
            email = tab['email'].lower() 
                     
            # est-il déjà dans le ldap ? (mail ou alias)          
            res, status_code = self.ldap_user_resource.get(email)         
            if status_code != 400: raise ValueError(f"ERREUR 1: {email} déjà existant dans ldap. {res}, on arrête tout")
            
            #test nom orga
            if tab['admin_orga'] == 1:
              if tab['nom_orga'] is None: 
                raise ValueError(f"ERREUR 0 sur paheko: {email} : nom_orga vide, on arrête tout")
              if not bool(re.match(r'^[a-z0-9-]+$', tab['nom_orga'])):
                raise ValueError(f"ERREUR 0 sur paheko: {email} : nom_orga ({tab['nom_orga']}) incohérent (minuscule/chiffre/-), on arrête tout")
                      
            #test email_secours
            email_secours = tab['email_secours'].lower()
            if not validate_email(email_secours): raise EmailNotValidError()

            #test quota
            quota = tab['quota_disque']
            if not quota.isdigit(): raise ValueError(f"ERREUR 2: quota non numérique : {quota}, on arrête tout")
          
            #quel type de test ?
            #"nom": "ROUSSEAU Mickael",
            nom, prenom = tab['nom'].split(maxsplit=1)
            
            #on génère un password
            password,status_code = self.password_create_resource.get()
          
            #on créé dans le ldap
            #à quoi servent prenom/nom dans le ldap ?
            data = {
              "prenom": prenom,
              "nom": nom,
              "password": password,
              "email_secours": email_secours,
              "quota": quota
            }            
            res, status_code = self.ldap_user_resource.put(email, **data)
            if status_code != 200: raise ValueError(f"ERREUR 3 sur ldap: {email} : {res}, on arrête tout ")
            
            #on créé dans MM 
            user = email.split('@')[0]
            res, status_code = self.mattermost_user_resource.post(user,email,password)
            if status_code != 200: raise ValueError(f"ERREUR 4 sur MM: {email} : {res}, on arrête tout ")
                      
            # et on affecte à l'équipe kaz
            res, status_code = self.mattermost_user_team_resource.post(email,"kaz")
            if status_code != 200: raise ValueError(f"ERREUR 5 sur MM: {email} : {res}, on arrête tout ")
          
            #et aux 2 canaux de base
            res, status_code = self.mattermost_user_channel_resource.post(email,"kaz","une-question--un-soucis")
            if status_code != 200: raise ValueError(f"ERREUR 6 sur MM: {email} : {res}, on arrête tout ")
            res, status_code = self.mattermost_user_channel_resource.post(email,"kaz","cafe-du-commerce--ouvert-2424h")
            if status_code != 200: raise ValueError(f"ERREUR 7 sur MM: {email} : {res}, on arrête tout ")
                        
            #on créé une nouvelle équipe ds MM si besoin
            if tab['admin_orga'] == 1:
              res, status_code = self.mattermost_team_resource.post(tab['nom_orga'],email)
              if status_code != 200: raise ValueError(f"ERREUR 8 sur MM: {email} : {res}, on arrête tout ")            
              #BUG: créer la nouvelle équipe n'a pas rendu l'email admin, on le rajoute comme membre simple
              res, status_code = self.mattermost_user_team_resource.post(email,tab['nom_orga'])
              if status_code != 200: raise ValueError(f"ERREUR 8.1 sur MM: {email} : {res}, on arrête tout ")
            
            #on créé dans le cloud genéral
            #inutile car tous les user du ldap sont user du cloud général.
            
            #on inscrit email et email_secours à la nl sympa_liste_info
            res, status_code = self.sympa_user_resource.post(email,sympa_liste_info)
            if status_code != 200: raise ValueError(f"ERREUR 9 sur Sympa: {email} : {res}, on arrête tout ")          
            res, status_code = self.sympa_user_resource.post(email_secours,sympa_liste_info)
            if status_code != 200: raise ValueError(f"ERREUR 10 sur Sympa: {email_secours} : {res}, on arrête tout ")
          
            #on construit/envoie le mail
            context = {
            'ADMIN_ORGA': tab['admin_orga'], 
            'NOM': tab['nom'],
            'EMAIL_SOUHAITE': email,
            'PASSWORD': password,
            'QUOTA': tab['quota_disque'],
            'URL_WEBMAIL': webmail_url,
            'URL_AGORA': mattermost_url,
            'URL_MDP': mdp_url,
            'URL_LISTE': sympa_url,
            'URL_SITE': site_url,
            'URL_CLOUD': cloud_url
            }                  
            subject="KAZ: confirmation d'inscription !"
            sender=app.config['MAIL_USERNAME']
            reply_to = app.config['MAIL_REPLY_TO']
            msg = Message(subject=subject, sender=sender, reply_to=reply_to, recipients=[email,email_secours])
            msg.html = render_template('email_inscription.html', **context)        
            mail.send(msg)
            
            #on met le flag paheko action à Aucune
            res, status_code = self.paheko_user_resource.put(email,"action_auto","Aucune")
            if status_code != 200: raise ValueError(f"ERREUR 12 sur paheko: {email} : {res}, on arrête tout ")            
          
            #on post sur MM pour dire ok
            msg=f"**POST AUTO** Inscription réussie pour {email} avec le secours {email_secours} Bisou!"
            self.mattermost_message_resource.post(message=msg)
                                    
          return "fin des inscriptions", 201
        
        except EmailNotValidError as e:
          msg=f"classe: {__class__.__name__} : ERREUR 13 : email_secours : {email_secours} " + str(e) +", on arrête tout"
          self.mattermost_message_resource.post(message=msg)             
          return msg, 401
        except ValueError as e:
          msg=f"(classe: {__class__.__name__} : {e}"
          self.mattermost_message_resource.post(message=msg)
          return str(msg), 401
        

#*************************************************

 
