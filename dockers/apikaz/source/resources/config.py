import os

#le paheko de kaz
paheko_ident=os.environ.get('paheko_API_USER')
paheko_pass=os.environ.get('paheko_API_PASSWORD')
paheko_url=os.environ.get('paheko_url')

#mattermost
mattermost_user=os.environ.get('mattermost_user')
mattermost_pass=os.environ.get('mattermost_pass')
mattermost_url=os.environ.get('mattermost_url')

#ldap
ldap_admin=os.environ.get('ldap_LDAP_ADMIN_USERNAME')
ldap_pass=os.environ.get('ldap_LDAP_ADMIN_PASSWORD')
ldap_root=os.environ.get('ldap_root')
ldap_host="ldapServ.ldapNet"

#cloud général
cloud_ident=os.environ.get('nextcloud_NEXTCLOUD_ADMIN_USER')
cloud_pass=os.environ.get('nextcloud_NEXTCLOUD_ADMIN_PASSWORD')
cloud_url=os.environ.get('cloud_url')

#sympa
sympa_ident=os.environ.get('sympa_SOAP_USER')
sympa_pass=os.environ.get('sympa_SOAP_PASSWORD')
sympa_url=os.environ.get('sympa_url')
sympa_liste_info=os.environ.get('sympa_liste_info')
MAIL_USERNAME=os.environ.get('apikaz_MAIL_USERNAME')

#pour QUOTA (à virer ensuite)
serveur_imap = os.environ.get('serveur_imap')
mot_de_passe_mail=os.environ.get('mot_de_passe_mail')

#dns
gandi_key=os.environ.get('gandi_GANDI_KEY')
gandi_url_api=os.environ.get('gandi_GANDI_API')

#kaz_user
site_url=os.environ.get('site_url')
#pour webmail_url et mdp_url, ça renvoie des tuples et non des str, bizarre, il fat mettre les url en dur
webmail_url=os.environ.get('webmail_url')
mdp_url=os.environ.get('mdp_url')
