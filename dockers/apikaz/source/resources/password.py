from resources.common_imports import *

#les variables globales minimum
#from resources.config import 


class Password_create(Resource):
    @jwt_required()
    def get(self):
        """
        créer un password qui colle avec les appli kaz
        ---
        tags:
          - Password
        security:
          - Bearer: []
        parameters: []
        responses:
          200:
            description: le password
          404:
            description: oops 
        """    
        global new_password

        cmd="apg -n 1 -m 10 -M NCL -d"
        try:
            output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)        
            new_password="_"+output.decode("utf-8")+"_"
            return new_password,200          
        
        except subprocess.CalledProcessError as e:
            return e.output.decode("utf-8"), 400 
 
