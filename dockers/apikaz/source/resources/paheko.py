from resources.common_imports import *

#les variables globales minimum
from resources.config import paheko_ident, paheko_pass, paheko_url


class Paheko_categories(Resource):
  
    @jwt_required()
    def get(self):
        """
        Récupérer les catégories Paheko avec le compteur associé
        ---
        tags:
          - Paheko
        security:
          - Bearer: []
        parameters: []
        responses:
          200:
            description: Liste des catégories Paheko
          404:
            description: oops 
        """    
        global paheko_ident, paheko_pass, paheko_url

        auth = (paheko_ident, paheko_pass)
        api_url = paheko_url + '/api/user/categories'
        
        response = requests.get(api_url, auth=auth)

        if response.status_code == 200:
            data = response.json()
            return jsonify(data)
        else:
            return jsonify({'error': 'La requête a échoué'}), response.status_code
    
 
#*************************************************

class Paheko_users(Resource):
  
    @jwt_required()
    def get(self,categorie):
        """
        Afficher les membres d'une catégorie Paheko
        ---
        tags:
          - Paheko
        security:
          - Bearer: []        
        parameters:
          - in: path
            name: categorie
            type: string
            required: true
        responses:
            200:
                description: Liste des membres une catégorie Paheko
            404:
                description: oops
        """    
    
        global paheko_ident, paheko_pass, paheko_url

        auth = (paheko_ident, paheko_pass)
        if not categorie.isdigit():
            return 'Id de category non valide', 400
        
        api_url = paheko_url + '/api/user/category/'+categorie+'.json'
        
        response = requests.get(api_url, auth=auth)

        if response.status_code == 200:
            data = response.json()
            return jsonify(data)
        else:
            return jsonify({'error': 'La requête a échoué'}), response.status_code
    

#*************************************************

class Paheko_user(Resource):
  
    def __init__(self):
        global paheko_ident, paheko_pass, paheko_url
        self.paheko_ident = paheko_ident
        self.paheko_pass = paheko_pass
        self.paheko_url = paheko_url
        self.auth = (self.paheko_ident, self.paheko_pass)
        
    @jwt_required()
    def get(self,ident):
        """
        Afficher un membre de Paheko par son email kaz ou son numéro ou le non court de l'orga
        ---
        tags:
          - Paheko        
        security:
          - Bearer: []                
        parameters: 
          - in: path
            name: ident
            type: string
            required: true        
            description: possible d'entrer un numéro, un email, le nom court de l'orga
        responses:
            200:
                description: Existe et affiche
            404:
                description: N'existe pas
        """    
        
        emailmatchregexp = re.compile(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$")

        if emailmatchregexp.match(ident):
          data = { "sql": f"select * from users where email='{ident}' or alias = '{ident}'" }
          api_url = self.paheko_url + '/api/sql/'
          response = requests.post(api_url, auth=self.auth,  data=data)
          #TODO: if faut Rechercher count et vérifier que = 1 et supprimer le count=1 dans la réponse
        elif  ident.isdigit():
          api_url = self.paheko_url + '/api/user/'+ident
          response = requests.get(api_url, auth=self.auth)                  
        else:
          nomorga = re.sub(r'\W+', '', ident) # on vire les caractères non alphanumérique
          data = { "sql": f"select * from users where admin_orga=1 and nom_orga='{nomorga}'" }
          api_url = self.paheko_url + '/api/sql/'
          response = requests.post(api_url, auth=self.auth,  data=data)
          #TODO:if faut Rechercher count et vérifier que = 1  et supprimer le count=1 dans la réponse
          
        if response.status_code == 200:
            data = response.json()
            if data["count"] == 1:
                 return jsonify(data["results"][0])
            elif data["count"] == 0:
                 return "pas de résultat", 400
            else: 
                 return "Plusieurs utilisateurs correspondent ?!", 400
        else:
            #return jsonify({'error': 'La requête a échoué'}), response.status_code
            return "pas de résultat", response.status_code
    
#*************************************************

    @jwt_required()
    def put(self,ident,field,new_value):
        """
        Modifie la valeur d'un champ d'un membre paheko (ident= numéro paheko ou email kaz)
        ---
        tags:
          - Paheko        
        security:
          - Bearer: []        
        parameters: 
          - in: path
            name: ident
            type: string
            required: true        
            description: possible d'entrer le numéro paheko, un email kaz 
          - in: path
            name: field
            type: string
            required: true        
            description: un champ de la table users de la base paheko
          - in: path
            name: new_value
            type: string
            required: true        
            description: la nouvelle valeur à remplacer
        
        responses:
            200:
                description: Modification effectuée avec succès
            400:
                description: Oops, ident non trouvé ou incohérent
            404:
                description: Oops, modification du champ KO
        """    
        
        #récupérer le numero paheko si on fournit un email kaz
        emailmatchregexp = re.compile(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$")
        if emailmatchregexp.match(ident):         
          data = { "sql": f"select id from users where email='{ident}'" }
          api_url = self.paheko_url + '/api/sql/'
          response = requests.post(api_url, auth=self.auth, data=data)
          if response.status_code == 200:
              #on extrait l'id de la réponse              
              data = response.json()
              if data['count'] == 0:
                return "email non trouvé", 400
              elif data['count'] > 1:
                return "trop de résultat", 400
              else:
                #OK 
                ident = data['results'][0]['id']
          else:
              return "pas de résultat", response.status_code          
        elif not ident.isdigit():
           return "Identifiant utilisateur invalide", response.status_code
        
        regexp = re.compile("[^a-zA-Z0-9 \\r\\n\\t" + re.escape(string.punctuation) + "]")
        valeur = regexp.sub('',new_value) # mouais, il faudrait être beaucoup plus précis ici en fonction des champs qu'on accepte...

        champ = re.sub(r'\W+','',field) # pas de caractères non alphanumériques ici, dans l'idéal, c'est à choisir dans une liste plutot
 
        api_url = self.paheko_url + '/api/user/'+str(ident)        
        payload = {champ: valeur}
        response = requests.post(api_url, auth=self.auth, data=payload)
        return response.json(),response.status_code
            
#*************************************************


#*************************************************

class Paheko_users_action(Resource):
  
    def __init__(self):
        global paheko_ident, paheko_pass, paheko_url
        self.paheko_ident = paheko_ident
        self.paheko_pass = paheko_pass
        self.paheko_url = paheko_url
        
    @jwt_required()
    def get(self, action):
        """
        retourne tous les membres de paheko avec une action à mener (création du compte kaz / modification...)
        ---
        tags:
          - Paheko 
        security:
          - Bearer: []                
        parameters:
          - in: path
            name: action
            type: string
            required: true
            enum: ['A créer','A modifier','En attente','Aucune']
        responses:
            200:
                description: liste des nouveaux kaznautes à créer
            404:
                description: aucun nouveau kaznaute à créer
        """    
        auth = (self.paheko_ident, self.paheko_pass)

        api_url = self.paheko_url + '/api/sql/'
        payload = { "sql": f"select * from users where action_auto='{action}'" }        
        response = requests.post(api_url, auth=auth,  data=payload)
          
        if response.status_code == 200:
            return response.json(),200
        else:
            return "pas de résultat", response.status_code
        

