
https://help.nextcloud.com/t/socket-error-when-accessing-collabora/22486/17
https://collabora-online-for-nextcloud.readthedocs.io/en/latest/install/
https://www.collaboraoffice.com/code/nginx-reverse-proxy/

https://www.digitalocean.com/community/tutorials/understanding-nginx-server-and-location-block-selection-algorithms

https://cloud.kaz.bzh/settings/admin/richdocuments
https://office.kaz.bzh/

docker run -t -d -p 127.0.0.1:9980:9980 -e 'domain=cloud\\.kaz\\.local --restart always --cap-add MKNOD collabora/code


https://stackoverflow.com/questions/31667160/running-docker-container-iptables-no-chain-target-match-by-that-name

*nat
:PREROUTING ACCEPT [144:8072]
:INPUT ACCEPT [87:5208]
:OUTPUT ACCEPT [118:8055]
:POSTROUTING ACCEPT [118:8055]
:DOCKER - [0:0]
... your previous rules here ...
-A PREROUTING -m addrtype --dst-type LOCAL -j DOCKER
-A OUTPUT ! -d 127.0.0.0/8 -m addrtype --dst-type LOCAL -j DOCKER
-A POSTROUTING -s 172.17.0.0/16 ! -o docker0 -j MASQUERADE
COMMIT
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [5781:5099614]
:DOCKER - [0:0]
... your previous rules here ...
-A FORWARD -o docker0 -j DOCKER
-A FORWARD -o docker0 -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
-A FORWARD -i docker0 ! -o docker0 -j ACCEPT
-A FORWARD -i docker0 -o docker0 -j ACCEPT
COMMIT
