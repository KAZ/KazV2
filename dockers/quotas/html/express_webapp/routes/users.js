
var express = require('express');
var router = express.Router();
const {Sequelize} = require("sequelize");

//___________________________________________________________________________________
// Ce petit script permet de connaitre la taille memoire d'un utilisateur

// Auteur : Nathaniel Guiquerro Lauryne Gaultier
// 
// GET : Quand on charge la page on tombe sur un formulaire 
// 	 il faut alors renseigner le email et un nombre entre 1 et 30
//	 ex : contact1@kaz.local et 25
// POST: SI l'email existe ou le nombre n'est pas correcte alors on va rechercher si 
// il existe les quotas avec le script  ../../quotas/UtilisateurTailleGlobal.sh
// 	 SINON on renvoie l'erreur du script ../../quotas/UtilisateurTailleGlobal.sh
//___________________________________________________________________________________


//fonction de convertiseur d'uniter automatique
function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

//fonction qui converti la donnée en paramètre
// bytes ----->  Mb
function getMB(bytes){
    return bytes/1000000;
}

async function getData(email , nb ){
    var tabDeDonnee = [];

    // XXX Felix
    // const sequelize = new Sequelize("quotas","root","tartelette",{
    // 	host: "quotasDB",
    // 	dialect:"mariadb"
    // });
    
    try{
	await sequelize.authenticate();
	//connecté
	const query ="SELECT * FROM Utilisateur WHERE utilisateur='"+email+"' ORDER BY date desc ;";
	sequelize.query(query).then(([results, metadata]) => {
	    
	    var i=0;
	    while(i<nb){
		tabDeDonnee.push(getMB(results[i].total));
		var date = new Date(results[i].date);
		var m =date.getMonth()+1;
		tabDeDonnee.push(date.getDate()+"/"+m+"/"+date.getFullYear()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds());
		i++;
	    }
	    
	})

    } catch (error){
	//pas connecté
	console.log(error);
	
    }
    return tabDeDonnee;	
}

//fonction qui permet de recuperer la limitation de place d'un utilisateur
async function getLimit(email, nb ){
    var tabDeDonnee = [];
    
    // XXX Felix
    // const sequelize = new Sequelize("quotas","root","tartelette",{
    // 	host: "quotasDB",
    // 	dialect:"mariadb"
    // });
    
    try{
	await sequelize.authenticate();
	//connecté
	const query ="SELECT * FROM Utilisateur WHERE utilisateur='"+email+"' ORDER BY date desc ;";
	sequelize.query(query).then(([results, metadata]) => {
	    var i=0;
	    while(i<nb){
		tabDeDonnee.push(getMB(results[i].limite*1000000000));
		i++;
	    }
	    tabDeDonnee
	    
	})
	
    } catch (error){
	//pas connecté
	console.log(error);
	
    }
    return tabDeDonnee;	
}

function splitDonne(array){
    var tabDonne = [];
    for (let i = 0; i < array.length; i++) {
	if(i%2==0){
	    tabDonne.push(array[i]);
	}
    }
    return tabDonne;
}


function splitTime(array){
    var tabTime = [];
    for (let i = 0; i < array.length; i++) {
	if(i%2!=0){
	    tabTime.push(array[i]);
	}
    }
    return tabTime;
}

//fonction qui converti le timestamp en un objet Date
function timeConverter(UNIX_timestamp){
    var date = new Date(UNIX_timestamp);
    
    return (date.getDate()+
            "/"+(date.getMonth()+1)+
            "/"+date.getFullYear()+
            " "+date.getHours()+
            ":"+date.getMinutes()+
            ":"+date.getSeconds());
}

/* 	
	GET users listing. 
	Cette page affiche le formulaire avec l'email
*/
router.get('/', function(req, res, next) {  
    res.render('users');
});


/*
  ICI ce trouve le traitement du mail renseignée via le formulaire
*/
router.post('/', async function(req, res, next) {
    myArray = req.body.email;
    var splits = myArray.split("@"); 	

    email=splits[0];
    
    var nombreJ = req.body.nombreJ;
    if(nombreJ<1 || nombreJ>30) res.render('usersError');
    
    // XXX Felix
    // const sequelize = new Sequelize("quotas","root","tartelette",{
    // 	host: "quotasDB",
    // 	dialect:"mariadb"
    // });
    
    (async ()=>{
  	try{
  	    await sequelize.authenticate();
  	    var dat = await getData(email,nombreJ);
  	    var lim = await getLimit(email,nombreJ);
	    
  	    setTimeout(function(){ 
		console.log("Ready")
		
  		var tabDonne = splitDonne(dat);
  		var tabTime = splitTime(dat);
  		
  		
  		//connecté
  		const query ="SELECT * FROM Utilisateur WHERE utilisateur='"+email+"' ORDER BY date desc LIMIT 1;";
  		sequelize.query(query).then(([results, metadata]) => {
  		    
		    var i=0;
		    var trouve =false;
		    while(i<results.length && !trouve ){
         		if(results[i].utilisateur==email){
         		    var R = {"nom":email,"Postfix":getMB(results[i].postfix),"NextCloud":getMB(results[i].nextcloud),"Total":getMB(results[i].total),"Dateduprélevement":timeConverter(results[i].date),"dataChart":tabDonne,"labels":tabTime,"Lim":lim};

			    res.render('usersfound',{data:R});
			    trouve=true;
         		}
         		i++;
		    }
		    
		    if(!trouve){
			res.render('usersError');
		    }
		    
		})
  		
  	    }, 1000);	
  	} catch (error){
  	    //pas connecté
  	    console.log(error);
  	}
    })();
});

module.exports = router;

