var express = require('express');
var router = express.Router();

//_________________________________________________________________
// script route pour la page indexAdmin.jade
//
// Auteur : Nathaniel GUIQUERRO
//_________________________________________________________________

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('indexAdmin', { title: 'Express' });
});

module.exports = router;
