var express = require('express');
var router = express.Router();

//_________________________________________________________________
// script route pour la page index.jade
//
// Auteur : Nathaniel GUIQUERRO
//_________________________________________________________________

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
