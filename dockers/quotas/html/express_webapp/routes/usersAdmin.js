
var express = require('express');
var router = express.Router();
const {Sequelize} = require("sequelize");

//___________________________________________________________________________________
// Ce petit script permet de connaitre la taille memoire d'un utilisateur

// Auteur : Nathaniel Guiquerro Lauryne Gaultier
// 
// GET : Quand on charge la page on tombe sur un formulaire 
// 	 il faut alors renseigner le email et un nombre entre 1 et 30
//	 ex : contact1@kaz.local et 25
// POST: SI l'email existe ou le nombre n'est pas correcte alors on va rechercher si 
// il existe les quotas avec le script  ../../quotas/UtilisateurTailleGlobal.sh
// 	 SINON on renvoie l'erreur du script ../../quotas/UtilisateurTailleGlobal.sh
//___________________________________________________________________________________

//fonction de convertiseur d'uniter automatique
function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

//fonction qui converti le timestamp en un objet Date
function timeConverter(UNIX_timestamp){
    var date = new Date(UNIX_timestamp);

    return (date.getDate()+
            "/"+(date.getMonth()+1)+
            "/"+date.getFullYear()+
            " "+date.getHours()+
            ":"+date.getMinutes()+
            ":"+date.getSeconds());
}

/* 	
	GET users listing. 
	Cette page affiche le formulaire avec l'email
*/
router.get('/', function(req, res, next) {  
    res.render('users');
});

/*
  ICI ce trouve le traitement du mail renseignée vvia le formulaire
*/
router.post('/', function(req, res, next) {
    myArray = req.body.email;
    var splits = myArray.split("@"); 	

    email=splits[0];
    
    // XXX Felix
    // const sequelize = new Sequelize("quotas","root","tartelette",{
    // 	host: "db",
    // 	dialect:"mariadb"
    // });
    
    (async ()=>{
  	try{
  	    await sequelize.authenticate();
  	    //connecté
  	    const query ="SELECT * FROM Utilisateur ORDER BY date desc LIMIT 1;";
  	    sequelize.query(query).then(([results, metadata]) => {
		var i=0;
		var trouve =false;
		while(i<results.length && !trouve ){
         	    if(results[i].utilisateur==email){
         		var R = {"Postfix: ":formatBytes(results[i].postfix),
				 "NextCloud: ":formatBytes(results[i].nextcloud),
				 "Total: ":formatBytes(results[i].total),
				 "Date du prélevement: ":timeConverter(results[i].date)};
			res.render('usersfound',{data:R});
			trouve=true;
         	    }
         	    i++;
		}
		
		if(!trouve){
		    res.render('usersError');
		}
		
	    })

  	} catch (error){
  	    //pas connecté
  	    console.log(error);
  	}
    })();
});

module.exports = router;
