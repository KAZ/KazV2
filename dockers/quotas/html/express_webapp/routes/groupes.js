var express = require('express');
var router = express.Router();

//_________________________________________________________________
// script route pour la page groupe.jade
//
// Auteur : Nathaniel GUIQUERRO
//_________________________________________________________________

/* GET users listing. */
router.get('/', function(req, res, next) {
  console.log("J'envoie le render de la page Groupe");
  res.render('groupes');
});

module.exports = router;
