
// Creating new Object of Sequelize
const sequelize = new Sequelize(
    process.env.MYSQL_DATABASE,
    process.env.MYSQL_USER,
    process.env.MYSQL_PASSWORD, {
        // Explicitly specifying 
        // mysql database
        dialect: 'mariadb',
        // By default host is 'localhost'           
        host: 'db'
    }
);

// Exporting the sequelize object. 
// We can use it in another file
// for creating models
module.exports = sequelize
