# KAZ : AFFICHAGE DES QUOTAS

Auteur du document : GUIQUERRO Nathaniel, GAULTIER Lauryne
Création de l'affichage de Quota : GAULTIER Lauryne, GUIQUERRO Nathaniel
Date : 7 / 01 / 2022

# Installation :
Avant tout lancement 'npm' doit déjà être installé.
Ensuite il faut récuperer le code contenu dans kaz.quota et express_webapp du git.
Vérifiez bien que express_webapp contient un répertoire bin avec un fichier www dedans.

Ensuite ouvez un terminal et placez vous dans le répertoire express_webapp grâce à la 
commande cd ./express_webapp et exécutez les commande 'npm install' et 'npm start'.

Si aucun message d'erreur ne s'affiche, rendez vous sur votre navigateur préféré pour vous connecter sur l'url : 

```bash 
http://localhost:3000/
```

Bonne lecture ! 

Cordialement l'équipe Nelph

# Installation avec Docker :

Lancement WEB et DB :

```bash
cd /kaz/src
```

```bash
./lancementDocker.sh
```
Initialiser la base de données:
```bash
./initDB.sh tartelette
```

Vous pouvez accéder à la DB avec la commande :
```bash
mysql -h 127.0.0.1 -tartelette
```
Le page est accessible par navigateur à l'adresse : 

```bash
localhost:3000
```

