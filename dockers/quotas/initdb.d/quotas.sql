CREATE DATABASE IF NOT EXISTS quotas;
USE quotas;

CREATE TABLE IF NOT EXISTS Global (
	date TIMESTAMP PRIMARY KEY,
	total BIGINT,
	agora BIGINT,
	postfix BIGINT,
	nextcloud BIGINT
);

CREATE TABLE IF NOT EXISTS Utilisateur (
	date TIMESTAMP,
	utilisateur VARCHAR(50) ,
	limite INT,
	total BIGINT,
	postfix BIGINT,
	nextcloud BIGINT
);

CREATE TABLE IF NOT EXISTS Associations (
	date TIMESTAMP PRIMARY KEY,
	associations VARCHAR(50),
	total BIGINT,
	agora BIGINT,
	postfix BIGINT,
	nextcloud BIGINT
);
