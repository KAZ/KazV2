#!/bin/bash

KAZ_ROOT=$(cd "$(dirname $0)/../.."; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

###########################################################################
printKazMsg "\n  *** Création du Dockerfile quotas"

cd "${KAZ_ROOT}"

docker build -t quotaskaz . -f dockers/quotas/Dockerfile
