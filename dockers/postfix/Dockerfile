FROM docker.io/mailserver/docker-mailserver:14.0.0

########################################
# APT local cache
# work around because COPY failed if no source file
COPY .dummy .apt-mirror-confi[g] .proxy-confi[g] /
RUN cp /.proxy-config /etc/profile.d/proxy.sh 2> /dev/null || true
RUN if [ -f /.apt-mirror-config ] ; then . /.apt-mirror-config && sed -i \
    -e "s/deb.debian.org/${APT_MIRROR_DEBIAN}/g" \
    -e "s/security.debian.org/${APT_MIRROR_DEBIAN_SECURITY}/g" \
    -e "s/archive.ubuntu.com/${APT_MIRROR_UBUNTU}/g" \
    -e "s/security.ubuntu.com/${APT_MIRROR_UBUNTU_SECURITY}/g" \
    /etc/apt/sources.list; fi

########################################
RUN apt-get update
RUN apt-get -y autoremove
RUN apt-get install -y locales locales-all
RUN sed -i '/fr_FR.UTF-8/s/^# //g' /etc/locale.gen && locale-gen
ENV LC_ALL fr_FR.UTF-8
ENV LANG fr_FR.UTF-8
ENV LANGUAGE fr_FR:fr
RUN update-locale LANG=fr_FR.UTF-8

RUN apt-get -y install rsyslog apt-utils apg gawk altermime
RUN apt-get -y install libboost-program-options-dev libboost-system-dev libboost-filesystem-dev libcurl4-gnutls-dev
#RUN apt-get -y install emacs elpa-php-mode
RUN apt-get -y install joe vim nano mailutils bsd-mailx procps dos2unix 

# creation du user filter,son repertoire home, copie des fichiers
RUN mkdir /home/filter ; useradd -d /home/filter filter ; chown filter /home/filter

RUN apt-get install -y --fix-missing doxygen dos2unix git build-essential make g++ libboost-program-options-dev libboost-system-dev libboost-filesystem-dev libcurl4-gnutls-dev libssl-dev
WORKDIR /home/
RUN git clone https://git.kaz.bzh/KAZ/depollueur.git
WORKDIR /home/depollueur/
RUN make

RUN cp build/out/* /home/filter/
RUN cp src/bash/* /home/filter/

RUN chown filter /home/filter/*; chmod 755 /home/filter/*

# creation du repertoire filter et application des bons droits pour le filtre
RUN mkdir -p /var/log/mail; touch /var/log/mail/filter.log ; chown filter /var/log/mail/filter.log ; chmod 777 /var/log/mail/filter.log
RUN mkdir -p /var/spool/filter ; chmod 775 /var/spool/filter ; chown filter /var/spool/filter
RUN mkdir -p /var/log/mail/pb ; chmod a+rwx /var/log/mail/pb
RUN sed -i '5i/var/log/mail/filter.log' /etc/logrotate.d/rsyslog

# modif des fichiers de postfix
RUN cat /home/filter/master.cf.update >> /etc/postfix/master.cf
RUN sed -i -e 's/reject_rbl_client bl.spamcop.net$//g' /etc/postfix/main.cf

# pour le confort : modif du .bashrc de root
RUN sed -i 's/# alias/alias/g' /root/.bashrc
RUN /etc/init.d/postfix restart

RUN echo "#!/bin/bash" > /entrypoint.sh
RUN echo "/usr/bin/supervisord -c /etc/supervisor/supervisord.conf" >> /entrypoint.sh
RUN chmod u+x /entrypoint.sh

# HOTFIX DMARC
RUN chmod 777 /var/run/opendmarc/

ENTRYPOINT ["/entrypoint.sh"]
