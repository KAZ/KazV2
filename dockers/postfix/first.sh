#!/bin/bash

SERV_DIR=$(cd $(dirname $0); pwd)
KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

cd $(dirname $0)
. "${DOCKERS_ENV}"

checkDockerRunning "${smtpServName}" "Postfix" || exit

SETUP_MAIL="docker exec -ti mailServ setup"

printKazMsg "\n  *** Premier lancement de Postfix"

if [ ! -s /var/lib/docker/volumes/postfix_mailConfig/_data/user-patches.sh ]; then
  cat > /var/lib/docker/volumes/postfix_mailConfig/_data/user-patches.sh << EOF
#!/bin/bash
awk -i inplace 'BEGIN {cp=0} /127.0.0.1:10025/ {cp=1} /content_filter=/ {if (cp) gsub("content_filter=.*", "content_filter=filter:dummy"); print \$0; cp=0; next} {print}' /etc/postfix/master.cf
echo ${domain} > /home/filter/config/domainedepot
touch /home/filter/config/file_domaines_non_depollues.txt
sed -i "s/\$mail_plugins/\$mail_plugins quota/g" /etc/dovecot/conf.d/10-mail.conf
sed -i "s/\$mail_plugins/\$mail_plugins imap_quota/g" /etc/dovecot/conf.d/20-imap.conf
mv /etc/dovecot/conf.d/90-quota.conf.disab /etc/dovecot/conf.d/90-quota.conf
EOF

chmod +x /var/lib/docker/volumes/postfix_mailConfig/_data/user-patches.sh
fi

[ "${mode}" == "local" ] || exit

# echo "virtual_alias_maps = ldap:/etc/postfix/ldap-aliases.cf, ldap:/etc/postfix/ldap-groups.cf, texthash:/etc/postfix/virtual" >> config/postfix-main.cf
