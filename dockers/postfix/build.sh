#!/bin/bash

KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

printKazMsg "\n  *** Création du Dockerfile Postfix"

cd "${KAZ_ROOT}"
docker build --no-cache -t postfixkaz . -f dockers/postfix/Dockerfile

