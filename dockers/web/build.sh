#!/bin/bash

KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

cd $(dirname $0)
. "${DOCKERS_ENV}"

cp autoconfig.yml.dist autoconfig.yml
