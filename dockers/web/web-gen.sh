#!/bin/bash

########################################
# setup

KAZ_ROOT=$(cd "$(dirname $0)/../.."; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

#TMPL=html/index.tmpl.html
#CONF=html/index.html
TEST_TMPL="${DOCK_VOL}/web_html/_data/status/allServices.tmpl.html"
TEST_CONF="${DOCK_VOL}/web_html/_data/status/allServices.html"

declare -A siteNames=( [prod1]="Rennes" [prod2]="Nantes A"  [prod3]="Nantes B" )
declare -A siteSSH=( [prod1]="" [prod2]="ssh -p 2201 root@prod2.kaz.bzh"  [prod3]="ssh -p 2201 root@kazoulet.kaz.bzh" )
siteKeys=$(echo "${!siteNames[@]}"|tr ' ' '\n'|sort|tr '\n' ' ')

declare -a availableServices
. "${DOCKERS_ENV}"
# XXX A reprendre de ce qui est attendu par container.sh
availableServices=(
    www
    ${calcHost}
    # ${castopodHost} : need orga
    ${cloudHost}
    ${dateHost}
    ${dokuwikiHost}
    ${fileHost}
    ${gitHost}
    # ${gravHost}
    ${imapsyncHost}
    # ${ldapHost}
    ${ldapUIHost}
    ${matterHost}
    ${mobilizonHost}
    ${padHost}
    # paheko : need orga
    ${sympaHost}
    ${vigiloHost}
    ${vaultwardenHost}
    ${webmailHost}
    # wordpress : need orga
)

echo -e "create status/allServices.html"

mkdir -p "$(dirname ${TEST_TMPL})"

########################################
# commun

echo -e "${BLUE}${BOLD}    * Base${NC}"
cat > "${TEST_TMPL}" <<EOF
<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <title>KAZ : test des services</title>
    <style>
      iframe { width: 450px; height: 300px; }
      div { display: inline-block; }
      p { display: block; margin: 2 0 0 0; }
    </style>
  </head>
  <!--noframes-->
  <body>

<H1>Commun</H1>
EOF

for service in ${availableServices[@]} ; do
    echo "    <div><p><a href=\"__HTTP_PROTO__://${service}.__DOMAIN__\">${service}</a></p><iframe src=\"__HTTP_PROTO__://${service}.__DOMAIN__\"></iframe></div>" >> "${TEST_TMPL}"
done

for service in prod1-office prod2-office kazoulet-office; do
    echo "    <div><p><a href=\"__HTTP_PROTO__://${service}.__DOMAIN__\">${service}</a></p><iframe src=\"__HTTP_PROTO__://${service}.__DOMAIN__\"></iframe></div>" >> "${TEST_TMPL}"
done

echo "    <br/>" >> "${TEST_TMPL}"

########################################
# All sites

for siteKey in ${siteKeys}; do
    echo -e "${BLUE}${BOLD}    * Orgas ${siteNames[$siteKey]}${NC}"
    declare -a availableOrga
    availableOrga=( $(${siteSSH[$siteKey]} cat "${KAZ_CONF_DIR}/container-orga.list" | sed -e "s/\(.*\)[ \t]*#.*$/\1/" -e "s/^[ \t]*\(.*\)$/\1/" -e "/^$/d") )

    echo "<H1>Le(s) ${#availableOrga[@]} orga(s) de ${siteNames[$siteKey]} : </H1>" >> "${TEST_TMPL}"
    for orga in ${availableOrga[@]} ; do
	orgaName=${orga%-orga}

	echo "  <b>${orgaName}</b>"
	services=$(${siteSSH[$siteKey]} "${KAZ_COMP_DIR}/${orga}/orga-gen.sh" -l | sed -e "s/collabora/office/")
	for service in ${services}; do
 	    echo "  <div><p><a href=\"__HTTP_PROTO__://${orgaName}-${service}.__DOMAIN__\">${orgaName}-${service}</a></p></div>"
	done
 	echo "  <br/>"
    done >> "${TEST_TMPL}"
done

########################################
# foreign domains

echo -e "${BLUE}${BOLD}    * Extern${NC}"
echo "<H1>Les redirections</H1>" >> "${TEST_TMPL}"
for siteKey in ${siteKeys}; do
    echo -e "${BLUE}${BOLD}      * Orgas ${siteNames[$siteKey]}${NC}"
    
    echo "<H2>${siteNames[$siteKey]}</H2>" >> "${TEST_TMPL}"
    for extrenal in $( ${siteSSH[$siteKey]} "${KAZ_BIN_DIR}/foreign-domain.sh" -l | sort -u) ; do
	echo "    <div><p><a href=\"__HTTP_PROTO__://${extrenal}\">${extrenal}</a></p></div>"
    done >> "${TEST_TMPL}"    
done
echo "    <br/><br/><br/></html>" >> "${TEST_TMPL}"

########################################
#"${APPLY_TMPL}" "${TMPL}" "${CONF}"
"${APPLY_TMPL}" "${TEST_TMPL}" "${TEST_CONF}"
