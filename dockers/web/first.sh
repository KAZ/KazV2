#!/bin/bash

KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

cd $(dirname $0)
. "${DOCKERS_ENV}"

WEB_URL="${httpProto}://${domain}"

checkDockerRunning "${webServName}" "Web" || exit

if [ $(ls "${DOCK_VOL}/web_html/_data/"  2>/dev/null | wc -l) -le 4 ] ; then
    printKazMsg "\n  *** Premier lancement de WEB"

    rsync -aAHXh --info=progress2 ./html/ "${DOCK_VOL}/web_html/_data/"
    chown -R www-data: "${DOCK_VOL}/web_html/_data/"
fi
