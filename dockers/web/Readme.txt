Pour l'installation d'un serveur web static (avec NGINX)

0) Verification qu'il n'y a pas deja une image

# docker images

# cd /root/install_web

1) fabrication du docker avec le fichier Dockerfile
Cela utilise
  - Dockerfile : le fichier de description
  - html : qui contient les pages static

# docker build -t webkaz .

2) verification de la creation (webkaz apparait)
# docker images

3) Lancement du web
Dans docker-compose.yml
  - il y a nommage du container

# docker-compose up -d

4) Verification
Il y a un container

# docker ps

