#!/bin/bash

KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

cd $(dirname $0)
. "${DOCKERS_ENV}"

WEB_DIR="${DOCK_VOL}/roundcube_webmailWeb/_data"
CONF_DIR="${WEB_DIR}/config"
IMG_DIR="${WEB_DIR}/skins/elastic/images"

if ! grep -q  "KAZ custom" "${CONF_DIR}/config.inc.php" 2> /dev/null; then
    printKazMsg "\n  *** Premier lancement de roundcube"

    cat "config/add-to-config.inc.php" >> "${CONF_DIR}/config.inc.php"
    cp "config/custom_config.inc.php" "${CONF_DIR}/"
    cp "config/kazmel.png" "${IMG_DIR}/"
fi
