#!/bin/bash

EMAIL=$1
NEWPASSWORD=$(base64 -d <<< $2)
OLDPASSWORD=$(base64 -d <<< $3)

URL_AGORA="https://${matterHost}.${domain}"
mattermost_token=${LDAPUI_MM_ADMIN_TOKEN}

IDUSER=$(curl -s -H "Authorization: Bearer ${mattermost_token}" "${URL_AGORA}/api/v4/users/email/${EMAIL}" | awk -F "," '{print $1}' | sed -e 's/{"id"://g' -e 's/"//g')
if [ ${IDUSER} == 'app.user.missing_account.const' ]
then
  exit 1
else
  curl -X PUT -i -H "Authorization: Bearer ${mattermost_token}" -d "{\"current_password\":\"${OLDPASSWORD}\",\"new_password\":\"${NEWPASSWORD}\"}" "${URL_AGORA}/api/v4/users/${IDUSER}/password"
fi
