<?php
#==============================================================================
# LTB Self Service Password
#
# Copyright (C) 2009 Clement OUDOT
# Copyright (C) 2009 LTB-project.org
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# GPL License: http://www.gnu.org/licenses/gpl.txt
#
#==============================================================================

# 30-04-2024 : Traduction en breton par Fanch Jestin ( @fanch.jestin sur l'agora de kaz.bzh)

#==============================================================================
# Breton
#==============================================================================
$messages['phpupgraderequired'] = "PHP a rank bezañ hizivaet";
$messages['nophpldap'] = "Mat e vefe deoc'h staliañ PHP LDAP evit implijout an ostilh-mañ";
$messages['nophpmhash'] = "Mat e vefe deoc'h staliañ PHP mhash evit implijout ar mod Samba";
$messages['nokeyphrase'] = "Ret eo deoc'h kefluniañ keyphrase evit lakaat an enkripterezh da vont en-dro";
$messages['ldaperror'] = "Ur fazi a zo bet en ur aksediñ ar c'havlec'h";
$messages['loginrequired'] = "Lakait hoc'h anv-implijer";
$messages['oldpasswordrequired'] = "Lakait ho ker-tremen kozh";
$messages['newpasswordrequired'] = "Lakait ho ker-tremen nevez";
$messages['confirmpasswordrequired'] = "Kadarnait ho ker-tremen nevez";
$messages['passwordchanged'] = "Cheñchet eo bet ho ker-tremen";
$messages['nomatch'] = "Ar gerioù-tremen ne glotont ket kenetrezo";
$messages['badcredentials'] = "Anv-implijer pe ger-tremen direizh";
$messages['passworderror'] = "Nac'het eo bet ar ger-tremen";
$messages['title'] = "Merañ ar ger-tremen";
$messages['login'] = "Anv-implijer";
$messages['oldpassword'] = "Ger-tremen kozh";
$messages['newpassword'] = "Ger-tremen nevez";
$messages['confirmpassword'] = "Kadarnaat ar ger-tremen";
$messages['submit'] = "Kas";
$messages['tooshort'] = "Re verr eo ho ker-tremen";
$messages['toobig'] = "Re hir eo ho ker-tremen";
$messages['minlower'] = "N'eus ket a-walc'h a lizherennoù munut en ho ker-tremen";
$messages['minupper'] = "N'eus ket a-walc'h a lizherennoù bras en ho ker-tremen";
$messages['mindigit'] = "N'eus ket a-walc'h a sifroù en ho ker-tremen";
$messages['minspecial'] = "N'eus ket a-walc'h a arouezioù ispisial en ho ker-tremen";
$messages['sameasold'] = "Heñvel eo ho ker-tremen ouzh an hini kozh";
$messages['policy'] = "Ho ker-tremen a rank doujañ d'ar redioù-mañ :";
$messages['policyminlength'] = "Niver a arouezioù d'an nebeutañ :";
$messages['policymaxlength'] = "Niver a arouezioù d'ar muiañ :";
$messages['policyminlower'] = "Niver a lizherennoù munut d'an nebeutañ :";
$messages['policyminupper'] = "Niver a lizherennoù bras d'an nebeutañ :";
$messages['policymindigit'] = "Niver a sifroù d'an nebeutañ :";
$messages['policyminspecial'] = "Niver a arouezioù ispisial d'an nebeutañ :";
$messages['forbiddenchars'] = "Arouezioù berzet a zo en ho ker-tremen";
$messages['policyforbiddenchars'] = "Arouezioù berzet :";
$messages['policynoreuse'] = "Arabat d'ho ker-tremen nevez bezañ heñvel ouzh an hini kozh";
$messages['questions']['birthday'] = "Peseurt deiz ez oc'h bet ganet ?";
$messages['questions']['color'] = "Peseurt liv a blij ar muiañ deoc'h ?";
$messages['password'] = "Ger-tremen";
$messages['question'] = "Goulenn";
$messages['answer'] = "Respont";
$messages['setquestionshelp'] = "Dibabit pe cheñchit ho koublad goulenn/respont a-benn adderaouiñ ho ker-tremen. Goude-se e c'helloc'h cheñch ho ker-tremen <a href=\"?action=resetbyquestions\">amañ</a>.";
$messages['answerrequired'] = "N'ho peus lakaet respont ";
$messages['questionrequired'] = "N'ho peus dibabet goulenn ebet";
$messages['passwordrequired'] = "Ho ker-tremen a rankit lakaat";
$messages['answermoderror'] = "N'eo ket bet enrollet ho respont";
$messages['answerchanged'] = "Enrollet eo bet ho respont";
$messages['answernomatch'] = "N'eo ket reizh ho respont";
$messages['resetbyquestionshelp'] = "Dibabit ur goulenn ha respontit outi a-benn adderaouiñ ho ker-tremen. En a-raok e rankit bezañ <a href=\"?action=setquestions\">enrollet ur respont</a>.";
$messages['changehelp'] = "Lakait ho ker-tremen kozh ha dibabit unan nevez.";
$messages['changehelpreset'] = "Ankounac'haet ho ker-tremen ganeoc'h ?";
$messages['changehelpquestions'] = "<a href=\"?action=resetbyquestions\">Adderaouit ho ker-tremen dre respont ouzh goulennoù</a>";
$messages['changehelptoken'] = "<a href=\"?action=sendtoken\">Adderaouit ho ker-tremen dre degemer un daeadenn dre bostel</a>";
$messages['changehelpsms'] = "<a href=\"?action=sendsms\">Adderaouit ho ker-tremen dre SMS</a>";
$messages['resetmessage'] = "Kevarc'h {login},\n\nKlikit amañ evit adderaouiñ ho ker-tremen :\n{url}\n\nMa n'eo ket ganeoc'h-c'hwi eo bet goulennet, na rit ket a van.";
$messages['resetsubject'] = "Adderaouiñ ho ker-tremen";
$messages['sendtokenhelp'] = "Lakait hoc'h anv-implijer hag ho chomlec'h postel evit adderaouiñ ho ker-tremen. Goude-se e rankoc'h klikañ war al liamm a vo bet kaset deoc'h dre bostel.";
$messages['sendtokenhelpnomail'] = "Lakait hoc'h anv-implijer evit adderaouiñ ho ker-tremen. Goude-se e rankoc'h klikañ war al liamm a vo bet kaset deoc'h dre bostel.";
$messages['mail'] = "Chomlec'h postel";
$messages['mailrequired'] = "Ho chomlec'h-postel a rankit lakaat";
$messages['mailnomatch'] = "Ne glot ket ar chomlec'h postel gant an anv-implijer merket";
$messages['tokensent'] = "Kaset ez eus bet ur postel kadarnaat";
$messages['tokennotsent'] = "En em gavet ez eus ur fazi pa 'z eo bet kaset ar postel kadarnaat";
$messages['tokenrequired'] = "Ret eo kaout ar jedouer adderaouiñ";
$messages['tokennotvalid'] = "N'eo ket reizh ar jedouer adderaouiñ";
$messages['resetbytokenhelp'] = "Ar jedaouer kaset dre bostel a ro an tu deoc'h da adderaouiñ ho ker-tremen. Evit degemer ur jedaouer nevez, <a href=\"?action=sendtoken\">klikit amañ</a>.";
$messages['resetbysmshelp'] = "Ar jedaouer kaset dre SMS a ro an tu deoc'h da adderaouiñ ho ker-tremen. Evit degemer ur jedaouer nevez, <a href=\"?action=sendsms\">klikit amañ</a>.";
$messages['changemessage'] = "Kevarc'h {login},\n\nCheñchet eo bet ho ker-tremen.\n\nMa n'eo ket ganeoc'h-c'hwi eo bet goulennet, kit diouzhtu e darempred gant merour ho rouedad.";
$messages['changesubject'] = "Cheñchet eo bet ho ker-tremen";
$messages['badcaptcha'] = "N'eo ket bet skoet mat ar 'c'haptcha'. Klaskit adarre.";
$messages['captcharequired'] = "Ar 'c'haptcha' a rankit skeiñ.";
$messages['captcha'] = "Captcha";
$messages['notcomplex'] = "Ne 'z eus ket a-walc'h a zoareoù arouezennoù disheñvel gant ho ker-stur.";
$messages['policycomplex'] = "Niver a zoareoù arouezennoù disheñvel d'an nebeutañ :";
$messages['sms'] = "Niverenn SMS";
$messages['smsresetmessage'] = "Setu ho jedouer :";
$messages['sendsmshelp'] = "Skoit hoc'h anv-implijer evit degemer ho kod-kadarnaat. Goude-se skoit ar c'hod ho po bet dre SMS.";
$messages['smssent'] = "Kaset eo bet ar c'hod-kadarnaat dre SMS.";
$messages['smsnotsent'] = "Ur fazi a zo bet en ur kas an SMS";
$messages['smsnonumber'] = "N'eo ket bet kavet an niverenn pellgomz hezoug.";
$messages['userfullname'] = "Anv klok";
$messages['username'] = "Anv-implijer";
$messages['smscrypttokensrequired'] = "Ret eo kaout an dibarzh crypt_tokens evit implijout ar fonktion SMS.";
$messages['smsuserfound'] = "Gwiriit eo reizh an titouroù amañ dindan ha klikit war Kas evit degemer ho kod-kadarnaat.";
$messages['smstoken'] = "Kod-kadarnaat";
$messages['getuser'] = "Kavout an implijer";
$messages['nophpmbstring'] = "Mat e vefe deoc'h staliañ PHP  mbstring";
$messages['menuquestions'] = "Question";
$messages['menutoken'] = "Mail";
$messages['menusms'] = "SMS";
$messages['nophpxml'] = "Vous devriez installer PHP XML pour utiliser cet outil";
$messages['tokenattempts'] = "Jedour didalvoud, klaskit adarre";
$messages['emptychangeform'] = "Cheñchit ho ker-tremen";
$messages['emptysendtokenform'] = "Degemerit ul liamm evit cheñch ho ker-tremen";
$messages['emptyresetbyquestionsform'] = "Adderaouit ho ker-tremen";
$messages['emptysetquestionsform'] = "Enrollit ho respont";
$messages['emptysendsmsform'] = "Degemerit ur c'hod-adderaouiñ";
$messages['sameaslogin'] = "Heñvel eo ho ker-tremen hag hoc'h anv-implijer";
$messages['policydifflogin'] = "Disheñvel diouzh hoc'h anv-implijer e rank bezañ ho ker-tremen";
$messages['changesshkeymessage'] = "Kevarc'h {login}, \n\nCheñchet eo bet hoc'h alc'hwez SSH. \n\nMa n'eo ket ganeoc'h-c'hwi eo bet goulennet, kit diouzhtu e darempred gant merour ho rouedad.";
$messages['menusshkey'] = "Alc'hwez SSH";
$messages['changehelpsshkey'] = "<a href=\"?action=changesshkey\">Cheñchit hoc'h alc'hwez SSH</a>";
$messages['sshkeychanged'] = "Cheñchet eo bet hoc'h alc'hwez SSH";
$messages['sshkeyrequired'] = "An alc'hwez SSH a rankit lakaat";
$messages['invalidsshkey'] = "An alc'hwez SSH-mañ a seblant bezañ didalvoud";
$messages['changesshkeysubject'] = "Cheñchet eo bet hoc'h alc'hwez SSH";
$messages['sshkey'] = "Alc'hwez SSH";
$messages['emptysshkeychangeform'] = "Cheñchit hoc'h alc'hwez SSH";
$messages['changesshkeyhelp'] = "Lakait ho ker-tremen hag an alc'hwez SSH nevez.";
$messages['sshkeyerror'] = "Nac'het eo bet an alc'hwez gant ar c'havlec'h LDAP";
$messages['pwned'] = "Siek eo ho ker-tremen nevez, mat e vefe deoc'h cheñch anezhañ e kement lec'h ec'h implijit anezhañ";
$messages['policypwned'] = "Arabat d'ho ker-tremen nevez bezañ anavezet e-barzh ur bon foran a c'herioù-tremen siek";
$messages['policydiffminchars'] = "Niver a arouezennoù o-unan d'an nebeutañ :";
$messages['diffminchars'] = "Re heñvel ouzh ho ker-tremen kozh eo an hini nevez";
$messages['specialatends'] = "Emañ arouezenn ispisial e-unan ho ker-tremen nevez e penn-kentañ pe er penn-diwezhañ";
$messages['policyspecialatends'] = "Arabat d'an arouezenn ispisial e-unan bezañ e penn-kentañ pe e fin ho ker-tremen nevez.";
$messages['checkdatabeforesubmit'] = "Gwiriit an titouroù, mar plij, araok kadarnaat ar furmenn";
$messages['forbiddenwords'] = "Gerioù difennet a zo en ho ker-stur";
$messages['policyforbiddenwords'] = "Arabat d'ho ker-tremen bezañ ennañ :";
$messages['forbiddenldapfields'] = "Ho ker-tremen a zo ennañ arroudoù eus ho antre LDAP";
$messages['policyforbiddenldapfields'] = "Arabat d'ho ker-tremen bezañ ennañ perzhioù eus hoc'h antre :";
$messages['ldap_cn'] = "anv klok";
$messages['ldap_givenName'] = "anv-bihan";
$messages['ldap_sn'] = "anv-familh";
$messages['ldap_mail'] = "chomlec'h postel";
$messages["questionspopulatehint"] = "Lakait hoc'h anv-implijer nemetken evit adkavout ar goulennoù ho peus enrollet.";
$messages['badquality'] = "N'eo ket pinvidik a-walc'h ho ker-tremen";
$messages['tooyoung'] = "Re nevez-cheñchet eo ho ker-tremen";
$messages['inhistory'] = "E-barzh roll ho kerioù-tremen kozh emañ ar ger-tremen-mañ";
$messages['throttle'] = "Re a daolioù-arnod dindan re verr amzer. Klaskit diwezhatoc'h (ma 'z eus un den ac'hanoc'h)";
