#!/bin/bash

SERV_DIR=$(cd $(dirname $0); pwd)
KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

cd $(dirname $0)
. "${DOCKERS_ENV}"
. "${KAZ_KEY_DIR}/env-${ldapServName}"
. "${KAZ_KEY_DIR}/env-${ldapUIName}"


checkDockerRunning "${ldapServName}" "LDAP" || exit

printKazMsg "\n  *** Premier lancement de LDAP : Mise en place"

LDAP_IP=$(docker inspect -f '{{.NetworkSettings.Networks.ldapNet.IPAddress}}' ldapServ)
MAIL_IP=$(docker inspect -f '{{.NetworkSettings.Networks.ldapNet.IPAddress}}' mailServ)

docker exec ${ldapUIName} bash -c "echo '${MAIL_IP} ${smtpHost}.${domain}' >> /etc/hosts"

BINDDN=cn=${LDAP_ADMIN_USERNAME},${ldap_root}
DC=$(echo ${ldap_root} | cut -d',' -f1 | cut -d'=' -f2)

for schema in `ls schemas/`
do
  ldapadd -H ldap://$LDAP_IP -D "cn=${LDAP_CONFIG_ADMIN_USERNAME},cn=config" -w ${LDAP_CONFIG_ADMIN_PASSWORD} -f schemas/${schema}
done

echo "dn: ${ldap_root}
objectClass: dcObject
objectClass: organization
dc: $DC
o: Kaz" | ldapadd -H ldap://$LDAP_IP -D "${BINDDN}" -w ${LDAP_ADMIN_PASSWORD}

./update.sh

CONFIG_IHM="${DOCK_VOL}/ldap_configSSP/_data/config.inc.php"

updateVarInConf(){
    # $1 key
    # $2 val
    # $3 file
    # $4 : vide => la valeur sera encadré par des guillement, sinon c'est du php
    if grep -q "^\s*\$$1" "$3" ; then
	echo "   update ${CYAN}${BOLD}$1${NC} => $2"
	# !!! les valeur ne doivent pas contenir le caractère '%'
	if [ -z "$4" ]; then
	    sed -i -e "s%^\s*\(\$$1\s*=\).*$%\1 \"$2\";%" "$3"
	else
	    sed -i -e "s%^\s*\(\$$1\s*=\).*$%\1 $2;%" "$3"
	fi
    else
	echo "   add ${CYAN}${BOLD}$1${NC} => $2"
	if [ -z "$4" ]; then
	    echo "\$$1 = \"$2\";" >> "$3"
	else
	    echo "\$$1 = $2;" >> "$3"
	fi
    fi
}

updateVarInConf "ldap_url" "${LDAPUI_URI}" "${CONFIG_IHM}"
updateVarInConf "ldap_binddn" "${LDAPUI_ADMIN_BIND_DN}" "${CONFIG_IHM}"
updateVarInConf "ldap_bindpw" "${LDAPUI_ADMIN_BIND_PWD}" "${CONFIG_IHM}"
updateVarInConf "ldap_base" "${LDAPUI_BASE_DN}" "${CONFIG_IHM}"
updateVarInConf "ldap_login_attribute" "cn" "${CONFIG_IHM}"
updateVarInConf "hash" "CRYPT" "${CONFIG_IHM}"
updateVarInConf "use_questions" "false" "${CONFIG_IHM}" "php"
updateVarInConf "mail_from" "admin@${domain}" "${CONFIG_IHM}"
updateVarInConf "mail_from_name" "Récupération de mot de passe Kaz" "${CONFIG_IHM}"
updateVarInConf "mail_smtp_host" "${smtpHost}.${domain}" "${CONFIG_IHM}"
updateVarInConf "use_sms" "false" "${CONFIG_IHM}" "php"
updateVarInConf "keyphrase" "${LDAPUI_PASSWORD}" "${CONFIG_IHM}"
updateVarInConf "lang" "fr" "${CONFIG_IHM}"
updateVarInConf "allowed_lang" "array('fr', 'br');" "${CONFIG_IHM}" "php"
updateVarInConf "mail_smtp_secure" "tls" "${CONFIG_IHM}"
updateVarInConf "mail_address_use_ldap" "true" "${CONFIG_IHM}"
updateVarInConf "mail_attributes" "array(\"mailDeSecours\", \"mail\")" "${CONFIG_IHM}" "php"
updateVarInConf "pwd_min_length" "10" "${CONFIG_IHM}"
updateVarInConf "pwd_min_special" "2" "${CONFIG_IHM}"
updateVarInConf "pwd_show_policy" "always" "${CONFIG_IHM}"
updateVarInConf "posthook" "/var/www/kaz/post-hook.sh" "${CONFIG_IHM}"
updateVarInConf "posthook_password_encodebase64" "true" "${CONFIG_IHM}"

# does not work
# docker cp "${KAZ_BIN_DIR}/look/kaz/kaz-tete.png" "${ldapUIName}:/var/www/html/images/ltb-logo.png"
