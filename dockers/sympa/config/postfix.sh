#!/bin/bash

postconf "postscreen_dnsbl_action = enforce"
postconf "smtpd_banner = ESMTP"
postconf "postscreen_dnsbl_sites = zen.spamhaus.org*3 bl.mailspike.net b.barracudacentral.org*2 bl.spameatingmonkey.net dnsbl.sorbs.net psbl.surriel.com list.dnswl.org=127.0.[0..255].0*-2 list.dnswl.org=127.0.[0..255].1*-3 list.dnswl.org=127.0.[0..255].[2..3]*-4"
postconf "postscreen_dnsbl_threshold = 3"
postconf "postscreen_dnsbl_whitelist_threshold = -1"
postconf "postscreen_greet_action = enforce"
postconf "postscreen_bare_newline_action = enforce"
postconf "smtputf8_enable = no"
postconf "smtpd_tls_cert_file=ENVCERT"
postconf "smtpd_tls_key_file=ENVKEY"
postconf "myhostname = MAILNAME.DOMAINNAME"
postconf "transport_maps = hash:/etc/postfix/transport"
postconf "message_size_limit = 1024000000"
postconf "slow_destination_concurrency_limit = 2"
postconf "slow_destination_recipient_limit = 20"
postconf "slow_destination_rate_delay = 5s"
postconf "slow_destination_concurrency_failed_cohort_limit=10"
postconf "veryslow_destination_concurrency_limit = 2"
postconf "veryslow_destination_recipient_limit = 2"
postconf "veryslow_destination_rate_delay = 10s"
postconf "veryslow_destination_concurrency_failed_cohort_limit=10"

postmap /etc/postfix/transport

grep "slow_destination" /etc/postfix/master.cf >/dev/null 2>&1
if [ "$?" -ne "0" ]
then
	echo -e "slow    unix    -       -       n       -       5       smtp\n \
	-o syslog_name=postfix-slow" \
	 >>/etc/postfix/master.cf
fi

grep "veryslow_destination" /etc/postfix/master.cf >/dev/null 2>&1
if [ "$?" -ne "0" ]
then
	echo -e "veryslow    unix    -       -       n       -       5       smtp\n \
	-o syslog_name=postfix-veryslow" \
	 >>/etc/postfix/master.cf
fi
