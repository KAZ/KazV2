#!/bin/bash

QUEUE=$(docker exec sympaServ mailq | grep "^[A-F0-9]")
COUNT=$(docker exec sympaServ mailq | grep -c "^[A-F0-9]")

if [ "$COUNT" -gt 4 ]; then
	echo $QUEUE
    echo "alerte mailq sympa : $COUNT"
fi
