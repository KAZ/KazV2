#!/bin/bash

FILTER=$(docker exec sympaServ cat /var/log/mail.err | grep filter.sh)
COUNT=$(docker exec sympaServ cat /var/log/mail.err | grep -c filter.sh)

if [ "$COUNT" -gt 2 ]; then
	echo $FILTER
    echo "alerte filter sympa : $COUNT"
fi
