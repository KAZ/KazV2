#!/bin/bash

KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

cd $(dirname $0)
. "${DOCKERS_ENV}"
. "${KAZ_KEY_DIR}/SetAllPass.sh"

DockerServName="${sympaServName}"

checkDockerRunning "${DockerServName}" "Sympa" || exit

printKazMsg "\n  *** Premier lancement de Sympa"

# docker exec "${DockerServName}" bash -c "DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends sympa || echo ok"
