# Didier le 28 avril 2021
# conf postfix ok
# sympa ne fonctionne pas en auto, lancer le contenu de la dernire ligne dans le container
# le fichier preseed contient l' installation en mysql, mais il faudra revenir dessus en lancant sympa_wizard
# ne pas oublier de verifier la fin du fichier sympa.conf qui doit contenir les lignes contenu dans le sympa.sh

# Fanch janvier 2022
# migration en bullseye, tentative de rendre générique
FROM debian:bullseye

########################################
# APT local cache
# work around because COPY failed if no source file
COPY .dummy .apt-mirror-confi[g] .proxy-confi[g] /
RUN cp /.proxy-config /etc/profile.d/proxy.sh 2> /dev/null || true
RUN if [ -f /.apt-mirror-config ] ; then . /.apt-mirror-config && sed -i \
    -e "s%s\?://deb.debian.org%://${APT_MIRROR_DEBIAN}%g" \
    -e "s%s\?://security.debian.org%://${APT_MIRROR_DEBIAN_SECURITY}%g" \
    -e "s%s\?://archive.ubuntu.com%://${APT_MIRROR_UBUNTU}%g" \
    -e "s%s\?://security.ubuntu.com%://${APT_MIRROR_UBUNTU_SECURITY}%g" \
    /etc/apt/sources.list; fi

########################################
RUN apt-get update \
    && apt-get install -y locales locales-all \
    && sed -i '/fr_FR.UTF-8/s/^# //g' /etc/locale.gen && locale-gen
ENV LC_ALL fr_FR.UTF-8
ENV LANG fr_FR.UTF-8
ENV LANGUAGE fr_FR:fr
RUN update-locale LANG=fr_FR.UTF-8 \
    && echo sympa >/etc/hostname

RUN apt-get -y install libboost-program-options-dev libboost-system-dev libboost-filesystem-dev libcurl4-gnutls-dev
#RUN apt-get -y install emacs elpa-php-mode
RUN apt-get -y install procps dos2unix

# creation du user filter,son repertoire home, copie des fichiers
RUN mkdir /home/filter ; useradd -d /home/filter filter ; chown filter /home/filter
RUN apt-get install -y --fix-missing doxygen dos2unix git \
    build-essential make g++ libboost-program-options-dev libboost-system-dev libboost-filesystem-dev libcurl4-gnutls-dev libssl-dev
WORKDIR /home/
RUN git clone https://git.kaz.bzh/KAZ/depollueur.git
WORKDIR /home/depollueur/
RUN make
RUN cp build/out/* /home/filter/
RUN cp src/bash/* /home/filter/

RUN chown filter /home/filter/*; chmod 755 /home/filter/*

# creation du repertoire filter et application des bons droits pour le filtre
RUN mkdir -p /var/log/mail; touch /var/log/mail/filter.log ; chown filter /var/log/mail/filter.log ; chmod 777 /var/log/mail/filter.log
RUN mkdir -p /var/spool/filter ; chmod 775 /var/spool/filter ; chown filter /var/spool/filter
RUN mkdir -p /var/log/mail/pb ; chmod a+rwx /var/log/mail/pb

COPY dockers/sympa/config/sympa.preseed /tmp/sympa.preseed
RUN debconf-set-selections < /tmp/sympa.preseed \
    && apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends sympa apache2 postfix supervisor anacron spawn-fcgi libapache2-mod-fcgid apg curl \
    && a2enmod rewrite ssl proxy_fcgi \
    && a2ensite default-ssl \
    && a2enconf sympa-soap \
    \
    && sed \
    -e 's%SSLCertificateKeyFile\s.*$%SSLCertificateKeyFile   ENVKEY%g' \
    -e 's%SSLCertificateFile\s.*$%SSLCertificateFile      ENVCERT%g' \
    -i /etc/apache2/sites-available/default-ssl.conf \
    && sed \
    -e '/ServerName/a	Redirect / https://MAILNAME.DOMAINNAME/' \
    -i /etc/apache2/sites-available/000-default.conf \
    && echo '<head>\n  <meta http-equiv="Refresh" content="0; URL=https://MAILNAME.DOMAINNAME/wws" />\n</head>\n' > /var/www/html/index.html \
    && sed -e 's%ScriptAlias.*%ProxyPass "/wws" "fcgi://localhost:8442/"%' -i /etc/apache2/conf-enabled/sympa.conf \
    && sed -e 's%ScriptAlias.*%ProxyPass "/sympasoap" "fcgi://localhost:8443/"%' -i /etc/apache2/conf-enabled/sympa-soap.conf

# pour le confort : modif du .bashrc de root
RUN sed -i 's/# alias/alias/g' /root/.bashrc \
    && sed -i 's/# export/export/g' /root/.bashrc \
    && sed -i 's/# eval/eval/g' /root/.bashrc

COPY dockers/sympa/config/supervisord.conf /etc/supervisord.conf

RUN touch /var/log/sympa.log \
    && chmod 640 /var/log/sympa.log \
    && echo "local1.* 		-/var/log/sympa.log" >>/etc/rsyslog.conf

# configuration de postfix
COPY dockers/sympa/config/transport /etc/postfix/
COPY dockers/sympa/config/postfix.sh /tmp
RUN bash /tmp/postfix.sh

# # modif des fichiers de postfix pour filter
RUN cat /home/filter/master.cf.update >>/etc/postfix/master.cf \
    && sed -i 's/^\(smtp .*smtpd\)$/\1\n\t-o content_filter=filter:dummy/' /etc/postfix/master.cf

# pour qu'admin@kaz.bzh recoivent les mails d'anomalie
RUN echo "root: ADMIN_EMAIL" >> /etc/aliases \
    && postalias hash:/etc/aliases

# Configuration de sympa
RUN echo aliases_program postalias >>/etc/sympa/sympa/sympa.conf \
    && echo sendmail /usr/sbin/sendmail >>/etc/sympa/sympa/sympa.conf \
    && echo soap_url /sympasoap >>/etc/sympa/sympa/sympa.conf \
    && echo dmarc_protection.mode dmarc_reject >>/etc/sympa/sympa/sympa.conf \
    && cp /usr/share/doc/sympa/examples/script/sympa_soap_client.pl.gz /usr/lib/sympa/bin/ \
    && gunzip /usr/lib/sympa/bin/sympa_soap_client.pl.gz \
    && chmod +x /usr/lib/sympa/bin/sympa_soap_client.pl \
    && chown sympa:sympa /usr/lib/sympa/lib/sympa/* \
    && chmod u+s /usr/lib/sympa/lib/sympa/*

COPY dockers/sympa/config/aliases.sympa.postfix /etc/sympa/aliases.sympa.postfix
COPY dockers/sympa/config/trusted_applications.conf /etc/sympa/trusted_applications.conf


RUN postconf "alias_maps = hash:/etc/aliases,hash:/etc/sympa/aliases.sympa.postfix,hash:/etc/mail/sympa/aliases" \
    && postconf "alias_database = hash:/etc/aliases,hash:/etc/sympa/aliases.sympa.postfix" \
    && postalias hash:/etc/sympa/aliases.sympa.postfix \
    && usermod -a -G sympa postfix

COPY dockers/sympa/config/postfix-wrapper.sh /usr/local/bin/
RUN chmod a+x /usr/local/bin/postfix-wrapper.sh

COPY dockers/sympa/config/start.sh /
ENTRYPOINT ["/bin/bash", "/start.sh"]

EXPOSE 80 443 25

# docs sympa : https://listes.renater.fr/sympa/arc/sympa-fr/2021-02/msg00026.html
# https://listes.renater.fr/sympa/arc/sympa-fr/2012-05/msg00042.html
