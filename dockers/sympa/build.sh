#!/bin/bash

KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

printKazMsg "\n  *** Création du Dockerfile Sympa"

cd "${KAZ_ROOT}"
docker build --no-cache -t sympakaz . -f dockers/sympa/Dockerfile
