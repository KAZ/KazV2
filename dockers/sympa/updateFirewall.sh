#!/bin/bash
# à lancer sur l'hôte pour paramétrer iptables
# ça peut brailler car certaines choses devraient être faites une et une seule fois, mais ce script peut être réappelé à chaque lancement du docker.

#cleaning, may throw errors at first launch
#iptables -t nat -D POSTROUTING -o ens18 -j ipbis
#iptables -t nat -F ipbis
#iptables -t nat -X ipbis

iptables -t nat -N ipbis
iptables -t nat -F ipbis
iptables -t nat -I ipbis -o ens18 -p tcp --source `docker inspect -f '{{.NetworkSettings.Networks.sympaNet.IPAddress}}' sympaServ` -j SNAT --to `ifconfig ens18:0 | grep "inet" | awk '{print $2}'`
iptables -t nat -I ipbis -o ens18 -p tcp --source `docker inspect -f '{{.NetworkSettings.Networks.jirafeauNet.IPAddress}}' sympaServ` -j SNAT --to `ifconfig ens18:0 | grep "inet" | awk '{print $2}'`
#add by fab mais non testé 'assque chu pas fou !
#iptables -t nat -I ipbis -o ens18 -p tcp --source `docker inspect -f '{{.NetworkSettings.Networks.apikazNet.IPAddress}}' sympaServ` -j SNAT --to `ifconfig ens18:0 | grep "inet" | awk '{print $2}'`
iptables -t nat -A ipbis -j RETURN
iptables -t nat -D POSTROUTING -o ens18 -j ipbis
iptables -t nat -I POSTROUTING -o ens18 -j ipbis
