#!/bin/bash

SERV_DIR=$(cd $(dirname $0); pwd)
KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

mkdir -p "${KAZ_DNLD_DIR}/dokuwiki"
cd "${KAZ_DNLD_DIR}/dokuwiki"

printKazMsg "\n  *** Download dokuwiki on ${KAZ_DNLD_DIR}"

downloadFile https://github.com/splitbrain/dokuwiki-plugin-captcha/zipball/master captcha.zip
downloadFile https://github.com/turnermm/ckgedit/archive/current.zip ckgedit.zip
downloadFile https://github.com/splitbrain/dokuwiki-plugin-smtp/zipball/master smtp.zip
downloadFile https://github.com/leibler/dokuwiki-plugin-todo/archive/stable.zip todo.zip
downloadFile https://github.com/selfthinker/dokuwiki_plugin_wrap/archive/stable.zip wrap.zip
downloadFile http://github.com/practical-solutions/dokuwiki-plugin-wrapadd/zipball/master wrapadd.zip
downloadFile https://github.com/Vincent31Fr/docnavwiki-template/zipball/master docnavwiki.zip
