#!/bin/bash

SERV_DIR=$(cd $(dirname $0); pwd)
KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

cd "${SERV_DIR}"
CONF_FILE="config/config.local.php"
touch "${CONF_FILE}"
chown 33:33 "${CONF_FILE}"

SRC_JIR="https://gitlab.com/mojo42/Jirafeau.git"
JIR_VER="4.3.0"

"${KAZ_BIN_DIR}/installDepollueur.sh"

printKazMsg "\n  *** Création du Dockerfile Jirafeau"

printKazMsg "\n  - GIT Jirafeau "
cd "${KAZ_GIT_DIR}"
if [ ! -d "Jirafeau" ]; then
    git clone "${SRC_JIR}" --branch ${JIR_VER}
fi

cd "${KAZ_GIT_DIR}/Jirafeau" && git reset --hard && git checkout ${JIR_VER}

printKazMsg "\n  - Dockefile"
cd "${KAZ_ROOT}"
# Pour permettre la copy de git il faut que le répertoire soit visible de la racine qui lance la construction
docker build -t filekaz . -f dockers/jirafeau/Dockerfile

