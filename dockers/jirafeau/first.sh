#!/bin/bash

KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

JIR_VER=4.5.0

cd $(dirname $0)
. "${DOCKERS_ENV}"
. "${KAZ_KEY_DIR}/env-${jirafeauServName}"

CONF_FILE="${DOCK_VOL}/jirafeau_config/_data/config.local.php"

if ! grep -q  "'installation_done'\s*=>\s*true" "${CONF_FILE}" 2>/dev/null ; then
    printKazMsg "\n  *** Premier lancement de Jirafeau"

    checkDockerRunning "${jirafeauServName}" "Jirafeau" || exit
    curl -X POST \
	 -d "jirafeau=${JIR_VER}" \
	 -d "step=1" \
	 -d "admin_password=${HTTPD_PASSWORD}"  \
	 -d "next=1" \
	 "${httpProto}://${fileHost}.${domain}/install.php"
    curl -X POST -d "jirafeau=${JIR_VER}" \
	 -d "step=2" \
	 -d "web_root=${httpProto}://${fileHost}.${domain}/" \
	 -d "var_root=${jirafeauDir}" \
	 -d "next=1" \
	 "${httpProto}://${fileHost}.${domain}/install.php"
fi

updatePhpVar(){
    # $1 key
    # $2 val
    # $3 file
    if grep -q "$1" "$3" ; then
	sed -i \
	    -e "s%\([\"']$1[\"']\s*=>\s*\)[^,]*,%\1$2,%" \
	    "$3"
    fi
}

updatePhpVar "style" "'kaz'" "${CONF_FILE}"
updatePhpVar "organisation" "'KAZ'" "${CONF_FILE}"

#updatePhpVar "web_root" "'${httpProto}://${fileHost}.${domain}/'" "${CONF_FILE}"
#updatePhpVar "admin_password" "'$(echo -n "${HTTPD_PASSWORD}" | sha256sum | awk '{print $1}')'" "${CONF_FILE}"
#updatePhpVar "var_root" "'${jirafeauDir}'" "${CONF_FILE}"
#updatePhpVar "installation_done" "true" "${CONF_FILE}"
