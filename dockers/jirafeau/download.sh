#!/bin/bash

SERV_DIR=$(cd $(dirname $0); pwd)
KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

SRC_JIR="https://gitlab.com/mojo42/Jirafeau.git"
JIR_VER="4.5.0"

printKazMsg "\n  *** Download Jirafeau"

mkdir -p "${KAZ_GIT_DIR}"
cd "${KAZ_GIT_DIR}"
if [ ! -d "Jirafeau" ]; then
    git clone "${SRC_JIR}" --branch ${JIR_VER}
fi

cd "${KAZ_GIT_DIR}/Jirafeau"
if [ -z "$(git branch | grep "${JIR_VER}")" ]; then
    printKazMsg "  checkout branch ${JIR_VER}"
    git fetch -a
    git reset --hard
    git checkout ${JIR_VER}
fi
