#!/bin/bash

# limitation du filter.sh
docker exec jirafeauServ bash -c "cp /var/jirafeauData/*/20241109/*.json /var/jirafeau/lib/locales/"
docker exec jirafeauServ bash -c "cp /var/jirafeauData/*/20241109/*.php /var/jirafeau/"
docker exec jirafeauServ bash -c "mv /var/jirafeau/settings.php /var/jirafeau/lib/"
docker exec jirafeauServ bash -c "mv /var/jirafeau/functions.js.php /var/jirafeau/lib/"
