#!/bin/bash

KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars
. "${DOCKERS_ENV}"
. $KAZ_ROOT/secret/SetAllPass.sh


${KAZ_BIN_DIR}/gestContainers.sh --install -M -cloud
