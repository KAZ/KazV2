#!/bin/bash

docker exec --user www-data -ti nextcloudServ bash -c "/var/www/html/occ db:add-missing-indices"
