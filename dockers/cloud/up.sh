#!/bin/bash

KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars
. "${DOCKERS_ENV}"
. $KAZ_ROOT/secret/SetAllPass.sh


#"${KAZ_BIN_DIR}/initCloud.sh"

docker exec -ti -u 33 nextcloudServ /var/www/html/occ app:enable user_ldap
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:delete-config s01
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:create-empty-config
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapAgentName cn=cloud,ou=applications,${ldap_root}
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapAgentPassword ${ldap_LDAP_CLOUD_PASSWORD}
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapAgentPassword ${ldap_LDAP_CLOUD_PASSWORD}
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapBase ${ldap_root}
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapBaseGroups ${ldap_root}
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapBaseUsers ou=users,${ldap_root}
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapExpertUsernameAttr identifiantKaz
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapHost ${ldapServName}
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapPort 389
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapTLS 0
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapLoginFilter "(&(objectclass=nextcloudAccount)(|(cn=%uid)(identifiantKaz=%uid)))"
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapQuotaAttribute nextcloudQuota
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapUserFilter "(&(objectclass=nextcloudAccount)(nextcloudEnabled=TRUE))"
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapUserFilterObjectclass nextcloudAccount
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapEmailAttribute mail
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapUserDisplayName cn
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapUserFilterMode 1
docker exec -ti -u 33 nextcloudServ /var/www/html/occ ldap:set-config s01 ldapConfigurationActive 1

# Dans le mariadb, pour permettre au ldap de reprendre la main : delete from oc_users where uid<>'admin';
# docker exec -i nextcloudDB mysql --user=<user> --password=<password> <db> <<< "delete from oc_users where uid<>'admin';"

# Doc : https://help.nextcloud.com/t/migration-to-ldap-keeping-users-and-data/13205

# Exemple de table/clés :
# +-------------------------------+----------------------------------------------------------+
# | Configuration                 | s01                                                      |
# +-------------------------------+----------------------------------------------------------+
# | hasMemberOfFilterSupport      | 0                                                        |
# | homeFolderNamingRule          |                                                          |
# | lastJpegPhotoLookup           | 0                                                        |
# | ldapAgentName                 | cn=cloud,ou=applications,dc=kaz,dc=sns                   |
# | ldapAgentPassword             | ***                                                      |
# | ldapAttributesForGroupSearch  |                                                          |
# | ldapAttributesForUserSearch   |                                                          |
# | ldapBackgroundHost            |                                                          |
# | ldapBackgroundPort            |                                                          |
# | ldapBackupHost                |                                                          |
# | ldapBackupPort                |                                                          |
# | ldapBase                      | ou=users,dc=kaz,dc=sns                                   |
# | ldapBaseGroups                | ou=users,dc=kaz,dc=sns                                   |
# | ldapBaseUsers                 | ou=users,dc=kaz,dc=sns                                   |
# | ldapCacheTTL                  | 600                                                      |
# | ldapConfigurationActive       | 1                                                        |
# | ldapConnectionTimeout         | 15                                                       |
# | ldapDefaultPPolicyDN          |                                                          |
# | ldapDynamicGroupMemberURL     |                                                          |
# | ldapEmailAttribute            | mail                                                     |
# | ldapExperiencedAdmin          | 0                                                        |
# | ldapExpertUUIDGroupAttr       |                                                          |
# | ldapExpertUUIDUserAttr        |                                                          |
# | ldapExpertUsernameAttr        | uid                                                      |
# | ldapExtStorageHomeAttribute   |                                                          |
# | ldapGidNumber                 | gidNumber                                                |
# | ldapGroupDisplayName          | cn                                                       |
# | ldapGroupFilter               |                                                          |
# | ldapGroupFilterGroups         |                                                          |
# | ldapGroupFilterMode           | 0                                                        |
# | ldapGroupFilterObjectclass    |                                                          |
# | ldapGroupMemberAssocAttr      |                                                          |
# | ldapHost                      | ldap                                                     |
# | ldapIgnoreNamingRules         |                                                          |
# | ldapLoginFilter               | (&(|(objectclass=nextcloudAccount))(cn=%uid))            |
# | ldapLoginFilterAttributes     |                                                          |
# | ldapLoginFilterEmail          | 0                                                        |
# | ldapLoginFilterMode           | 0                                                        |
# | ldapLoginFilterUsername       | 1                                                        |
# | ldapMatchingRuleInChainState  | unknown                                                  |
# | ldapNestedGroups              | 0                                                        |
# | ldapOverrideMainServer        |                                                          |
# | ldapPagingSize                | 500                                                      |
# | ldapPort                      | 389                                                      |
# | ldapQuotaAttribute            | nextcloudQuota                                           |
# | ldapQuotaDefault              |                                                          |
# | ldapTLS                       | 0                                                        |
# | ldapUserAvatarRule            | default                                                  |
# | ldapUserDisplayName           | cn                                                       |
# | ldapUserDisplayName2          |                                                          |
# | ldapUserFilter                | (&(objectclass=nextcloudAccount)(nextcloudEnabled=TRUE)) |
# | ldapUserFilterGroups          |                                                          |
# | ldapUserFilterMode            | 1                                                        |
# | ldapUserFilterObjectclass     | nextcloudAccount                                         |
# | ldapUuidGroupAttribute        | auto                                                     |
# | ldapUuidUserAttribute         | auto                                                     |
# | turnOffCertCheck              | 0                                                        |
# | turnOnPasswordChange          | 0                                                        |
# | useMemberOfToDetectMembership | 1                                                        |
# +-------------------------------+----------------------------------------------------------+
