Pour l'installation de Nextcloud

Documentation:
https://registry.hub.docker.com/_/nextcloud?tab=description
https://registry.hub.docker.com/_/mariadb?tab=description
https://blog.ssdnodes.com/blog/installing-nextcloud-docker/

____________________________________________________________
Contenu du répertoire

____________________________________________________________
Se placer dans le bon répertoire

# cd /docker/cloud

____________________________________________________________
Lancement de nextcloud et nextcloudDB

# docker-compose up -d

____________________________________________________________
Vérification
Il y a des containers qui tournent cloudServ cloudDB (collabraServ)

# docker ps

# docker exec -ti cloudServ bash
exit

# docker exec -ti cloudDB bash
exit

____________________________________________________________
Personalisation

Il faut attendre 2 minutes pour le lancement

Pour mettre en français
emacs /var/lib/docker/volumes/cloud_cloudConfig/_data/config.php
il faut ajouter :
"default_language" => "fr",

Création des comptes.
Application
 * Tasks
 * Calendar
 * Desk
 * Contact
 * Mail
 * Talk
 * Draw.io
 


 * Collabora Online - Built-in CODE Server (il faut un port d'écoute)
apt update
apt install sudo
sudo -u www-data php -d memory_limit=512M ./occ app:install richdocumentscode 
sudo -u www-data php -d memory_limit=512M ./occ app:update --all

ou

installer un docker collabra et
apt update
apt install sudo
sudo -u www-data ./occ config:app:set --value http://89.234.186.106:9980/ richdocuments wopi_url
sudo -u www-data ./occ richdocuments:activate-config

https://cloud.kaz.bzh/settings/admin/richdocuments

____________________________________________________________
Mettre à jour le mot de passe dans /kaz/secret

____________________________________________________________
Test
Y a plus qu'a tester
http://kaz.bzh:8080

____________________________________________________________
Traces
https://cloud.kaz.bzh/index.php/settings/admin/logging

____________________________________________________________
Pour la sauvegarde il faut également des scripts

Didier le 11/12/2020
installation du module RainLoop pour les mails
mot passe de l' admin : voir dans /kaz/secret/SetAllPass.sh
le module a viré pas de custom_apps dans le docker-compose
proposition de modif du docker-compose.yml mis en commentaires.
