#!/bin/bash

KAZ_ROOT=$(cd "$(dirname $0)/../.."; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

printKazMsg "\n  *** Création du Dockerfile paheko"

cd "${KAZ_ROOT}"

docker build -t pahekokaz . -f dockers/paheko/Dockerfile
