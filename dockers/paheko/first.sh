#!/bin/bash

SERV_DIR=$(cd $(dirname $0); pwd)
KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

cd $(dirname $0)
. "${DOCKERS_ENV}"


if ! [[ "$(docker ps -f name=${pahekoServName} | grep -w ${pahekoServName})" ]]; then
    echo -e "Paheko not running... abort"
    exit
fi

printKazMsg "\n  *** Premier lancement de Paheko"

docker exec "${pahekoServName}" /usr/share/paheko/setupWebRights.sh
