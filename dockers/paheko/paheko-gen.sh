#!/bin/bash

KAZ_ROOT=$(cd "$(dirname $0)/../.."; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

TMPL=config/config.local.tmpl.php
CONF=config/config.local.php

cd $(dirname $0)

"${APPLY_TMPL}" "${TMPL}" "${CONF}"
