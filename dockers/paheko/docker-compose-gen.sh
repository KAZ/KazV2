#!/bin/bash

#koi : recréer le docker-compose.yml de paheko en insérant les routes traefik pour toutes les orga qui ont un paheko 
#kan : 04/12/2023
#ki  : fab

KAZ_ROOT=$(cd "$(dirname $0)/../.."; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

file="docker-compose.yml"

#je construis les labels
TMPFILE=$(mktemp /tmp/paheko-XXXXX)
echo "      - \"traefik.docker.network=pahekoNet\"" > $TMPFILE
echo "      - \"traefik.enable=true\"" >> $TMPFILE
echo "      - \"traefik.frontend.rule=HostRegexp:{host:[a-zA-Z0-9_-]+-${pahekoHost}}.${domain}\"" >> $TMPFILE
for orga in $(getPahekoOrgaList); do
  echo "      - \"traefik.http.routers."$orga".rule=Host(\`"$orga"-paheko.kaz.bzh\`)\"" >> $TMPFILE
done

#je remet TMPFILE entre labels: et volumes: 
awk '/labels:/,/volumes:/ {if (/labels:/) {p=1; print; next} else if (p && /volumes:/) {p=0; system("cat '$TMPFILE'"); print; next} else if (p) {next} } 1' "$file" > "$file.tmp" && mv "$file.tmp" "$file"

rm $TMPFILE
