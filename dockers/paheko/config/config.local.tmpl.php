<?php

namespace Paheko;

/**
 * Ce fichier permet de configurer Garradin pour une utilisation
 * avec plusieurs associations, mais une seule copie du code source.
 * (aussi appel?? installation multi-sites, ferme ou usine)
 *
 * Voir la doc : https://fossil.kd2.org/paheko/wiki?name=Multi-sites
 *
 * N'oubliez pas d'installer ??galement le script cron.sh fournit
 * pour lancer les rappels automatiques et sauvegardes.
 *
 * Si cela ne suffit pas ?? vos besoins, contactez-nous : https://paheko.eu/contact
 * pour une aide sp??cifique ?? votre installation.
 */

// Nom de domaine parent des associations h??berg??es
// Exemple : si vos associations sont h??berg??es en clubdetennis.paheko.eu,
// indiquer ici 'paheko.eu'
const FACTORY_DOMAIN = "__DOMAIN__";

// R??pertoire o?? seront stock??es les donn??es des utilisateurs
// Dans ce r??pertoire, un sous-r??pertoire sera cr???? pour chaque compte
// Ainsi 'clubdetennis.paheko.eu' sera dans le r??pertoire courant (__DIR__),
// sous-r??pertoire 'users' et dans celui-ci, sous-r??pertoire 'clubdetennis'
//
// Pour chaque utilisateur il faudra cr??er le sous-r??pertoire en premier lieu
// (eg. mkdir .../users/clubdetennis)
const FACTORY_USER_DIRECTORY = __DIR__ . '/users';

// Envoyer les erreurs PHP par mail ?? l'adresse de l'administrateur syst??me
// (mettre ?? null pour ne pas recevoir d'erreurs)
const MAIL_ERRORS = 'admin@kaz.bzh';

// IMPORTANT !
// Modifier pour indiquer une valeur al??atoire de plus de 30 caract??res
const SECRET_KEY = 'HkXFwmMIMaI1T4X9/BIxKahxa74tQvAj0z1keal/jXj7i2w1ifzvalPSAshAbSg2P/fhmh9TlA3+gD28jg+ljA==';

// Quota de stockage de documents (en octets)
// D??finit la taille de stockage disponible pour chaque association pour ses documents
const FILE_STORAGE_QUOTA = 10 * 1024 * 1024 * 1024; // 10 Go

////////////////////////////////////////////////////////////////
// R??glages conseill??s, normalement il n'y a rien ?? modifier ici

// Indiquer que l'on va utiliser cron pour lancer les t??ches ?? ex??cuter (envoi de rappels de cotisation)
const USE_CRON = true;

// Cache partag??
const SHARED_CACHE_ROOT = __DIR__ . '/cache';

// D??sactiver le log des erreurs PHP visible dans l'interface (s??curit??)
const ENABLE_TECH_DETAILS = false;

// D??sactiver les mises ?? jour depuis l'interface web
// Pour ??tre s??r que seul l'admin sys puisse faire des mises ?? jour
const ENABLE_UPGRADES = false;

////////////////////////////////////////////////////////////////
// Code 'magique' qui va configurer Garradin selon les r??glages

$login = null;

// Un sous-domaine ne peut pas faire plus de 63 caract??res
$login_regexp = '([a-z0-9_-]{1,63})';
$domain_regexp = sprintf('/^%s\.%s$/', $login_regexp, preg_quote(FACTORY_DOMAIN, '/'));


//original
//if (isset($_SERVER['SERVER_NAME']) && preg_match($regexp, $_SERVER['SERVER_NAME'], $match)) {
//maj kaz
if (isset($_SERVER['SERVER_NAME']) && preg_match('/^([a-z0-9_\-]+)-paheko\.__DOMAIN__$/', $_SERVER['SERVER_NAME'], $match)) {
        $login = $match[1];
}
elseif (PHP_SAPI == 'cli' && !empty($_SERVER['PAHEKO_FACTORY_USER']) && preg_match('/^' . $login_regexp . '$/', $_SERVER['PAHEKO_FACTORY_USER'])) {
        $login = $_SERVER['PAHEKO_FACTORY_USER'];
}
else {
        // Login invalide ou non fourni
        http_response_code(404);
        die('<h1>Page non trouv??e</h1>');
}

$user_data_dir = rtrim(FACTORY_USER_DIRECTORY, '/') . '/' . $login;


if (!is_dir($user_data_dir)) {
        mkdir($user_data_dir, 0700, true);
        //http_response_code(404);
        //die("<h1>Cette association n'existe pas.</h1>");
}

// D??finir le dossier o?? sont stock??s les donn??es
define('Paheko\DATA_ROOT', $user_data_dir);

const PREFER_HTTPS = false;
const SMTP_HOST =  "__SMTP_HOST__.__DOMAIN__";
const SMTP_USER = null;
const API_USER =  "__PAHEKO_API_USER__";
const API_PASSWORD = "__PAHEKO_API_PASSWORD__";
const SMTP_PASSWORD = null;
const SMTP_PORT = 25;
const SMTP_SECURITY = 'NONE';
const ROOT = __DIR__;

const DB_FILE = DATA_ROOT . '/association.sqlite';
//const PLUGINS_ROOT = DATA_ROOT . '/plugins';
const PLUGINS_ROOT =  __DIR__ . '/data/plugins';

// D??finir l'URL
//original
//define('Garradin\WWW_URL', 'https://' . $login . FACTORY_USER_DIRECTORY . '/');
//maj kaz
define('Paheko\WWW_URL', "__HTTP_PROTO__://".$login."-paheko.__DOMAIN__/");

define('Paheko\WWW_URI', '/');

// Désactiver le log des erreurs visible dans l'interface (sécurité)
define('Paheko\ERRORS_ENABLE_LOG_VIEW', true);

// Ne pas afficher les erreurs de code
define('Paheko\SHOW_ERRORS', true);


#add by fab le 21/04/2022
//const PDF_COMMAND = 'prince';
# const PDF_COMMAND = 'auto';
const PDF_COMMAND = 'chromium --no-sandbox --headless --disable-dev-shm-usage --autoplay-policy=no-user-gesture-required --no-first-run --disable-gpu --disable-features=DefaultPassthroughCommandDecoder --use-fake-ui-for-media-stream --use-fake-device-for-media-stream --disable-sync --print-to-pdf=%2$s %1$s';
