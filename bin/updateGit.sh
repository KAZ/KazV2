#!/bin/bash

# l'idée et de faire un rsync dans un répertoir provisoire et de téléverser les différences.

# initialilisation :
# cd /MonRepDeTest
# mkdir -p kazdev kazprod
# rsync -rlptDEHAX --delete --info=progress2 root@kazdev:/kaz/ ./kazdev/
# rsync -rlptDEHAX --delete --info=progress2 root@kazprod:/kaz/ ./kazprod/

# exemple :
# cd /MonRepDeTest/kazdev/
# ./dockers/rdiff.sh /MonRepDeTest/kazprod/ root@kazprod
# cd /MonRepDeTest/kazprod/
# ./dockers/rdiff.sh /MonRepDeTest/kazdev/ root@kazdev

export KAZ_ROOT=$(cd "$(dirname $0)/.."; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

export REF_DIRS="bin config dockers secret.tmpl"
export SIMU=""

usage () {
    echo "Usage: $0 [-n] [-h]"
    echo "  -h help"
    echo "  -n simulation"
    exit 1
}

for ARG in $@
do
    case "${ARG}" in
	'-h' | '-help' )
	    usage
	    ;;
	'-n' )
	    shift
	    export SIMU="echo"
	    ;;
    esac
done

if [[ $# -ne 0 ]]; then
    echo "Illegal number of parameters"
    usage
fi

########################################
# check system

for prg in git ssh rsync kompare; do
    if ! type "${prg}" > /dev/null; then
	printKazError "$0 need ${prg}"
	echo "please run \"apt-get install ${prg}\""
	exit
    fi
done


########################################
# config

declare -a SKIP_FILE
export SKIP_FILE=$(grep -v -e ^# -e ^$ "${KAZ_CONF_DIR}/skip-file.txt")
KAZ_CFG_UPDATED=""
KAZ_UPDATE_ENV="${KAZ_CONF_DIR}/updateGit.conf"
if [ -f "${KAZ_UPDATE_ENV}" ]; then
    . "${KAZ_UPDATE_ENV}"
else
    KAZ_SRC_TYPE="VAGRANT"
    KAZ_VAGRANT_ROOT="~/kaz-vagrant"
    KAZ_DEV_REMOTE="root@kazdev"
    KAZ_DEV_ROOT="/kaz"
    KAZ_PROD_REMOTE="root@kazprod"
    KAZ_PROD_ROOT="/kaz"
    KAZ_OTHER_REMOTE="192.168.1.1"
    KAZ_OTHER_ROOT="~/git/kaz"
fi

while : ; do
    read -p "    Form which tested server ([Vagrant|DEV|PROD|OTHER]) you want updaye git KAZ? [${KAZ_SRC_TYPE}]: " rep
    case "${rep}" in
	"")
	    break
	    ;;
	[vV]*)
	    KAZ_SRC_TYPE="VAGRANT"
	    KAZ_CFG_UPDATED="true"
	    break
	    ;;
	[dD]*)
	    KAZ_SRC_TYPE="DEV"
	    KAZ_CFG_UPDATED="true"
	    break
	    ;;
	[pP]*)
	    KAZ_SRC_TYPE="PROD"
	    KAZ_CFG_UPDATED="true"
	    break
	    ;;
	[oO]*)
	    KAZ_SRC_TYPE="OTHER"
	    KAZ_CFG_UPDATED="true"
	    break
	    ;;
        * )
	    printKazError "\"${rep}\" not match with [Vagrant|DEV|PROD|OTHER]."
	    ;;
    esac
done

case "${KAZ_SRC_TYPE}" in
    VAGRANT)
	while : ; do
	    read -p "    Give kaz-vagrant root? [${KAZ_VAGRANT_ROOT}]: " vagrantPath
	    if [ -z "${vagrantPath}" ]; then
		vagrantPath="${KAZ_VAGRANT_ROOT}"
	    else
		KAZ_CFG_UPDATED="true"
	    fi
	    if [ ! -d "${vagrantPath/#\~/${HOME}}" ]; then
		printKazError "${vagrantPath} doesn't exist"
		continue
	    fi
	    KAZ_VAGRANT_ROOT="${vagrantPath}"
	    KAZ_VAGRANT_PAHT="$(cd "${vagrantPath/#\~/${HOME}}" 2>/dev/null; pwd)"
	    (for sign in .git .vagrant; do
		 if [ ! -d "${KAZ_VAGRANT_PAHT}/${sign}" ]; then
		     printKazError "${KAZ_VAGRANT_PAHT} not contains ${sign}"
		     exit 1
		 fi
	     done
	     exit 0
	    ) && break;
	done
	;;
    DEV|PROD|OTHER)
	case "${KAZ_SRC_TYPE}" in
	    DEV)
		remoteUser="${KAZ_DEV_REMOTE}"
		remotePath="${KAZ_DEV_ROOT}"
		;;
	    PROD)
		remoteUser="${KAZ_PROD_REMOTE}"
		remotePath="${KAZ_PROD_ROOT}"
		;;
	    OTHER)
		remoteUser="${KAZ_OTHER_REMOTE}"
		remotePath="${KAZ_OTHER_ROOT}"
		;;
	esac
	while : ; do
	    read -p "Give remote access? [${remoteUser}]: " rep
	    case "${rep}" in
		"" )
		    break
		    ;;
		* )
		    if [[ "${rep}" =~ ^([a-zA-Z0-9._%+-]+@)?[a-zA-Z0-9.-]+$ ]]; then
			remoteUser="${rep}"
			break
		    else
			printKazError "${rep} not match with [user@server]"
		    fi
		    ;;
	    esac
	done
	while : ; do
	    read -p "Give remote path? [${remotePath}]: " rep
	    case "${rep}" in
		"" )
		    break
		    ;;
		* )
		    if [[ "${rep}" =~ ^~?[a-zA-Z0-9/._-]*/$ ]]; then
			remotePath="${rep}"
			break
		    else
			printKazError "${rep} not match with [path]"
		    fi
		    ;;
	    esac
	done
	case "${KAZ_SRC_TYPE}" in
	    DEV)
		if [ "${remoteUser}" != "${KAZ_DEV_REMOTE}" ]; then
		    KAZ_DEV_REMOTE="${remoteUser}"; KAZ_CFG_UPDATED="true"
		fi
		if [ "${remotePath}" != "${KAZ_DEV_ROOT}" ]; then
		    KAZ_DEV_ROOT="${remotePath}"; KAZ_CFG_UPDATED="true"
		fi
		;;
	    PROD)
		if [ "${remoteUser}" != "${KAZ_PROD_REMOTE}" ]; then
		    KAZ_PROD_REMOTE="${remoteUser}"; KAZ_CFG_UPDATED="true"
		fi
		if [ "${remotePath}" != "${KAZ_PROD_ROOT}" ]; then
		    KAZ_PROD_ROOT="${remotePath}"; KAZ_CFG_UPDATED="true"
		fi
		;;
	    OTHER)
		if [ "${remoteUser}" != "${KAZ_OTHER_REMOTE}" ]; then
		    KAZ_OTHER_REMOTE="${remoteUser}"; KAZ_CFG_UPDATED="true"
		fi
		if [ "${remotePath}" != "${KAZ_OTHER_ROOT}" ]; then
		    KAZ_OTHER_ROOT="${remotePath}"; KAZ_CFG_UPDATED="true"
		fi
		;;
	esac
	;;
esac

if [ -n "${KAZ_CFG_UPDATED}" ]; then
    printKazMsg "Update ${KAZ_UPDATE_ENV}"
    cat > "${KAZ_UPDATE_ENV}" <<EOF
KAZ_SRC_TYPE="${KAZ_SRC_TYPE}"
KAZ_VAGRANT_ROOT="${KAZ_VAGRANT_ROOT}"
KAZ_DEV_REMOTE="${KAZ_DEV_REMOTE}"
KAZ_DEV_ROOT="${KAZ_DEV_ROOT}"
KAZ_PROD_REMOTE="${KAZ_PROD_REMOTE}"
KAZ_PROD_ROOT="${KAZ_PROD_ROOT}"
KAZ_OTHER_REMOTE="${KAZ_OTHER_REMOTE}"
KAZ_OTHER_ROOT="${KAZ_OTHER_ROOT}"
EOF
fi

########################################
# check git/kaz

cd "${KAZ_ROOT}"
CURRENT_BRANCH="$(git branch | grep "*")"
if [ "${CURRENT_BRANCH}" == "* develop" ]; then
    printKazMsg "You are on ${CURRENT_BRANCH}."
else
    printKazError "You supposed to be on * develop"
    checkContinue
fi

if [ "$(git status | grep "git restore")" ]; then
    echo "You didn't commit previous change."
    checkContinue
    
fi

########################################
# download valide source from Vagrant, DEV, PROD or OTHER

export TESTED_DIR="${KAZ_ROOT}/tmp/kaz"
mkdir -p "${TESTED_DIR}"

printKazMsg "Download from ${KAZ_SRC_TYPE} to ${TESTED_DIR}"
checkContinue
case "${KAZ_SRC_TYPE}" in
    VAGRANT)
	(
	    echo "check vagrant status (must be launch with vagrant)"
	    cd "${KAZ_VAGRANT_ROOT/#\~/${HOME}}"
	    while :; do
		if [ -n "$(vagrant status | grep running)" ]; then
		    exit
		fi
		printKazMsg "Please start vagrant"
		checkContinue
	    done
	)

	printKazMsg check key
	while grep -q "@@@@@@@@@@" <<<"$(ssh -p 2222 -i ${KAZ_VAGRANT_ROOT/#\~/${HOME}}/.vagrant/machines/default/virtualbox/private_key vagrant@127.0.0.1 date 2>&1 >/dev/null)"; do
	    printKazError "ssh key has changed"
	    echo "you must call :"
	    echo "${YELLOW} ssh-keygen -f ~/.ssh/known_hosts -R \"[127.0.0.1]:2222\"${NC}"
	    checkContinue
	done
	# XXX remote root
	${SIMU} rsync -rlptDEHAX --no-o --delete --info=progress2 \
		-e "ssh -p 2222 -i ${KAZ_VAGRANT_ROOT/#\~/${HOME}}/.vagrant/machines/default/virtualbox/private_key" \
		$(for i in ${REF_DIRS} git download ; do echo "vagrant@127.0.0.1:/kaz/$i" ; done) \
		"${TESTED_DIR}"
	;;
    DEV|PROD|OTHER)
	# remoteUser is already set
	case "${KAZ_SRC_TYPE}" in
	    DEV)
		remoteUser="${KAZ_DEV_REMOTE}"; remotePath="${KAZ_DEV_ROOT}"
		;;
	    PROD)
		remoteUser="${KAZ_PROD_REMOTE}"; remotePath="${KAZ_PROD_ROOT}"
		;;
	    OTHER)
		remoteUser="${KAZ_OTHER_REMOTE}"; remotePath="${KAZ_OTHER_ROOT}"
		;;
	esac

	${SIMU} rsync -rlptDEHAX --no-o --delete --info=progress2 \
		$(for i in ${REF_DIRS} ; do echo "${remoteUser}:${remotePath}$i" ; done) \
		"${TESTED_DIR}"
	;;
esac


cd "${TESTED_DIR}"

badName(){
    [[ -z "$1" ]] && return 0
    for item in ${SKIP_FILE[@]}; do
	[[ "$1/" =~ "${item}" ]] && return 0
    done
    return 1
}


declare -a CHANGED_DIRS
CHANGED_DIRS=$(find ${REF_DIRS} -type d ! -exec /bin/test -d "${KAZ_ROOT}/{}" \; -print -prune)
for file in ${CHANGED_DIRS[@]}; do
    if badName "${file}" ; then
	echo SKIP ${file}
	continue
    fi
    printKazMsg "New dir ${file}"
    while true; do
        read -p "Synchronize ${GREEN}${file}/${NC} to ${GREEN}${KAZ_ROOT}/${file}/${NC}? [y/n/i/help]: " yn
        case $yn in
	    [Yy]*)
		${SIMU} rsync -rlptDEHAX --info=progress2 "${file}/" "${KAZ_ROOT}/${file}/"
		(cd "${KAZ_ROOT}" ; git add "${file}/" )
		break
		;;  
	    ""|[Nn]*)
		break
		;;
	    [Ii]*)
		# add to skip
		echo "${file}" >> "${KAZ_CONF_DIR}/skip-file.txt"
		break
		;;
	    *)
		echo -e \
		     "     yes: add all subdir ${file} in git\n" \
		     "      no: don't update git\n" \
		     "  ignore: never ask this question\n" \
		     "    help: print this help"
		;;
        esac
    done
done

declare -a NEW_FILES
NEW_FILES=$(find ${REF_DIRS} '(' -type d ! -exec /bin/test -d "${KAZ_ROOT}/{}" \; -prune ')' -o '(' -type f ! -exec /bin/test -f "${KAZ_ROOT}/{}" \; -print ')')
for file in ${NEW_FILES[@]}; do
    if badName "${file}" ; then
	echo SKIP ${file}
	continue
    fi
    echo "New file ${file}"
    while true; do
        read -p "Synchronize ${GREEN}${file}${NC} to ${GREEN}${KAZ_ROOT}/${file}${NC}?  [y/n/i/help]: " yn
        case $yn in
	    [Yy]*)
		${SIMU} rsync -rlptDEHAX --info=progress2 "${file}" "${KAZ_ROOT}/${file}"
		(cd "${KAZ_ROOT}" ; git add "${file}" )
		break
		;;  
	    [Nn]*)
		break
		;;
	    [Ii]*)
		# add to skip
		echo "${file}" >> "${KAZ_CONF_DIR}/skip-file.txt"
		break
		;;
	    *)
		echo -e \
		     "     yes: add all subdir ${file} in git\n" \
		     "      no: don't update git\n" \
		     "  ignore: never ask this question\n" \
		     "    help: print this help"
		;;
        esac
    done
done

trap 'rm -f "${TMPFILE}"' EXIT
export TMPFILE="$(mktemp)" || exit 1

CHANGED_FILES=$(find ${REF_DIRS} '(' -type d ! -exec /bin/test -d "${KAZ_ROOT}/{}" \; -prune ')' -o '(' -type f -exec /bin/test -f "${KAZ_ROOT}/{}" \; ! -exec cmp -s "{}" "${KAZ_ROOT}/{}" \; -print ')')
for file in ${CHANGED_FILES[@]} ; do
    if badName "${file}" ; then
	echo SKIP ${file}
	continue
    fi
    echo "TEST ${file}"
    kompare "${file}" "${KAZ_ROOT}/${file}"
    if [ "${KAZ_ROOT}/${file}" -ot "${TMPFILE}" ]; then
	echo "No change of ${KAZ_ROOT}/${file}"
	continue
    fi
    chmod --reference="${file}" "${KAZ_ROOT}/${file}"
done

echo
while : ; do
    read -p "Do you want to keep ${TESTED_DIR} to speed up next download? [yes]" rep
    case "${rep}" in
        ""|[yYoO]* )
	    break
	    ;;
	[Nn]* )
	    rm -r "${TESTED_DIR}"
	    ;;
        * )
	    echo "Please answer yes no."
	    ;;
    esac
done

cd "${KAZ_ROOT}"
echo -e "\nThe git will now commit in ${CURRENT_BRANCH}"
checkContinue
git commit -a

echo -e "\nThe git will now pull in ${CURRENT_BRANCH}"
checkContinue
git pull

printKazError "\nCheck if any confict"
#XXX check conflict

echo -e "\nThe git will now push in ${CURRENT_BRANCH}"
checkContinue
git push

printKazMsg "\nYou have to logged on ${KAZ_SRC_TYPE}, and launch:\n"
echo -e \
     "  ssh root@host\n" \
     "  cd /kaz\n" \
     "  git reset --hard\n" \
     "  git pull"

printKazMsg "\nIf you want to promote in master branch:\n" \
echo -e \
     "  git checkout master\n" \
     "  git pull\n" \
     "  git merge develop\n" \
     "${RED}check if confict${NC}\n" \
     "  git commit -a \n" \
     "  git push\n"
