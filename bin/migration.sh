#!/bin/bash

CV1=/kaz-old/bin/container.sh
DV1=/kaz-old/dockers
EV1=/kaz-old/config
SV1=/kaz-old/secret

BV2=/kaz/bin
DV2=/kaz/dockers
EV2=/kaz/config
SV2=/kaz/secret
OV2=/kaz/config/orgaTmpl/orga-gen.sh

[ -x "${CV1}" ] || exit
[ -d "${BV2}" ] || exit

SIMU="echo SIMU"

${SIMU} "${CV1}" stop orga
${SIMU} "${CV1}" stop

${SIMU} rsync "${EV1}/dockers.env" "${EV2}/"
${SIMU} rsync "${SV1}/SetAllPass.sh" "${SV2}/"
${SIMU} "${BV2}/updateDockerPassword.sh"

# XXX ? rsync /kaz/secret/allow_admin_ip /kaz-git/secret/allow_admin_ip

${SIMU} "${BV2}/container.sh" start cloud dokuwiki ethercalc etherpad framadate paheko gitea jirafeau mattermost postfix proxy roundcube web

${SIMU} rsync -aAHXh --info=progress2 "${DV1}/web/html/" "/var/lib/docker/volumes/web_html/_data/"
${SIMU} chown -R www-data: "/var/lib/docker/volumes/web_html/_data/"

${SIMU} cd "${DV1}"
cd "${DV1}"
for ORGA_DIR in *-orga; do
    services=$(echo $([ -x "${ORGA_DIR}/tmpl-gen.sh" ] && "${ORGA_DIR}/tmpl-gen.sh" -l))
    if [ -n "${services}" ]; then
	ORGA="${ORGA_DIR%-orga}"

	echo "  * ${ORGA}: ${services}"
	${SIMU} "${OV2}" "${ORGA}" $(for s in ${services}; do echo "+${s}"; done)
    fi
done

