#!/bin/bash

OLDVERSION=$(docker-compose -v | sed -En 's/.*version ([a-z0-9\.]*).*/\1/p')
DOCKERCOMPOSE_VERSION="v2.17.3"

if [ "$OLDVERSION" = "$DOCKERCOMPOSE_VERSION" ]
then
        echo -e "Docker Compose déjà en version $DOCKERCOMPOSE_VERSION"
        exit
fi

curl -SL https://github.com/docker/compose/releases/download/$DOCKERCOMPOSE_VERSION/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
