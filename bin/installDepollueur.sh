#!/bin/bash

SRC_DEP=https://git.kaz.bzh/KAZ/depollueur.git

KAZ_ROOT=$(cd "$(dirname $0)/.."; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

if [[ -f "${KAZ_GIT_DIR}/depollueur/test-running" ]]; then
    exit
fi

printKazMsg "\n  *** Installation du dépollueur"

sudo apt-get install -y --fix-missing build-essential make g++ libboost-program-options-dev libboost-system-dev libboost-filesystem-dev libcurl4-gnutls-dev libssl-dev

mkdir -p "${KAZ_GIT_DIR}"
cd "${KAZ_GIT_DIR}"
if [ ! -d "depollueur" ]; then
    git clone "${SRC_DEP}"
fi
cd depollueur
git reset --hard && git pull
make

. "${DOCKERS_ENV}"
echo "${domain}" > "src/bash/domainname"
