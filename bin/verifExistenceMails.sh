#!/bin/bash

#Koi: on vérifie que chaque email possède son répertoire et vice et versa (on supprime sinon)
#Kan: 20/06/2022
#Ki: fab

#on récupère toutes les variables et mdp
# on prend comme source des repertoire le dossier du dessus ( /kaz dans notre cas )
KAZ_ROOT=$(cd "$(dirname $0)"/..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

cd $(dirname $0)/..
. "${DOCKERS_ENV}"
. "${KAZ_KEY_DIR}/SetAllPass.sh"

DOCK_DIR=$KAZ_COMP_DIR

SETUP_MAIL="docker exec -ti mailServ setup"

#on détermine le script appelant, le fichier log et le fichier source, tous issus de la même racine
PRG=$(basename $0)
RACINE=${PRG%.sh}

# emails et les alias KAZ déjà créés
TFILE_EMAIL=$(mktemp /tmp/test_email.XXXXXXXXX.TFILE_EMAIL)

#on stocke les emails et alias déjà créés
(
  ${SETUP_MAIL} email list | cut -d ' ' -f 2 | grep @
  ${SETUP_MAIL} alias list | cut -d ' ' -f 2 | grep @  
) > ${TFILE_EMAIL}


#did on supprime le ^M en fin de fichier pour pas faire planter les grep 
sed -i -e 's/\r//g' ${TFILE_EMAIL}

rep_email="/var/lib/docker/volumes/postfix_mailData/_data"

#étape n°1: pour chaque répertoire, on vérifie que l'email existe
echo "Début Etape n°1: on liste les répertoires des emails et on vérifie que les emails correspondant existent"

ls -Ifilter -Itmp ${rep_email} | while read fin_email; do
  ls ${rep_email}/${fin_email} | while read debut_email; do
    email=`echo ${debut_email}@${fin_email}`
    #est-ce que l'email existe ?
    nb_ligne=$(grep "^${email}$" ${TFILE_EMAIL} | wc -l)
    if [ ${nb_ligne} -gt 0  ];then
      false
    else
      #suppression du répertoire
      echo rm ${rep_email}/${fin_email}/${debut_email} -rf
    fi    
  done
  #si le répertoire domaine est vide, on le supprime
  find ${rep_email}/${fin_email} -maxdepth 0 -type d -empty -delete
done
echo "aucune commande n'a été lancée, possible de le faire à la main"
echo "Fin Etape n°1"

#Etape n°2: pour chaque email, on vérifie que le répertoire existe
echo "Début Etape n°2 n°2: on liste les emails et on vérifie que les répertoires correspondant existent"
cat ${TFILE_EMAIL} | while read email; do
  debut_email=$(echo ${email} | awk -F '@' '{print $1}')
  fin_email=$(echo ${email} | awk -F '@' '{print $2}')
  if [ -d ${rep_email}/${fin_email}/${debut_email} ];then
    true
  else
    echo "Attention, le répertoire ${fin_email}/${debut_email} n'existe pas alors que l'email existe!"
  fi    
done
echo "Fin Etape n°2"
