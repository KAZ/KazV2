#!/bin/sh -e

. /usr/share/debconf/confmodule
db_version 2.0

if [ "$1" = "fset" ]; then
    db_fset kaz/mode seen false
    db_fset kaz/domain seen false
    db_go
fi
if [ "$1" = "reset" ]; then
    db_reset kaz/mode
    db_reset kaz/domain
    db_go
fi

#db_set kaz/domain test


db_title "a b c"

db_input critical kaz/mode
db_input critical kaz/domain 
db_go
