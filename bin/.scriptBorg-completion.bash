#!/bin/bash
KAZ_ROOT=/kaz
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

_borg()
{
        local current=${COMP_WORDS[COMP_CWORD]}
        case "$current" in
                -*)
                        local_prop="-h -d -i -l -m -u -t -p -v -info"
                        COMPREPLY=( $(compgen -W "${local_prop}" -- $current) )
                        ;;
        esac
}
complete -F _borg scriptBorg.sh

