#!/bin/bash

KAZ_ROOT=$(cd "$(dirname $0)/.."; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

cd "${KAZ_COMP_DIR}"
for orga in *-orga
do
    ${orga}/orga-gen.sh
    "${KAZ_ROOT}/bin/container.sh" stop "${orga}"
    "${KAZ_ROOT}/bin/container.sh" start "${orga}"
done
