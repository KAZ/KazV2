g#!/bin/bash

set -e
# on pourra inclure le fichier dockers.env pour
# gérer l' environnement DEV, PROD ou LOCAL

KAZ_ROOT=$(cd "$(dirname $0)/.."; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars
export VAGRANT_SRC_DIR=/vagrant/files
cd "${KAZ_ROOT}"

if [ ! -f "${KAZ_ROOT}/config/dockers.env" ]; then
    printKazError "dockers.env not found"
    exit 1
fi
for type in mail orga proxy withMail withoutMail ; do
    if [ ! -f "${KAZ_ROOT}/config/container-${type}.list" ]; then
	printKazError "container-${type}.list not found"
	exit 1
    fi
done

mkdir -p "${KAZ_ROOT}/log/"
export DebugLog="${KAZ_ROOT}/log/log-install-$(date +%y-%m-%d-%T)-"
(
    declare -a DOCKERS_LIST NEW_SERVICE
    # dockers à démarrer (manque : sympa, wordpress, orga)
    DOCKERS_LIST+=($(getList "${KAZ_CONF_DIR}/container-withoutMail.list"))
    DOCKERS_LIST+=($(getList "${KAZ_CONF_DIR}/container-proxy.list"))
    DOCKERS_LIST+=($(getList "${KAZ_CONF_DIR}/container-mail.list"))
    DOCKERS_LIST+=($(getList "${KAZ_CONF_DIR}/container-withMail.list"))
    # web  proxy  postfix sympa roundcube jirafeau ldap quotas cachet ethercalc etherpad framadate paheko dokuwiki gitea mattermost cloud collabora
    # 8080 443                  8081      8082     8083 8084   8085   8086      8087     8088      8089     8090     8091  8092       8093  8094
    # pour ne tester qu'un sous-ensemble de service
    if [ $# -ne 0 ]; then
	case $1 in
	    -h*|--h*)
		echo $(basename "$0") " [-h] [-help] ([1-9]* | {service...})"
		echo "    -h"
		echo "    -help Display this help."
		echo "    service.. service to enable"
		echo "    [1-9]* level of predefined services set selection"
		exit
		;;
	    0)
		echo $(basename "$0"): " level '0' not defined"
		exit
		;;
	    [0-9]*)
		for level in $(seq 1 $1); do
		    case ${level} in
			1)  NEW_SERVICE+=("web" "proxy");;
			2)  NEW_SERVICE+=("postfix");;
			3)  NEW_SERVICE+=("roundcube");;
			4)  NEW_SERVICE+=("sympa");;
			5)  NEW_SERVICE+=("jirafeau");;
			6)  NEW_SERVICE+=("ldap");;
			7)  NEW_SERVICE+=("quotas");;
			8)  NEW_SERVICE+=("cachet");;
			9)  NEW_SERVICE+=("ethercalc");;
			10) NEW_SERVICE+=("etherpad");;
			11) NEW_SERVICE+=("framadate");;
			12) NEW_SERVICE+=("paheko");;
			13) NEW_SERVICE+=("dokuwiki");;
			14) NEW_SERVICE+=("gitea");;
			15) NEW_SERVICE+=("mattermost");;
			16) NEW_SERVICE+=("collabora");;
			17) NEW_SERVICE+=("cloud");;
			*)
			    echo $(basename "$0"): " level '${level}' not defined"
			    exit
			    ;;
		    esac
		done
		DOCKERS_LIST=(${NEW_SERVICE[@]})
		printKazMsg "level $1"
		;;
	    *)
		# XXX il manque l'extention des noms (jir va fair le start de jirafeau mais pas le download et le first)
		DOCKERS_LIST=($*)
		;;
	esac
    fi

    DOCKERS_LIST=($(filterAvailableComposes ${DOCKERS_LIST[*]}))

    printKazMsg "dockers: ${DOCKERS_LIST[*]}"

    # on pré-télécharge à l'origine Vagrant (jirafeau...)
    mkdir -p "${KAZ_ROOT}/git" "${KAZ_ROOT}/download"
    for DOCKER in ${DOCKERS_LIST[@]}; do
	if [ -f "${KAZ_ROOT}/dockers/${DOCKER}/download.sh" ]; then
	    cd "${KAZ_ROOT}/dockers/${DOCKER}"
	    ./download.sh
	fi
    done

    # on pré-télécharge le dépollueur
    if [[ " ${DOCKERS_LIST[*]} " =~ " "(jirafeau|postfix|sympa)" " ]]; then
        "${KAZ_BIN_DIR}/installDepollueur.sh"
        docker volume create filterConfig
    fi

    # on sauve les pré-téléchargement pour le prochain lancement de Vagrant
    [ -d "${VAGRANT_SRC_DIR}/kaz/download" ] &&
	rsync -a  "${KAZ_ROOT}/download/" "${VAGRANT_SRC_DIR}/kaz/download/"
    [ -d "${VAGRANT_SRC_DIR}/kaz/git" ] &&
	rsync -a "${KAZ_ROOT}/git/" "${VAGRANT_SRC_DIR}/kaz/git/"

    # on construit les dockers qui contiennent un script de création (etherpad, framadate, jirafeau...)
    for DOCKER in ${DOCKERS_LIST[@]}; do
	if [ -f "${KAZ_ROOT}/dockers/${DOCKER}/build.sh" ]; then
	    cd "${KAZ_ROOT}/dockers/${DOCKER}"
	    ./build.sh
	fi
    done

    # on démare les containers de la liste uniquement (en une fois par cohérence de proxy)
    # "${KAZ_ROOT}/bin/container.sh" stop ${DOCKERS_LIST[*]}
    "${KAZ_ROOT}/bin/container.sh" start ${DOCKERS_LIST[*]}

    if [[ " ${DOCKERS_LIST[*]} " =~ " traefik " ]]; then
      # on initialise traefik :-(
      ${KAZ_COMP_DIR}/traefik/first.sh
    fi

    if [[ " ${DOCKERS_LIST[*]} " =~ " etherpad " ]]; then
	# pb avec la lanteur de démarrage du pad :-(
	sleep 5
	"${KAZ_ROOT}/bin/container.sh" start etherpad
    fi


    if [[ " ${DOCKERS_LIST[*]} " =~ " jirafeau " ]]; then
	# pb avec la lanteur de démarrage du jirafeau :-(
	(cd "${KAZ_COMP_DIR}/jirafeau" ; docker-compose restart)
    fi

    # on construit les dockers qui contiennent un script de création (etherpad, framadate, jirafeau...)
    for DOCKER in ${DOCKERS_LIST[@]}; do
	if [ -f "${KAZ_ROOT}/dockers/${DOCKER}/first.sh" ]; then
	    cd "${KAZ_ROOT}/dockers/${DOCKER}"
	    ./first.sh
	fi
    done

    echo "########## ********** End install $(date +%D-%T)"
)  > >(tee ${DebugLog}stdout.log) 2> >(tee ${DebugLog}stderr.log >&2)
