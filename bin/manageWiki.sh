#!/bin/bash
# Script de manipulation d'un dokuwiki'
# init /versions / restart ...
#

KAZ_ROOT=$(cd "$(dirname $0)"/..; pwd)
. $KAZ_ROOT/bin/.commonFunctions.sh
setKazVars
. $DOCKERS_ENV
. $KAZ_ROOT/secret/SetAllPass.sh

#GLOBAL VARS
PRG=$(basename $0)

availableOrga=($(getList "${KAZ_CONF_DIR}/container-orga.list"))
AVAILABLE_ORGAS=${availableOrga[*]//-orga/}
DNLD_DIR="${KAZ_DNLD_DIR}/dokuwiki"

QUIET="1"
ONNAS=

WIKICOMMUN="OUI_PAR_DEFAUT"
DockerServName=${dokuwikiServName}

declare -A Posts

usage() {
echo "${PRG} [OPTION] [COMMANDES] [ORGA]
Manipulation d'un dokuwiki

OPTIONS 
 -h|--help          Cette aide :-)
 -n|--simu          SIMULATION
 -q|--quiet         On ne parle pas (utile avec le -n pour avoir que les commandes)
 --nas              L'orga se trouve sur le NAS !

COMMANDES (on peut en mettre plusieurs dans l'ordre souhaité)
 -I|--install       L'initialisation du dokuwiki
 -v|--version       Donne la version du dokuwiki et signale les MàJ

 --reload           kill lighthttpd

ORGA                parmi : ${AVAILABLE_ORGAS}
                    ou vide si dokuwiki commun
"
}



Init(){

    NOM=$ORGA
    if [ -n "$WIKICOMMUN" ] ; then NOM="KAZ" ; fi
    TPL_DIR="${VOL_PREFIX}wikiLibtpl/_data"
    PLG_DIR="${VOL_PREFIX}wikiPlugins/_data"
    CONF_DIR="${VOL_PREFIX}wikiConf/_data"

    # Gael, j'avais ajouté ça mais j'ai pas test alors je laisse comme avant ... 
    # A charge au prochain qui monte un wiki de faire qque chose
    #WIKI_ROOT="${dokuwiki_WIKI_ROOT}"
    #WIKI_EMAIL="${dokuwiki_WIKI_EMAIL}"
    #WIKI_PASS="${dokuwiki_WIKI_PASSWORD}"

    WIKI_ROOT=Kaz
    WIKI_EMAIL=wiki@kaz.local
    WIKI_PASS=azerty

    ${SIMU} checkDockerRunning "${DockerServName}" "${NOM}" || exit

    if [ ! -f "${CONF_DIR}/local.php" ] ; then
        echo "\n  *** Premier lancement de Dokuwiki ${NOM}" >& $QUIET

        ${SIMU} waitUrl "${WIKI_URL}"

        ${SIMU} curl -X POST \
            -A "Mozilla/5.0 (X11; Linux x86_64)" \
            -d "l=fr" \
            -d "d[title]=${NOM}" \
            -d "d[acl]=true" \
            -d "d[superuser]=${WIKI_ROOT}" \
            -d "d[fullname]=Admin"\
            -d "d[email]=${WIKI_EMAIL}" \
            -d "d[password]=${WIKI_PASS}" \
            -d "d[confirm]=${WIKI_PASS}" \
            -d "d[policy]=1" \
            -d "d[allowreg]=false" \
            -d "d[license]=0" \
            -d "d[pop]=false" \
            -d "submit=Enregistrer" \
            "${WIKI_URL}/install.php"

            # XXX initialiser admin:<pass>:admin:<mel>:admin,user
            #${SIMU} rsync -auHAX local.php users.auth.php acl.auth.php "${CONF_DIR}/"

            ${SIMU} sed -i "${CONF_DIR}/local.php" \
                -e "s|\(.*conf\['title'\].*=.*'\).*';|\1${NOM}';|g" \
                -e "s|\(.*conf\['title'\].*=.*'\).*';|\1${NOM}';|g" \
                -e "/conf\['template'\]/d" \
                -e '$a\'"\$conf['template'] = 'docnavwiki';"''

            ${SIMU} sed -i -e "s|\(.*conf\['lang'\].*=.*'\)en';|\1fr';|g" "${CONF_DIR}/dokuwiki.php"

            ${SIMU} chown -R www-data: "${CONF_DIR}/"
        fi

        ${SIMU} unzipInDir "${DNLD_DIR}/docnavwiki.zip" "${TPL_DIR}/"
        ${SIMU} chown -R www-data: "${TPL_DIR}/"
        # ckgedit : bof
        for plugin in captcha smtp todo wrap wrapadd; do
            ${SIMU} unzipInDir "${DNLD_DIR}/${plugin}.zip" "${PLG_DIR}"
        done
        ${SIMU} chown -R www-data:  "${PLG_DIR}/"
}

Version(){
    # $1 ContainerName
    VERSION=$(docker exec $1 cat /dokuwiki/VERSION)
    echo "Version $1 : ${GREEN}${VERSION}${NC}"
}


Reload(){
    # $1 ContainerName
    if [ -f "${VOL_PREFIX}wikiData/_data/farms/init.sh" ]; then
        ${SIMU} docker exec -ti "${1}" /dokuwiki/data/farms/init.sh
        ${SIMU} pkill -KILL lighttpd
    fi
}

########## Main #################
for ARG in "$@"; do 
    case "${ARG}" in
        '-h' | '--help' )
        usage && exit ;;
        '-n' | '--simu')
        SIMU="echo" ;;
        '-q' )
        QUIET="/dev/null" ;;
        '--nas' | '-nas' )
        ONNAS="SURNAS" ;; 
        '-v' | '--version')
        COMMANDS="$(echo "${COMMANDS} VERSION" | sed "s/\s/\n/g" | sort | uniq)" ;; 
        '--reload' )
         COMMANDS="$(echo "${COMMANDS} RELOAD" | sed "s/\s/\n/g" | sort | uniq)" ;; # le sed sort uniq, c'est pour pas l'avoir en double
        '-I' | '--install' )
        COMMANDS="$(echo "${COMMANDS} INIT" | sed "s/\s/\n/g" | sort | uniq)" ;; # le sed sort uniq, c'est pour pas l'avoir en double
        '-*' ) # ignore
        ;; 
        *)
        ORGA="${ARG%-orga}"
        DockerServName="${ORGA}-${dokuwikiServName}"
        WIKICOMMUN=
        ;;
    esac
done

if [ -z "${COMMANDS}" ]; then usage && exit ; fi

VOL_PREFIX="${DOCK_VOL}/orga_${ORGA}-"
WIKI_URL="${httpProto}://${ORGA}-${dokuwikiHost}.${domain}"
if [ -n "${WIKICOMMUN}" ]; then 
    VOL_PREFIX="${DOCK_VOL}/dokuwiki_doku"
    WIKI_URL="${httpProto}://${dokuwikiHost}.${domain}"
elif [ -n "${ONNAS}" ]; then 
    VOL_PREFIX="${NAS_VOL}/orga_${ORGA}-"
fi

for COMMAND in ${COMMANDS}; do
    case "${COMMAND}" in
        'VERSION' )
         Version "${DockerServName}" && exit ;;
        'INIT' )
         Init "${DockerServName}" ;;
        'RELOAD' )
         Reload "${DockerServName}";;
    esac
done