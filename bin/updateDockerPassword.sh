#!/bin/bash

KAZ_ROOT=$(cd $(dirname $0)/..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

# pour mise au point
# SIMU=echo

# Améliorations à prévoir
# - donner en paramètre les services concernés (pour limité les modifications)
# - pour les DB si on déclare un nouveau login, alors les privilèges sont créé mais les anciens pas révoqués

. "${DOCKERS_ENV}"
. "${KAZ_KEY_DIR}/SetAllPass.sh"

updateEnvDB(){
    # $1 = prefix
    # $2 = envName
    # $3 = containerName of DB
    rootPass="$1_MYSQL_ROOT_PASSWORD"
    dbName="$1_MYSQL_DATABASE"
    userName="$1_MYSQL_USER"
    userPass="$1_MYSQL_PASSWORD"

    ${SIMU} sed -i \
	    -e "s/MYSQL_ROOT_PASSWORD=.*/MYSQL_ROOT_PASSWORD=${!rootPass}/g" \
	    -e "s/MYSQL_DATABASE=.*/MYSQL_DATABASE=${!dbName}/g" \
	    -e "s/MYSQL_USER=.*/MYSQL_USER=${!userName}/g" \
	    -e "s/MYSQL_PASSWORD=.*/MYSQL_PASSWORD=${!userPass}/g" \
	    "$2"

    # seulement si pas de mdp pour root
    # pb oeuf et poule (il faudrait les anciennes valeurs) :
    # * si rootPass change, faire à la main
    # * si dbName change, faire à la main
    checkDockerRunning "$3" "$3" || return
    echo "change DB pass on docker $3"
    echo "grant all privileges on ${!dbName}.* to '${!userName}' identified by '${!userPass}';" | \
	docker exec -i $3 bash -c "mysql --user=root --password=${!rootPass}"
}

updateEnv(){
    # $1 = prefix
    # $2 = envName

    for varName in $(grep "^[a-zA-Z_]*=" $2 | sed "s/^\([^=]*\)=.*/\1/g")
    do
	srcName="$1_${varName}"
	srcVal=$(echo "${!srcName}" | sed -e "s/[&]/\\\&/g")
	${SIMU} sed -i \
		-e "s%^[ ]*${varName}=.*\$%${varName}=${srcVal}%" \
		"$2"
    done
}

framadateUpdate(){
    [[ "${COMP_ENABLE}" =~ " framadate " ]] || return
    if [ ! -f "${DOCK_LIB}/volumes/framadate_dateConfig/_data/config.php" ]; then
	return 0
    fi
    checkDockerRunning "${framadateServName}" "Framadate" &&
	${SIMU} docker exec -ti "${framadateServName}" bash -c -i "htpasswd -bc /var/framadate/admin/.htpasswd ${framadate_HTTPD_USER} ${framadate_HTTPD_PASSWORD}"
    ${SIMU} sed -i \
	    -e "s/^#*const DB_USER[ ]*=.*$/const DB_USER= '${framadate_MYSQL_USER}';/g" \
	    -e "s/^#*const DB_PASSWORD[ ]*=.*$/const DB_PASSWORD= '${framadate_MYSQL_PASSWORD}';/g" \
	    "${DOCK_LIB}/volumes/framadate_dateConfig/_data/config.php"
}

jirafeauUpdate(){
    [[ "${COMP_ENABLE}" =~ " jirafeau " ]] || return
    if [ ! -f "${DOCK_LIB}/volumes/jirafeau_fileConfig/_data/config.local.php" ]; then
	return 0
    fi
    SHA=$(echo -n "${jirafeau_HTTPD_PASSWORD}" | sha256sum | cut -d \  -f 1)
    ${SIMU} sed -i \
	    -e "s/'admin_password'[ ]*=>[ ]*'[^']*'/'admin_password' => '${SHA}'/g" \
	    "${DOCK_LIB}/volumes/jirafeau_fileConfig/_data/config.local.php"
}

####################
# main

updateEnvDB "etherpad" "${KAZ_KEY_DIR}/env-${etherpadDBName}" "${etherpadDBName}"
updateEnvDB "framadate" "${KAZ_KEY_DIR}/env-${framadateDBName}" "${framadateDBName}"
updateEnvDB "gitea" "${KAZ_KEY_DIR}/env-${gitDBName}" "${gitDBName}"
updateEnvDB "mattermost" "${KAZ_KEY_DIR}/env-${mattermostDBName}" "${mattermostDBName}"
updateEnvDB "nextcloud" "${KAZ_KEY_DIR}/env-${nextcloudDBName}" "${nextcloudDBName}"
updateEnvDB "roundcube" "${KAZ_KEY_DIR}/env-${roundcubeDBName}" "${roundcubeDBName}"
updateEnvDB "sympa" "${KAZ_KEY_DIR}/env-${sympaDBName}" "${sympaDBName}"
updateEnvDB "vigilo" "${KAZ_KEY_DIR}/env-${vigiloDBName}" "${vigiloDBName}"
updateEnvDB "wp" "${KAZ_KEY_DIR}/env-${wordpressDBName}" "${wordpressDBName}"
updateEnvDB "vaultwarden" "${KAZ_KEY_DIR}/env-${vaultwardenDBName}" "${vaultwardenDBName}"
updateEnvDB "castopod" "${KAZ_KEY_DIR}/env-${castopodDBName}" "${castopodDBName}"

updateEnv "apikaz" "${KAZ_KEY_DIR}/env-${apikazServName}"
updateEnv "ethercalc" "${KAZ_KEY_DIR}/env-${ethercalcServName}"
updateEnv "etherpad" "${KAZ_KEY_DIR}/env-${etherpadServName}"
updateEnv "framadate" "${KAZ_KEY_DIR}/env-${framadateServName}"
updateEnv "gandi" "${KAZ_KEY_DIR}/env-gandi"
updateEnv "gitea" "${KAZ_KEY_DIR}/env-${gitServName}"
updateEnv "jirafeau" "${KAZ_KEY_DIR}/env-${jirafeauServName}"
updateEnv "mattermost" "${KAZ_KEY_DIR}/env-${mattermostServName}"
updateEnv "nextcloud" "${KAZ_KEY_DIR}/env-${nextcloudServName}"
updateEnv "office" "${KAZ_KEY_DIR}/env-${officeServName}"
updateEnv "roundcube" "${KAZ_KEY_DIR}/env-${roundcubeServName}"
updateEnv "vigilo" "${KAZ_KEY_DIR}/env-${vigiloServName}"
updateEnv "wp" "${KAZ_KEY_DIR}/env-${wordpressServName}"
updateEnv "ldap" "${KAZ_KEY_DIR}/env-${ldapServName}"
updateEnv "sympa" "${KAZ_KEY_DIR}/env-${sympaServName}"
updateEnv "mail" "${KAZ_KEY_DIR}/env-${smtpServName}"
updateEnv "mobilizon" "${KAZ_KEY_DIR}/env-${mobilizonServName}"
updateEnv "mobilizon" "${KAZ_KEY_DIR}/env-${mobilizonDBName}"
updateEnv "vaultwarden" "${KAZ_KEY_DIR}/env-${vaultwardenServName}"
updateEnv "castopod" "${KAZ_KEY_DIR}/env-${castopodServName}"
updateEnv "ldap" "${KAZ_KEY_DIR}/env-${ldapUIName}"


framadateUpdate
jirafeauUpdate
exit 0
