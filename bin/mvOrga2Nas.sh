#!/bin/bash

# déplace des orga de
#    /var/lib/docker/volumes/
# vers
#    /mnt/disk-nas1/docker/volumes/

KAZ_ROOT=$(cd "$(dirname $0)"/..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars
DOCK_NAS="/mnt/disk-nas1/docker/volumes"

DOCK_SRC="${DOCK_VOL}"
DOCK_DST="${DOCK_NAS}"

export PRG="$0"
cd $(dirname $0)

. "${DOCKERS_ENV}"

declare -a availableOrga
availableOrga=($(sed -e "s/\(.*\)[ \t]*#.*$/\1/" -e "s/^[ \t]*\(.*\)-orga$/\1/" -e "/^$/d" "${KAZ_CONF_DIR}/container-orga.list"))

# no more export in .env
export $(set | grep "domain=")

export SIMU=""
export ONLY_SYNC=""
export NO_SYNC=""
export FORCE=""
export REVERSE=""

usage(){
    echo "Usage: ${PRG} [orga...]"
    echo "  -h help"
    echo "  -n simulation"
    echo "  -y force"
    echo "  -s phase1 only"
    echo "  -r reverse (${DOCK_NAS} to ${DOCK_VOL})"
    echo "  -ns no pre sync"
    exit 1
}

for ARG in $@; do
    case "${ARG}" in
	'-h' | '-help' )
	    usage
	    ;;
	'-n' )
	    shift
	    export SIMU="echo"
	    ;;
	'-y' )
	    shift
	    export FORCE="yes"
	    ;;
	'-s' )
	    shift
	    export ONLY_SYNC="yes"
	    ;;
	'-r' )
	    shift
	    export REVERSE="yes"
	    ;;
	'-ns' )
	    shift
	    export NO_SYNC="yes"
	    ;;
	*)
	    break
	    ;;
    esac
done

[[ -z "$1" ]] && usage

for ARG in $@; do
    [[ ! " ${availableOrga[*]} " =~ " ${ARG} " ]] && echo "${RED}${ARG}${NC} is not an orga" && usage
done

########################################

# on copie dans new pour que le changement soit atomique
NEW=""
if [ -n "${REVERSE}" ]; then
    DOCK_SRC="${DOCK_NAS}"
    DOCK_DST="${DOCK_VOL}"
    NEW="/new"
    ${SIMU} mkdir -p "${DOCK_DST}${NEW}"
fi
# on garde une copie dans back au cas où
BACK="${DOCK_SRC}/old"

echo "  move from ${BLUE}${BOLD}${DOCK_SRC}${NC} to ${BLUE}${BOLD}${DOCK_DST}${NC}"
checkContinue

cd "${DOCK_SRC}"
volext=$(ls -d orga* | sed 's%.*-%%' | sort -u)
declare -a orgaPhase2

# Pour que l'interruption de service soit la plus courte possible,
# on pré-copie toutes les infos de puis la création de l'orga
echo -n "${BLUE}Phase 1: pre sync.${NC} "
[[ -z "${FORCE}" ]] && [[ -z "${NO_SYNC}" ]] && checkContinue
echo
for ARG in $@; do
    for EXT in ${volext}; do
	vol="orga_${ARG}-${EXT}"
	# test le service existe
	[ -e "${DOCK_SRC}/${vol}" ] || continue
	# si c'est un lien sur /var/lib c'est déjà fait
	[ -z "${REVERSE}" ] && [ -L "${DOCK_SRC}/${vol}" ] && echo "${GREEN}${vol}${NC} : done" && continue
	# si c'est un lien sur le NAS c'est un problème
	[ -n "${REVERSE}" ] && [ -L "${DOCK_SRC}/${vol}" ] && echo "${GREEN}${vol}${NC} : bug" && continue
	# si c'est pas un répertoire c'est un problème
	! [ -d "${DOCK_SRC}/${vol}" ] && echo "${RED}${vol}${NC} : done ?" && continue
	# si transfert est déjà fait
	if [ -n "${REVERSE}" ]; then
	    ! [ -L "${DOCK_DST}/${vol}" ] && echo "${RED}${vol}${NC} : done" && continue
	fi
	echo "   - ${YELLOW}${vol}${NC}"
	[[ -z "${NO_SYNC}" ]] && ${SIMU} rsync -auHAX --info=progress2 "${DOCK_SRC}/${vol}/" "${DOCK_DST}${NEW}/${vol}/"
	[[ " ${orgaPhase2[@]} " =~ " ${ARG} " ]] || orgaPhase2+=( "${ARG}" )
    done
done

[ -n "${ONLY_SYNC}" ] && exit 0

if (( ${#orgaPhase2[@]} == 0 )); then
    exit 0
fi

echo -n "${BLUE}Phase 2: mv.${NC} "
[[ -z "${FORCE}" ]] && checkContinue
echo
mkdir -p "${BACK}"
for ARG in "${orgaPhase2[@]}"; do
    cd "${KAZ_ROOT}"
    cd "${KAZ_COMP_DIR}/${ARG}-orga"
    ! [ -e "docker-compose.yml" ] && echo "no docker-compose.yml for ${RED}${ARG}${NC}" && continue
    ${SIMU} docker-compose down
    # L'arrêt ne durera que le temps de copier les modifications depuis la phase 1.
    for EXT in ${volext}; do
	vol="orga_${ARG}-${EXT}"
	# test le service existe
	[ -e "${DOCK_SRC}/${vol}" ] || continue
	# si c'est un lien sur /var/lib c'est déjà fait
	[ -z "${REVERSE}" ] && [ -L "${DOCK_SRC}/${vol}" ] && echo "${GREEN}${vol}${NC} : done" && continue
	# si c'est un lien sur le NAS c'est un problème
	[ -n "${REVERSE}" ] && [ -L "${DOCK_SRC}/${vol}" ] && echo "${GREEN}${vol}${NC} : bug" && continue
	# si c'est pas un répertoire c'est un problème
	! [ -d "${DOCK_SRC}/${vol}" ] && echo "${RED}${vol}${NC} : done ?" && continue
	# si transfert est déjà fait
	if [ -n "${REVERSE}" ]; then
	    ! [ -L "${DOCK_DST}/${vol}" ] && echo "${RED}${vol}${NC} : done" && continue
	fi
	echo "   - ${YELLOW}${vol}${NC}"
	${SIMU} rsync -auHAX --info=progress2 --delete "${DOCK_SRC}/${vol}/" "${DOCK_DST}${NEW}/${vol}/" || exit 1
	${SIMU} mv "${DOCK_SRC}/${vol}" "${BACK}/"
	if [ -z "${REVERSE}" ]; then
	    # cas de /var/lib vers NAS
	    ${SIMU} ln -sf "${DOCK_DST}/${vol}" "${DOCK_SRC}/"
	else
	    # cas NAS vers /var/lib
	    ${SIMU} rm -f "${DOCK_SRC}/${vol}"
	    ${SIMU} mv "${DOCK_DST}${NEW}/${vol}" "${DOCK_DST}/"
	fi
    done
    ${SIMU} docker-compose up -d
    [[ -x "reload.sh" ]] && "./reload.sh"
    echo
done
