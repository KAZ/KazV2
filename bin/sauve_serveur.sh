#! /bin/sh
# date: 12/11/2020

PATH=/bin:/sbin:/usr/bin:/usr/sbin
PATH_SAUVE="/home/sauve/"

iptables-save > $PATH_SAUVE/iptables.sav

dpkg --get-selections > $PATH_SAUVE/dpkg_selection
tar -clzf $PATH_SAUVE/etc_sauve.tgz /etc 1> /dev/null 2> /dev/null
tar -clzf $PATH_SAUVE/var_spool.tgz /var/spool 1> /dev/null 2> /dev/null
tar -clzf $PATH_SAUVE/root.tgz /root 1> /dev/null 2> /dev/null
tar -clzf $PATH_SAUVE/kaz.tgz /kaz 1> /dev/null 2> /dev/null
rsync -a /var/spool/cron/crontabs $PATH_SAUVE

#sauve les bases
/kaz/bin/container.sh save

