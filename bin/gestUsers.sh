#!/bin/bash
# gestion des utilisateurs de kaz ( mail, cloud général, mattermost )

KAZ_ROOT=$(cd "$(dirname $0)"/..; pwd)
. $KAZ_ROOT/bin/.commonFunctions.sh
setKazVars

. $DOCKERS_ENV
. $KAZ_ROOT/secret/SetAllPass.sh

VERSION="5-12-2024"
PRG=$(basename $0)
RACINE=$(echo $PRG | awk '{print $1}')
IFS=' '
BLINK='\033[0;5m'

LOG=$RACINE".log"
URL_NC=$(echo $cloudHost).$(echo $domain)
URL_AGORA=$(echo $matterHost).$(echo $domain)
URL_LISTE=$(echo $sympaHost).$(echo $domain)
URL_PAHEKO="$httpProto://${paheko_API_USER}:${paheko_API_PASSWORD}@kaz-paheko.$(echo $domain)"
NL_LIST=infos@listes.kaz.bzh
URL_AGORA_API=${URL_AGORA}/api/v4
EQUIPE=kaz
LISTMASTER=$(echo ${sympa_LISTMASTERS} | cut -d',' -f1)


#### Test du serveur sur lequel s' execute le script ####
echo ${site} | grep -E 'prod1|dev' || { echo "Le script ne fonctionne que sur Prod1 et Dev ";exit;}
##############################

TFILE_EMAILS=$(mktemp /tmp/$RACINE.XXXXXXXXX.TFILE_EMAILS)
TFILE_EMAILS_LIST=$(mktemp /tmp/$RACINE.XXXXXXXXX.TFILE_EMAILS_LIST)
TFILE_MAILS_TROUVE=$(mktemp /tmp/$RACINE.XXXXXXXXX.TFILE_MAILS_TROUVE)
TFILE_MAILS_MATTERMOST=$(mktemp /tmp/$RACINE.XXXXXXXXTFILE_MAILS_MATTERMOST)
TFILE_CREATE_MAIL=$(mktemp /tmp/$RACINE.XXXXXXXXTFILE_CREATE_MAIL.sh)
LDAP_IP=$(docker inspect -f '{{.NetworkSettings.Networks.ldapNet.IPAddress}}' ldapServ)

regexMail="^(([A-Za-z0-9]+((\.|\-|\_|\+)?[A-Za-z0-9]?)*[A-Za-z0-9]+)|[A-Za-z0-9]+)@(([A-Za-z0-9]+)+((\.|\-|\_)?([A-Za-z0-9]+)+)*)+\.([A-Za-z]{2,})+$"


rm -rf /tmp/$RACINE*
rm -rf /tmp/*.json

############################################ Fonctions #######################################################

ExpMail() {
        MAIL_DEST=$1
        MAIL_SUJET=$2
        MAIL_TEXTE=$3
        # a mettre ailleurs
        mailexp=${service_mail}
        mailpassword=${service_password}
        mailserveur=${smtpHost}.${domain}
        printf "Subject:${MAIL_SUJET}\n${MAIL_TEXTE}" | msmtp ${MAIL_DEST}
}

PostMattermost() {
        PostM=$1
        CHANNEL=$2
        TEAMID=$(curl -s -H "Authorization: Bearer ${mattermost_token}" "${URL_AGORA_API}/teams/name/${EQUIPE}" | jq .id | sed -e 's/"//g')
        CHANNELID=$(curl -s -H "Authorization: Bearer ${mattermost_token}" ${URL_AGORA_API}/teams/${TEAMID}/channels/name/${CHANNEL} | jq .id | sed -e 's/"//g')
        curl -s i-X POST -i -H "Authorization: Bearer ${mattermost_token}" -d "{\"channel_id\":\"${CHANNELID}\",\"message\":\"${PostM}\"}" "${URL_AGORA_API}/posts" >/dev/null 2>&1
}


searchEmail() {
		# on peut appeler cette fonction avec un paramêtre 
		# qui doit être une adresse email
		CHOIX_MAIL=""
		SEARCH_OBJECT_CLASS="inetOrgPerson"
		[ "$1" = "alias" ] && SEARCH_OBJECT_CLASS="PostfixBookMailForward"
		rm -rf $TFILE_MAILS_TROUVE
		rm -rf $TFILE_MAILS
		fait=0
		while [ ${fait} = 0 ]
		do
			clear
			echo "----------------------------------------------------------------------"
			printKazMsg "${ACTION_EN_COURS}"
			echo "----------------------------------------------------------------------"
			read -p "Adresse ou caractere contenu dans cette adresse (r ou R pour retour ) ? : " RMAIL
			[[ ${RMAIL} =~ ^[rRqQ]$ ]] && Main
			if [ "${RMAIL}" == "" ] 
			then
				fait=0
			else
				fait=1
			fi
		done
		ldapsearch -H ldap://${LDAP_IP} \
		-x -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" \
		-w "${ldap_LDAP_ADMIN_PASSWORD}" \
		-b "${ldap_root}" "(&(objectclass=${SEARCH_OBJECT_CLASS})(cn=*${RMAIL}*))" cn | grep ^cn | sed -e 's/^cn: //' >$TFILE_EMAILS
		COMPTEUR_LIGNE=0
		while read LIGNE
		do
			COMPTEUR_LIGNE=$(expr $COMPTEUR_LIGNE + 1)
			printf "%d - %s\n" $COMPTEUR_LIGNE $LIGNE  >>$TFILE_MAILS_TROUVE
		done <$TFILE_EMAILS
		if [ $COMPTEUR_LIGNE -gt 0 ]
		then
			fait=0
			while [ ${fait} = 0 ]
			do
				echo "----------------------------------------------------------------------"
				cat $TFILE_MAILS_TROUVE
				echo "----------------------------------------------------------------------"
				read -p  "Choisir le numéro correspondant ( R ou r ou 0 pour retour ): " NB_LIGNE_MAIL
				# si on tape r ou r ou Q ou q ou 0 on relance le menu de recherche de mail
				[[ $NB_LIGNE_MAIL =~ [rRqQ0] ]] && searchEmail $1
				CHOIX_MAIL=$(cat ${TFILE_MAILS_TROUVE} | grep "^${NB_LIGNE_MAIL}\b" | awk '{print $3}' | tr -d '[:space:]')
				
				# si on répond par entrée nb_ligne_mail sera vide 
				# si on donne une réponse qui est une lettre ou un mauvais chiffre choix_mail sera vide 
				# alors on reboucle sur la liste des mails
		 		
				if [ "$CHOIX_MAIL" == "" ] || [ "$NB_LIGNE_MAIL" == "" ]
				then
					CHOIX_MAIL=""
					fait=0
				else
					# on renseigne le domaine en cours pour gérer les options non kaz.bzh
					#  ----------------------------------------------------------------------
				        DOMAINE_EN_COURS=$(echo ${CHOIX_MAIL} | sed -e 's/^.*@//')
					#  ----------------------------------------------------------------------
					fait=1
				fi
			done
		else
			searchEmail $1
		fi
}

searchMattermost() {
		#Ici $1 est une adresse email
            	docker exec -ti ${mattermostServName} bin/mmctl --suppress-warnings auth login $httpProto://$URL_AGORA --name local-server --username $mattermost_user --password $mattermost_pass >/dev/null 2>&1
		docker exec -ti ${mattermostServName} bin/mmctl --suppress-warnings config set ServiceSettings.EnableAPIUserDeletion "true" >/dev/null 2>&1
		#on créé la list des mails dans mattermost
		docker exec -ti ${mattermostServName} bin/mmctl --suppress-warnings user list --all >${TFILE_MAILS_MATTERMOST} 2>/dev/null
		REP_SEARCH_MATTERMOST=$(cat ${TFILE_MAILS_MATTERMOST} | grep $1 | awk '{print $2}'  | tr -d '[:space:]')
            	if [ ! -z ${REP_SEARCH_MATTERMOST} ]
            	then
			if [ "$2" = "DETAILS" ]
			then
				touch /tmp/$1-mattermost.json
				docker exec -ti ${mattermostServName} bin/mmctl --suppress-warnings --format json user search $1 >/tmp/$1-mattermost.json 2>/dev/null
                    		echo -e "${RED} Compte : ${GREEN} " ;jq .username /tmp/$1-mattermost.json
                   		echo -e "${RED} Mail : ${GREEN} " ; jq .email /tmp/$1-mattermost.json
                    		echo -e "${RED} Role : ${GREEN} " ; jq .roles /tmp/$1-mattermost.json
			else
                   		echo "${REP_SEARCH_MATTERMOST}"
			fi
            	else
                    echo ""
            	fi
}


infoEmail() {
		ACTION_EN_COURS="Information sur un compte"
		while :
		do
			clear
                	echo "------------------------------------------------"
			printKazMsg "${ACTION_EN_COURS}"
        	        echo "------------------------------------------------"
			read -p "Alias ou Mail ? (R pour retour ou M/A [M] :" RINFOMAIL
			case ${RINFOMAIL} in 
			"" | M | m )
					infofait=O
				 	searchEmail
					clear
					echo "------------------------------------------------"
					printKazMsg " ADRESSE DE MESSAGERIE :  $CHOIX_MAIL"
					echo "------------------------------------------------"
					printKazMsg "  DETAILS DU COMPTE DANS MATTERMOST"
					searchMattermost $CHOIX_MAIL DETAILS
					echo " ------------------------------------------------"
					printKazMsg " DETAILS DU COMPTE DANS NEXTCLOUD PRINCIPAL"
					echo -e ""
					#TEMP_USER_NC=$(mktemp /tmp/$RACINE.XXXXXXXXX.TEMP_USER_NC)
					#curl -s -o $TEMP_USER_NC -X GET -H 'OCS-APIRequest:true' $httpProto://admin:$nextcloud_NEXTCLOUD_ADMIN_PASSWORD@$URL_NC/ocs/v1.php/cloud/users?search=$CHOIX_MAIL
					#cat $TEMP_USER_NC | grep -i "element" | sed -e s/[\<\>\/]//g | sed -e s/element//g
					echo -ne "${NC}"
					echo -ne " - Nextcloud enable : "
					echo -ne "${GREEN}"
					ldapsearch -H ldap://${LDAP_IP} -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" -x -w "${ldap_LDAP_ADMIN_PASSWORD}" -b "cn=${CHOIX_MAIL},ou=users,${ldap_root}" | grep -i nextcloudEnabled | cut -c 18-30
					echo -ne "${NC}"
					echo -e "${NC} ------------------------------------------------"
					printKazMsg " DETAILS DU COMPTE DANS LDAP ET PAHEKO"
					echo ""
					curl -s ${URL_PAHEKO}/api/sql -d "SELECT nom,adresse,code_postal,ville,email,email_secours,admin_orga,nom_orga,quota_disque FROM users where email='${CHOIX_MAIL}' LIMIT 1;" >/tmp/$CHOIX_MAIL-paheko.json
					jq .results[].nom /tmp/$CHOIX_MAIL-paheko.json
					jq .results[].adresse /tmp/$CHOIX_MAIL-paheko.json
					jq .results[].code_postal /tmp/$CHOIX_MAIL-paheko.json
					jq .results[].ville /tmp/$CHOIX_MAIL-paheko.json
					echo -n " - Quota (Paheko) : "
					echo -ne "${GREEN}"
					jq .results[].quota_disque /tmp/$CHOIX_MAIL-paheko.json
					echo -ne "${NC}"
					echo -n " - Quota Mail (Ldap) : " 
					echo -ne "${GREEN}"
					ldapsearch -H ldap://${LDAP_IP} -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" -x -w "${ldap_LDAP_ADMIN_PASSWORD}" -b "cn=${CHOIX_MAIL},ou=users,${ldap_root}" | grep -i mailquota | cut -c 11-60
					echo -ne "${NC}"
					echo -n " - Quota Nextcloud (Ldap) : "
					echo -ne "${GREEN}"
					ldapsearch -H ldap://${LDAP_IP} -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" -x -w "${ldap_LDAP_ADMIN_PASSWORD}" -b "cn=${CHOIX_MAIL},ou=users,${ldap_root}" | grep -i nextcloudquota | cut -c 17-60
					echo -ne "${NC}"
					echo -n " - Mail de secours (Paheko ): "
					echo -ne "${GREEN}"
					jq .results[].email_secours /tmp/$CHOIX_MAIL-paheko.json | sed -e 's/"//g'
					echo -ne "${NC}"
					echo -n " - Mail de secours (Ldap): "
					echo -ne "${GREEN}"
					ldapsearch -H ldap://${LDAP_IP} -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" -x -w "${ldap_LDAP_ADMIN_PASSWORD}" -b "cn=${CHOIX_MAIL},ou=users,${ldap_root}" | grep -i maildeSecours | sed -e 's/mailDeSecours://'
					echo -ne "${NC}"
					echo -n " - Alias (Ldap) : "
					echo -ne "${GREEN}"
					LDAP_ALIAS=$(ldapsearch -H ldap://${LDAP_IP} -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" -x -w "${ldap_LDAP_ADMIN_PASSWORD}" -b "cn=${CHOIX_MAIL},ou=users,${ldap_root}" | grep -i alias | cut -c 11-60)
					echo -ne "${NC}"
					echo -ne "${GREEN}"
					for ldap_alias in ${LDAP_ALIAS}
					do
						echo "${ldap_alias}" | tr -d [:space:]
						echo -n " "
					done
					echo -e "${NC}"
					echo " - L' adresse apparait dans :"
					echo -n -e "${GREEN}"
					curl -s ${URL_PAHEKO}/api/sql -d "SELECT * FROM users where emails_Rattaches like '%$CHOIX_MAIL%' ;" |  jq .results[].nom
					echo -e "${NC}"
					read -p "         ==> Entrée pour continuer <=="
					;;
			A | a )
					searchEmail alias
                			echo "------------------------------------------------"
					echo  " Alias : ${CHOIX_MAIL} "
					echo ""
					for INFOALIAS in $(ldapsearch -H ldap://${LDAP_IP} -x -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" \
                		        -w "${ldap_LDAP_ADMIN_PASSWORD}" -b "${ldap_root}" "(&(objectclass=PostfixBookMailForward)(cn=*${CHOIX_MAIL}*))" mail \
					| grep ^mail: | sed -e 's/^mail://')
					do
						echo -ne "=====> ${GREEN}  " 
						echo "${INFOALIAS}" | tr -d [:space:]
						echo "${NC}"
					done
					echo ""
					read -p "==> Entrée pour continuer <=="
					;;
			r | R  )
					Main
					;;
			* )
					echo ""
					;;
			esac
		done
}

searchDestroy() {
			ACTION_EN_COURS="Suppression d'un compte"
			clear
			#TODO pourquoi REP_SEARCH_DESTROY=$(searchEmail) ne marche pas et fait déconner la fonction search_mail, j' en suis la à m' esbaudir
			CHOIX_MAIL=""
			searchEmail
			REP_SEARCH_DESTROY=$CHOIX_MAIL
			echo "CHOIX=$REP_SEARCH_DESTROY"
			echo "domaine en cours : ${DOMAINE_EN_COURS}"
			echo "---------------------------------   SUPPRESION ----------------------------------------"
			while :
			do
	                        echo "----------------------------------------------------------------------"
        	                printKazMsg "${GREEN}${ACTION_EN_COURS}${NC}"
                	        echo "----------------------------------------------------------------------"
				echo -e "${BLINK}           TOUT RETOUR EN ARRIERE EST IMPOSSIBLE ${NC}"
  				read -p "ON CONTINUE ? [ o / n ]: " SEARCH_DESTROY_INPUT
				if [ "$SEARCH_DESTROY_INPUT" = "n" ] || [ "$SEARCH_DESTROY_INPUT" = "N" ]
				then
					searchDestroy
				fi
				if [ "$SEARCH_DESTROY_INPUT" = "o" ] || [ "$SEARCH_DESTROY_INPUT" = "O" ]
				then
				####################################################################################################################
					# si le domaine en cours est Kaz alors on cherche dans mattermost, dans nextcloud et on supprime
					# le mail dans sympa
				####################################################################################################################
					if [ "${DOMAINE_EN_COURS}" = "${domain}" ]
					then
						REP_SEARCH=$(searchMattermost $REP_SEARCH_DESTROY)
						printKazMsg "réponse de mattermost : ${REP_SEARCH_DESTROY}"
						if [ ! -z ${REP_SEARCH} ]
						then
							echo -e "${RED} suppression de ${REP_SEARCH_DESTROY} dans mattermost ${NC}"
							docker exec -ti ${mattermostServName} bin/mmctl user delete ${REP_SEARCH_DESTROY} --confirm >/dev/null 2>&1
							if [ "$?" -eq "0" ]
                       					then
                       						printKazMsg "Suppresion ok"
                       					else
                          					printKazError -e "Erreur de suppression"
                     					fi
								echo "               -----------------              "
						else
							echo "Rien a supprimer dans mattermost"
						fi
						echo -e "${NC}"
						echo -e "Recherche de ${GREEN} ${REP_SEARCH_DESTROY} ${NC} dans nextcloud"
						USER_NEXTCLOUD_SUPPR=$(curl -s -X GET -H 'OCS-APIRequest:true' $httpProto://admin:$nextcloud_NEXTCLOUD_ADMIN_PASSWORD@$URL_NC/ocs/v1.php/cloud/users?search=${REP_SEARCH_DESTROY} | grep element | sed -s 's/[ \<\>\/]//g' | sed  's/element//g')
						if [ ! -z ${USER_NEXTCLOUD_SUPPR} ]
						then
							printKazMsg "le user trouvé est : ${USER_NEXTCLOUD_SUPPR}"
							echo -e "${RED} Suppresion de ${USER_NEXTCLOUD_SUPPR}"
							curl -H 'OCS-APIREQUEST: true' -X DELETE $httpProto://admin:$nextcloud_NEXTCLOUD_ADMIN_PASSWORD@$URL_NC/ocs/v1.php/cloud/users/${USER_NEXTCLOUD_SUPPR} >/dev/null 2>&1
							if [ "$?" -eq "0" ]
                        				then
								printKazMsg "Suppresion ok"
                        				else
                            					printKazError "Erreur de suppression"
                        				fi
						else
							printKazMsg "Rien à supprimer dans Nextcloud"
						fi
							echo -e "${NC}"
							echo ""
							echo -e "${RED} suppression de ${REP_SEARCH_DESTROY} dans la liste info de sympa"
							echo -e "${NC}"
							echo ""
							docker exec -ti sympaServ /usr/lib/sympa/bin/sympa_soap_client.pl --soap_url=${httpProto}://${URL_LISTE}/sympasoap --trusted_application=${sympa_SOAP_USER} --trusted_application_password=${sympa_SOAP_PASSWORD} --proxy_vars=USER_EMAIL=${LISTMASTER} --service=del --service_parameters="${NL_LIST},${REP_SEARCH_DESTROY}"
							echo -e "${NC}"
							echo ""
							echo  -e "${RED} suppression de ${REP_SEARCH_DESTROY} dans le serveur de mail"
							echo -e "${NC}"
							echo ""
					else
						echo "-------------------------------"
						echo "Le domaine n' est pas Kaz.bzh"
						echo "-------------------------------"
					fi
					############################## - Fin du cas de Kaz   ##########################################################################################
						echo -e "${NC}"
                                                echo ""
                                                echo  -e "${RED} suppression de ${REP_SEARCH_DESTROY} dans le ldap"
                                                echo -e "${NC}"
                                                echo ""
						ldapdelete -H ldap://${LDAP_IP} -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" -x -w "${ldap_LDAP_ADMIN_PASSWORD}" "cn=${REP_SEARCH_DESTROY},ou=users,${ldap_root}"
						if [ "$?" -eq "0" ]
						then
							printKazMsg "Suppresion ok"
						else
							printKazError "Erreur de suppression"
						fi
						printKazMsg  "Envoi d'un message dans mattermost pour la suppression du compte"
						docker exec -ti mattermostServ bin/mmctl post create kaz:Creation-Comptes --message "Le compte ${REP_SEARCH_DESTROY} est supprimé" >/dev/null 2>&1
						MAIL_SUPPR="Suppression du compte ${REP_SEARCH_DESTROY}"
						docker exec -i mailServ mailx -a 'Content-Type: text/plain; charset="UTF-8"' -r contact@${domain} -s "Suppression de mail" contact@${domain} << EOF
						${MAIL_SUPPR}
EOF
						echo -e "${NC}"
						read -p " ---------------------- Appuyer sur une touche pour continuer -------------------------"
						searchDestroy
				fi
			done
}

gestPassword() {  
		ACTION_EN_COURS="Gestion du mot de passe d' un compte"
		searchEmail
		#cree un mdp acceptable par postfix/nc/mattermost
		PASSWORD=_`apg -n 1 -m 10 -M NCL -d`_
		COMPTE_A_MODIFIER=${CHOIX_MAIL}

                # On considère le mot de passe de secours de ldap
		# FICMAILSECOURS=$(mktemp /tmp/FICMAILSECOURSXXXXX.json)
		# curl -s ${URL_PAHEKO}/api/sql -d "SELECT email_secours FROM users where email='${COMPTE_A_MODIFIER}' LIMIT 1 ;" >$FICMAILSECOURS
		# MAIL_SECOURS=$(jq .results[].email_secours $FICMAILSECOURS | sed -e 's/\"//g')

		MAIL_SECOURS=$(ldapsearch -H ldap://${LDAP_IP} \
                -x -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" \
                -w "${ldap_LDAP_ADMIN_PASSWORD}" \
                -b "${ldap_root}" "(&(objectclass=inetOrgPerson)(cn=*${CHOIX_MAIL}*))" | grep ^mailDeSecours | sed -e 's/^mailDeSecours: //')
		if [ "$MAIL_SECOURS" = "" ]
                then
			ADRESSE_SEC="NON"
		else
			ADRESSE_SEC="OUI"
                fi
		##############################################################################
		# on cherche le moyen de savoir si ce mail est le mail dans une orga
		# si c' est le cas ça veut dire qu' il faut changer le mot de passe dans le cloud de l' orga
		rm -rf /tmp/FICMAILORGA*
		EMAIL_ORGA_TROUVE=0
		EMAIL_ORGA_TROUVE=$(curl -s ${URL_PAHEKO}/api/sql -d "SELECT id FROM users where emails_Rattaches like '%$CHOIX_MAIL%' ;" |  jq .results[].id | wc -l)
		if [ $EMAIL_ORGA_TROUVE -ge 1 ]
		then
			printKazError "L' adresse apparait dans plusieurs orga comme mails rattaché IL EST PREFERABLE DE REPONDRE NON !!!"
		fi
		##########################################################################
		read -p "ON CONTINUE ? [ o / n ]: " SEARCH_RESET_INPUT
                if [ "$SEARCH_RESET_INPUT" = "n" ] || [ "$SEARCH_RESET_INPUT" = "N" ]
		then
			clear
			gestPassword
		fi
		if [ "$SEARCH_RESET_INPUT" = "o" ] || [ "$SEARCH_RESET_INPUT" = "O" ]
		then
			USER_NEXTCLOUD_MODIF=$(curl -s -X GET -H 'OCS-APIRequest:true' $httpProto://admin:$nextcloud_NEXTCLOUD_ADMIN_PASSWORD@$URL_NC/ocs/v1.php/cloud/users?search=${COMPTE_A_MODIFIER} | grep element | sed -e 's/[ \<\>\/]//g' -e 's/element//g')
			echo -e "$GREEN Compte à modifier = $RED ${COMPTE_A_MODIFIER}  ${NC}"
			echo -e "$GREEN Mail de secours = $RED ${MAIL_SECOURS}  ${NC}"
			echo -e "$GREEN Compte $RED $(searchMattermost $COMPTE_A_MODIFIER)  ${NC}"
			echo -e "$GREEN Compte Nextcloud $RED ${USER_NEXTCLOUD_MODIF} ${NC}"
			echo -e "$GREEN Le mot de passe sera = $RED ${PASSWORD} ${NC}"
			docker exec -ti mattermostServ bin/mmctl user change-password $(searchMattermost $COMPTE_A_MODIFIER) -p $PASSWORD >/dev/null 2>&1
			curl -H 'OCS-APIREQUEST: true' -X PUT $httpProto://admin:$nextcloud_NEXTCLOUD_ADMIN_PASSWORD@$URL_NC/ocs/v1.php/cloud/users/${USER_NEXTCLOUD_MODIF} -d key=password -d value=${PASSWORD} >/dev/null 2>&1
			pass=$(mkpasswd -m sha512crypt ${PASSWORD})
			echo -e "\n\ndn: cn=${COMPTE_A_MODIFIER},ou=users,${ldap_root}\n\
changeType: modify\n\
replace: userPassword\n\
userPassword: {CRYPT}${pass}\n\n" | ldapmodify -c -H ldap://${LDAP_IP} -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" -x -w "${ldap_LDAP_ADMIN_PASSWORD}"
			echo -e "Envoi d'un message dans mattermost pour la modification du mot de passe"
			docker exec -ti mattermostServ bin/mmctl post create kaz:Creation-Comptes --message "Le mot de passe du compte ${COMPTE_A_MODIFIER} a été modifié" >/dev/null 2>&1
			if [ $ADRESSE_SEC == "OUI" ]
			then
				echo -e "Envoi d'un message à l' adresse de secours : $GREEN${MAIL_SECOURS}${NC}"
				MAIL_CHANG="
Hello,

le nouveau mot de passe de ${COMPTE_A_MODIFIER} est : ${PASSWORD}

le webmail est disponible à https://webmail.kaz.bzh
Le site d' aide pour la configuration des services : https://wiki.kaz.bzh
Le site Web de Kaz : https://kaz.bzh
Le mail de la collégiale : contact@kaz.bzh

A bientôt"
				docker exec -i mailServ mailx -a 'Content-Type: text/plain; charset="UTF-8"' -r admin@${domain} -s "Modification du compte" $MAIL_SECOURS << EOF
				${MAIL_CHANG}
EOF
			fi
			if [ $ADRESSE_SEC == "NON" ]
			then
				echo -e "${RED} Pas d adresse de secours ${NC}"
			fi
		fi

}

createMail() {
	ACTION_EN_COURS="Création d' une adresse de messagerie"
	PRENOM=""
	NOM=""
	QUOTA=1
	TRUE_KAZ=TRUE
	fait=0
	# On demande le mail souhaite on regarde si c' est valide et si ça existe déjà 
	while [ $fait -eq 0 ]
	do
		clear
		echo "----------------------------------------------------------------------"
		printKazMsg "${ACTION_EN_COURS}"
		echo "----------------------------------------------------------------------"
		read -p "Mail souhaité (r ou R pour quitter ) : " EMAIL_SOUHAITE
		[[ ${EMAIL_SOUHAITE} =~ ^[rRqQ]$ ]] && Main
		if [[ ${EMAIL_SOUHAITE} =~ ${regexMail} ]] 
		then
			ldapsearch -H ldap://${LDAP_IP} \
                        -x -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" \
                        -w "${ldap_LDAP_ADMIN_PASSWORD}" \
                        -b "${ldap_root}" "(&(objectclass=inetOrgPerson)(cn=${EMAIL_SOUHAITE}))" cn | grep ^cn | sed -e 's/^cn: //' >$TFILE_EMAILS
	        	if grep -q "^${EMAIL_SOUHAITE}$" "${TFILE_EMAILS}"
			then
				printKazError " - Le mail ${EMAIL_SOUHAITE} existe déjà"
				fait=0
			elif ! grep $(echo ${EMAIL_SOUHAITE} | sed -e 's/^.*@//') ${KAZ_CONF_DIR}/autorized-domains.txt >/dev/null
			then
				printKazMsg  "Le domaine n' est pas dans la liste autorisée"
				sleep 2
				fait=0
			else
				fait=1
			fi
		else
			printKazError  " - Mail invalide !"
			sleep 2
			fait=0
		fi
	done
	# on renseigne le domaine en cours pour  les options qui concernent pas le domaine kaz.bzh
	#  ----------------------------------------------------------------------
        DOMAINE_EN_COURS=$(echo ${EMAIL_SOUHAITE} | sed -e 's/^.*@//')
        echo "   ---- Domaine: " ${DOMAINE_EN_COURS} ----   
	#  ----------------------------------------------------------------------
	# On demande le mail de secours et on teste si c' est un mail valide
	fait=0
	while [ $fait -eq 0 ]
        do
                read -p "Mail de secours : " EMAIL_SECOURS
                if [[ ${EMAIL_SECOURS} =~ ${regexMail} ]] && [ "${EMAIL_SECOURS}" != "" ]
                then
			fait=1
                else
                        printKazError "Mail invalide"
                        fait=0
                fi
		if [ "${EMAIL_SECOURS}" = "${EMAIL_SOUHAITE}" ]
                then
                         echo -e "${RED}Le mail de secours ne peut pas etre le meme que le mail demande${NC}"
                         fait=0
                 fi
        done
	# On demande le prenom
        while [ "${PRENOM}" = "" ]
        do
                read -p "Prenom : " PRENOM
        done
        # On demande le nom
        while [ "${NOM}" = "" ]
        do
                read -p "Nom : " NOM
        done
	# On constitue l' ident kaz pour le ldap
	IDENT_KAZ=$(unaccent utf8 "${PRENOM,,}.${NOM,,}")
	# on créé le mot de passe
	PASSWORD=_`apg -n 1 -m 10 -M NCL -d`_
	# on constitue le user,domain et pass crypté pour le ldap
       	LDAPUSER=$(echo ${EMAIL_SOUHAITE} | awk -F '@' '{print $1}')
       	LDAPDOMAIN=$(echo ${EMAIL_SOUHAITE} | awk -F '@' '{print $2}')
       	LDAPPASS=$(mkpasswd -m sha512crypt ${PASSWORD})	
	[ "${DOMAINE_EN_COURS}" != "${domain}" ] && TRUE_KAZ=FALSE
	echo "${GREEN}Mail souhaité : ${NC}${EMAIL_SOUHAITE}"
	echo "${GREEN}Mail secours  : ${NC}${EMAIL_SECOURS}"
	echo "${GREEN}Prénom        : ${NC}${PRENOM}"
	echo "${GREEN}Nom           : ${NC}${NOM}"
	echo "${GREEN}Mot de passe  : ${NC}${PASSWORD}"
	RCREATION=""
	repRegEx='^[oOnN]$'
	while ! [[ ${RCREATION} =~ ${repRegEx} ]]
	do
		read -p "Création ? o/n : " RCREATION
	done
	if [ ${RCREATION} = "n" ] || [ ${RCREATION} = "N" ]
	then
		Main
	fi
       	echo "echo -e '\n\ndn: cn=${EMAIL_SOUHAITE},ou=users,${ldap_root}\n\
changeType: add\n\
objectclass: inetOrgPerson\n\
objectClass: PostfixBookMailAccount\n\
objectClass: nextcloudAccount\n\
objectClass: kaznaute\n\
sn: ${PRENOM} ${NOM}\n\
mail: ${EMAIL_SOUHAITE}\n\
mailEnabled: TRUE\n\
mailGidNumber: 5000\n\
mailHomeDirectory: /var/mail/${LDAPDOMAIN}/${LDAPUSER}/\n\
mailQuota: ${QUOTA}G\n\
mailStorageDirectory: maildir:/var/mail/${LDAPDOMAIN}/${LDAPUSER}/\n\
mailUidNumber: 5000\n\
mailDeSecours: ${EMAIL_SECOURS}\n\
identifiantKaz: ${IDENT_KAZ}\n\
quota: ${QUOTA}\n\
nextcloudEnabled: ${TRUE_KAZ}\n\
nextcloudQuota: ${QUOTA} GB\n\
mobilizonEnabled: ${TRUE_KAZ}\n\
agoraEnabled: ${TRUE_KAZ}\n\
userPassword: {CRYPT}${LDAPPASS}\n\n' | ldapmodify -c -H ldap://${LDAP_IP} -D \"cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}\" -x -w ${ldap_LDAP_ADMIN_PASSWORD}" >${TFILE_CREATE_MAIL}
# on execute le fichier avec les données ldap pour créer l' entrée dans l' annuaire
bash ${TFILE_CREATE_MAIL} >/dev/null
# on colle le compte et le mot de passe dans le fichier 
echo "Création de : ${EMAIL_SOUHAITE} avec le mot de passe : ${PASSWORD}" >>${TFILE_CREATE_MAIL}
OLDIFS=${IFS}
IFS=''
MSG_CREATION_MAIL="Bonjour

Le compte de messagerie ${EMAIL_SOUHAITE} a été créé avec le mot de passe ${PASSWORD}

Le mot de passe peut être changé à l' adresse https://mdp.${domain}

$(cat ${KAZ_CONF_DIR}/domains/${DOMAINE_EN_COURS})

Bonne journée"
ExpMail ${EMAIL_SECOURS} "Creation de ${EMAIL_SOUHAITE}" ${MSG_CREATION_MAIL}
docker exec -ti mattermostServ bin/mmctl post create kaz:Creation-Comptes --message "création du compte ${EMAIL_SOUHAITE} avec l' adresse de secours ${EMAIL_SECOURS}" >/dev/null 2>&1
IFS=${OLDIFS}
echo " --------------------------------------------------------------------"
echo "!            INFO :                                                  !"
echo "!            Le Mot de passe                                         !"
echo "!            Est dans le fichier                                     !"
echo "!            ${TFILE_CREATE_MAIL}          !"
echo " --------------------------------------------------------------------"
echo
read -p " -  ENTREE pour continuer - "
# Retour au menu principal
Main
}

createAlias() {
	ACTION_EN_COURS="création d' un alias de messagerie"
	fait=0
	# On demande alias souhaite on regarde si c' est valide et si ça existe déjà 
	while [ $fait -eq 0 ]
	do
		clear
		echo "-------------------------------------------------"
		printKazMsg "${ACTION_EN_COURS}"
		echo "-------------------------------------------------"
		read -p "Alias souhaité (r ou q pour quitter ) : " AMAIL
		[[ ${AMAIL} =~ ^[rRqQ]$ ]] && Main
		if [[ ${AMAIL} =~ ${regexMail} ]] 
		then
			RESU_ALIAS=$(ldapsearch -H ldap://${LDAP_IP} \
			-x -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" \
			-w "${ldap_LDAP_ADMIN_PASSWORD}" \
			-b "${ldap_root}" "(&(objectclass=PostfixBookMailForward)(cn=*${AMAIL}*))" | grep ^cn | sed -e 's/^cn: //')
			RESU_ALIAS_IS_MAIL=$(ldapsearch -H ldap://${LDAP_IP} \
 	                -x -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" \
			-w "${ldap_LDAP_ADMIN_PASSWORD}" \
             		-b "${ldap_root}" "(&(objectclass=inetOrgPerson)(cn=*${AMAIL}*))" cn | grep ^cn | sed -e 's/^cn: //')

	        	if echo ${RESU_ALIAS} | grep -q "^${AMAIL}$" || echo ${RESU_ALIAS_IS_MAIL} | grep -q "^${AMAIL}$"
			then
				echo "${GREEN} - alias ou mail ${AMAIL} déjà dans présent l' annuaire${NC}"
				sleep 2
				fait=0
			elif ! grep $(echo ${AMAIL} | sed -e 's/^.*@//') ${KAZ_CONF_DIR}/autorized-domains.txt >/dev/null
			then
				echo -e "${GREEN}Le domaine n' est pas dans la liste autorisée${NC}"
				sleep 2
				fait=0
			else
				fait=1
			fi
		else
			echo -e "${GREEN} - Mail invalide !${NC}"
			sleep 2
			fait=0
		fi
	done
	echo "Alias : ${AMAIL} peut être créé"
	declare -a TABAMAIL
	fait=0
	while [ $fait -eq 0 ]
	do
		clear
		echo "-----------------------------------------------------------------"
		echo "Ajout de mail pour cet alias"
		echo "-----------------------------------------------------------------"
		[ "${TABAMAIL[*]}" == "" ] || echo -e "Liste des mails dans cet alias: [ ${GREEN}${TABAMAIL[*]}${NC} ]"
		read -p "ENTREE ou F ( pour fin :-) pour une liste vide ou saisir une adresse et enfin saisir F pour arrêter : " RALIAS
		if [[ ${RALIAS} =~ ^[Ff]$ ]] || [ "${RALIAS}" == "" ]
		then
			fait=1
		else
			if echo ${TABAMAIL[*]} | grep -i ${RALIAS} >/dev/null 2>&1
			then
				echo -e "${GREEN}l' alias est déjà dans la liste ${NC}"
				sleep 2
			else
				if [[ ${RALIAS} =~ ${regexMail} ]]
				then
					TABAMAIL+=(${RALIAS})
				else
					echo -e "${GREEN} - Mail invalide !${NC}"
					sleep 2
				fi
			fi
		fi
	done
	fait=0
	while [ "$fait" = 0 ]
	do	
		clear
		echo "--------------------------------------------------"
		echo -e "${GREEN}résumé de la situation${NC}"
		echo "--------------------------------------------------"
		echo -e "création d' un alias : ${GREEN}${AMAIL}${NC}"
		echo -e "avec le(s) Mail suivant : ${GREEN}${TABAMAIL[*]}${NC}"
		echo "--------------------------------------------------"
		RCREATONALIAS=""
		read -p "OK pour la création ?(o/n) : " RCREATIONALIAS
		case "$RCREATIONALIAS" in
		o | O )
			for ALIASMAIL in ${TABAMAIL[*]}
			do
				LDAPALAISMAIL="${LDAPALAISMAIL}$(echo mail :${ALIASMAIL})\n"
			done
			echo -e "\n\ndn: cn=${AMAIL},ou=mailForwardings,${ldap_root}\n\
changeType: add\n\
objectClass: organizationalRole\n\
objectClass: PostfixBookMailForward\n\
mailAlias: ${AMAIL}\n\
${LDAPALAISMAIL}\n\n" | ldapmodify -c -H ldap://${LDAP_IP} -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" -x -w ${ldap_LDAP_ADMIN_PASSWORD}
			fait=1
			printKazMsg "Création de ${AMAIL}"
			sleep 3			
			createAlias
			;;
		 n | N )
			printKazMsg "abandon de création"
			sleep 2
			createAlias
			;;
		* )
			fait=0
			;;
		esac
	done
}

delAlias() {
	ACTION_EN_COURS="Suppresion d' un alias de messagerie"
 	fait=0
        while [ $fait -eq 0 ]
        do
		RALIAS=""
		CHOIX_MAIL=""
		RESU_ALIAS=""
		searchEmail alias
		RALIAS=${CHOIX_MAIL}	
                [[ ${RALIAS} =~ ^[rRqQ]$ ]] && Main
                if [[ ${RALIAS} =~ ${regexMail} ]]
                then
                	RESU_ALIAS=$(ldapsearch -H ldap://${LDAP_IP} \
                        -x -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" \
                        -w "${ldap_LDAP_ADMIN_PASSWORD}" \
                        -b "${ldap_root}" "(&(objectclass=PostfixBookMailForward)(cn=${RALIAS}))" cn | grep ^cn | sed -e 's/^cn: //')
			if [ ! -z ${RESU_ALIAS} ]
			then
				faitdel=0
				while [ ${faitdel} = 0 ]
				do
					read -p "suppression de ${RESU_ALIAS} ? (o/n): " REPDELALIAS
					case "${REPDELALIAS}" in
					o | O )
                        			ldapdelete -H ldap://${LDAP_IP} -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" -x -w "${ldap_LDAP_ADMIN_PASSWORD}" "cn=${RESU_ALIAS},ou=mailForwardings,${ldap_root}"
						printKazMsg "suppression ${RESU_ALIAS} effectuée"
						sleep 2
						faitdel=1
						;;
					n | N )
						faitdel=1
						;;
					"" | * )
						faitdel=0
						;;
						
					esac		
				done
			else
				fait=0
			fi
				
                else
                         printKazError " - format alias invalide !"
                         sleep 2
                         fait=0
                fi
        done
	delAlias
}

modifyAlias() 
{
        ACTION_EN_COURS="Modfication d' un alias de messagerie"
	MRESU_ALIAS=""
	LISTE_MAIL_ALIAS=""
	NEW_LISTE_MAIL_ALIAS=""	
	ACHANGE=0
	searchEmail alias
	LISTE_MAIL_ALIAS=$(ldapsearch -H ldap://${LDAP_IP} \
        -x -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" \
        -w "${ldap_LDAP_ADMIN_PASSWORD}" \
        -b "${ldap_root}" "(&(objectclass=PostfixBookMailForward)(cn=*${CHOIX_MAIL}*))" \
	| grep -i ^mail: | sed -e 's/^mail: /_/' | tr -d [:space:] | sed -s 's/_/ /g')
	echo "-------------------------------------------------------------------"
	echo "-------------------------------------------------------------------"
	echo -e "${GREEN} [  ${LISTE_MAIL_ALIAS}  ] ${NC} "
	echo "-------------------------------------------------------------------"
	BOUCLE_M_MAIL=0
	NEW_LISTE_MAIL_ALIAS=""
	for M_MAIL in ${LISTE_MAIL_ALIAS}
        do
        read -p " - On garde l' alias ${M_MAIL} (o/n) ? [o] : " READM_MAIL
        case  ${READM_MAIL} in
        "" | o | O )
			NEW_LISTE_MAIL_ALIAS="${NEW_LISTE_MAIL_ALIAS} ${M_MAIL}"
                  	;;
        n | N )
			ACHANGE=$(expr ${ACHANGE} + 1)
        		NEW_LISTE_MAIL_ALIAS=${NEW_LISTE_MAIL_ALIAS}
                  	;;
	* )
			NEW_LISTE_MAIL_ALIAS="${NEW_LISTE_MAIL_ALIAS} ${M_MAIL}"
			;;
        esac
        done
	TALIAS_SUPP=""
	BOUCLE_ADD_MAIL=0
	BOUCLE_ALIAS=0
	read -p "Ajouter un alias ? (o/n) [n] :" RALIAS_SUPP
	while [ $BOUCLE_ADD_MAIL = 0 ]
	do
       	case ${RALIAS_SUPP} in
	o | O )
        		while [ ${BOUCLE_ADD_MAIL} = 0 ]
        		do
       				read -p " - Nouvel Alias: ( F pour finir ) :  " ALIAS_SUPP
        			if [[ ${ALIAS_SUPP} =~ ${regexMail} ]]
				then
					if echo ${NEW_LISTE_MAIL_ALIAS} | grep -w ${ALIAS_SUPP} >/dev/null
               				then
						printKazMsg "Alias déjà existant"
						BOUCLE_ADD_MAIL=0
               				else
						NEW_LISTE_MAIL_ALIAS="${NEW_LISTE_MAIL_ALIAS} ${ALIAS_SUPP}"
						ACHANGE=$(expr ${ACHANGE} + 1)
               				fi
         			elif [[ ${ALIAS_SUPP} =~ ^[[fF]+$ ]]
         			then
					BOUCLE_ADD_MAIL=1
				else
					printKazMsg "erreur de mail"
				fi		
        		done
         		;;
	 "" | n | N )
			BOUCLE_ADD_MAIL=1
			;;
	* )	
			BOUCLE_ADD_MAIL=1
			;;
	esac
	done
		if [ "${ACHANGE}" -gt "0" ]
		then
			printKazMsg "Changement à effectuer : ${NEW_LISTE_MAIL_ALIAS}"
			sleep 3
			FIC_MODIF_LDIF=$(mktemp /tmp/$RACINE.XXXXXXXXX.TFIC_MODIF_LDIF.ldif)
			echo "dn: cn=${CHOIX_MAIL},ou=mailForwardings,${ldap_root}" >${FIC_MODIF_LDIF}
			echo "changeType: modify" >>${FIC_MODIF_LDIF}
			echo "replace: mail" >>${FIC_MODIF_LDIF}
			for key in ${NEW_LISTE_MAIL_ALIAS}
	                do
				echo "mail: ${key}" >>${FIC_MODIF_LDIF}
			done
			echo "-" >>${FIC_MODIF_LDIF}
		 	ldapmodify -c -H ldap://${LDAP_IP} -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" \
			-x -w ${ldap_LDAP_ADMIN_PASSWORD} \
			-f ${FIC_MODIF_LDIF} >/dev/null
		else
			printKazMsg "Pas de changement"
			sleep 3
		fi
}

updateUser() {
	rm -rf /tmp/*attributs.txt 
	ACTION_EN_COURS="Modification d'un compte"
	ATTRIB_MAILS="mailDeSecours mailAlias"
	ATTRIB_QUOTA="mailQuota nextcloudQuota"
	rm -rf /tmp/*fic_attributs.txt
	while :
	do
		FIC_ATTRIBUTS=$(mktemp /tmp/XXXXfic_attributs.txt)
		CHOIX_MAIL=""
		declare -a ATTRIB
		searchEmail
		MAILDESECOURSACTUEL=""
		MAILDESECOURS=""
		Ucompteur=1
		for attribut in mailDeSecours mailAlias mailQuota nextcloudQuota
		do
			ATTRIB+=([${attribut}]=$(ldapsearch -H ldap://${LDAP_IP} \
	               	-x -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" \
                	-w "${ldap_LDAP_ADMIN_PASSWORD}" \
                	-b "${ldap_root}" "(&(objectclass=inetOrgPerson)(cn=*${CHOIX_MAIL}*))" \
			 | grep ^"${attribut}": | sed -e 's/^'${attribut}': //' | tr -s '[:space:]' ' ' ))
			# si l' attribut est mailDesecours on l' attrape et on on le stocke pour pouvoir l' enlever de sympa
			[ ${attribut} = mailDeSecours ] && MAILDESECOURSACTUEL=${ATTRIB[${attribut}]}
			echo -e "${Ucompteur} - ${attribut} = ${ATTRIB[${attribut}]}" >>${FIC_ATTRIBUTS}
			Ucompteur=$(expr ${Ucompteur} + 1)
		done
		faitAttrib=0
		declare -A CHANGED
		while [ ${faitAttrib} = 0 ]
		do
			clear
			echo "---------------------------------------------------"
			printKazMsg "${ACTION_EN_COURS} ${CHOIX_MAIL}"
			echo "---------------------------------------------------"
			[ "${#CHANGED[@]}" -gt "0" ] && echo "------   Les Modification en cours   -------------"
			for key in "${!CHANGED[@]}"
			do
				echo "${GREEN}${key}${NC}: ${CHANGED[$key]}"
			done
			[ "${#CHANGED[@]}" -gt "0" ] && echo "---------------------------------------------------"
			cat ${FIC_ATTRIBUTS}
			read -p "Quel attribut est à changer  ? (R ou r pour retour F pour Finir ): " REP_ATTRIBUT
			case "${REP_ATTRIBUT}" in
			r | R )
				if [ "${#CHANGED[@]}" -gt "0" ]
				then
					echo "------------------------------------------------------------------------"
					read -p "=====> ATTENTION : il y a des modifs en cours abandonner ?(o ou n) ? <===== : " RABANDON
					case "${RABANDON}" in 
					o | O )
						faitAttrib=1
						;;
					n | N )
						faitAttrib=0
						;;
					* )
						echo ""
						;;
					esac	
				else
					faitAttrib=1
				fi
				;;
			[0-9] )
				if [ "${REP_ATTRIBUT}" -gt "$(expr ${Ucompteur} - 1 )" ]
				then
					printKazMsg "Chiffre invalide"
					sleep 3
				else
					# pour être sur de virer tous les espaces et les tab etc on utilise [:space:]
					# on affiche tout les arguments ( les $1 $2 $2 avec la boucle for)
					# on ajoute de ___ pour pouvoir le remplace par un espace entre chaque variable
					# afin d' afficher un beau contenu1 contenu2 contenu 3 

					ATTRIBUT_EN_COURS=$(cat ${FIC_ATTRIBUTS} | grep "^${REP_ATTRIBUT}\b" | awk '{print $3}')
					CONTENU_ATTRIBUT=$(cat ${FIC_ATTRIBUTS} | grep "^${REP_ATTRIBUT}\b" \
					| awk '{for (i=5; i<=NF; i++) print $i"___"}'\
					| tr -d [:space:] | sed -e 's/___/ /g')

					case ${ATTRIBUT_EN_COURS} in
					mailQuota | nextcloudQuota )
							echo "------------------------------------------------"
							read -p " - Nouveau ${ATTRIBUT_EN_COURS} (R ou r pour retour) : " RCHANGE
							if [[ ${RCHANGE} =~ ^[0-9]+$ ]]
							then
								[ "${ATTRIBUT_EN_COURS}" = "mailQuota" ] && CHANGED+=([mailQuota]="${RCHANGE}G")
								[ "${ATTRIBUT_EN_COURS}" = "nextcloudQuota" ] && CHANGED+=([nextcloudQuota]="${RCHANGE} GB")
							else
								printKazError "Valeur incorecte"
								sleep 3
							fi
							faitAttrib=0
							;;
					mailDeSecours )
							echo "------------------------------------------------"
							read -p " - Nouveau Mail de Secours  : " RCHANGE
							if [[ ${RCHANGE} =~ ${regexMail} ]]  	
							then
								CHANGED+=([mailDeSecours]=${RCHANGE})
							else
								printKazMsg "Mail invalide"
								sleep 3
							fi
							faitAttrib=0
							;;
					mailAlias )
							NEW_CONTENU_ATTRIBUT=""
							READVALMAIL=""
							ALIAS_SUPP=""
							RALIAS_SUPP=""
							TALIAS_SUPP=""
							BOUCLE_ADD_MAIL=0
							MAILALIAS_CHANGE=0
							for VALMAIL in ${CONTENU_ATTRIBUT}
							do
								read -p " - On garde ${VALMAIL} (o/n) ? [o] : " READVALMAIL
								case  ${READVALMAIL} in
								* | "" | o | O )
									NEW_CONTENU_ATTRIBUT="${NEW_CONTENU_ATTRIBUT} ${VALMAIL}"
									;;
								n | N )
									NEW_CONTENU_ATTRIBUT="${NEW_CONTENU_ATTRIBUT}"
									MAILALIAS_CHANGE=$(expr ${MAILALIAS_INCHANGE} + 1)
									;;
								* )
									printKazError "Erreur"
									sleep 2
								esac
							done
							read -p "Ajouter un alias ? (o/n) [n] :" RALIAS_SUPP
							case ${RALIAS_SUPP} in
							o | O )
								while [ ${BOUCLE_ADD_MAIL} = 0 ]
								do
									read -p " - ${GREEN}Nouvel Alias: ( F pour finir ) :${NC} " ALIAS_SUPP
									BOUCLE_ADD_MAIL=0
									if [[ ${ALIAS_SUPP} =~ ${regexMail} ]] 
									then
										if echo "${CONTENU_ATTRIBUT}" | grep  "^${ALIAS_SUPP}$"
										then
											printKazMsg "Alias déjà existant"
											sleep 3
										else
											TALIAS_SUPP="${TALIAS_SUPP} ${ALIAS_SUPP}"
											MAILALIAS_CHANGE=$(expr ${MAILALIAS_INCHANGE} + 1)
										fi
									fi
									if [[ ${ALIAS_SUPP} =~ ^[[fF]+$ ]]
									then
										BOUCLE_ADD_MAIL=1
									fi
								done
								;;
							"" | n | N  )
									#CHANGED+=([mailAlias]="${NEW_CONTENU_ATTRIBUT}")
									;;
							* )
									printKazMsg "Erreur"
									sleep 2
									;;
							esac 
							[ "${MAILALIAS_CHANGE}" -gt "0" ] && CHANGED+=([mailAlias]="${NEW_CONTENU_ATTRIBUT} ${TALIAS_SUPP}")
							faitattrib=0
							;;
					esac
				fi
				;;
			f | F )
				if [ "${#CHANGED[@]}" -eq "0" ]
				then
					printKazMsg "Il n'y a pas eu de modif"
					sleep 3
					updateUser
				else
					echo "Départ des modifs"
					LISTE_MODIF_USER=""
					FIC_MODIF_LDIF=$(mktemp /tmp/$RACINE.XXXXXXXXX.TFIC_MODIF_LDIF.ldif)
					echo "dn: cn=${CHOIX_MAIL},ou=users,${ldap_root}" >${FIC_MODIF_LDIF}
					echo "changeType: modify" >>${FIC_MODIF_LDIF}
					for key in "${!CHANGED[@]}"
	                         	do
						if [ ${key} = "mailDeSecours" ]
						then
							MAILDESECOURS=${CHANGED[mailDeSecours]}
						fi
						if [ ${key} = "mailAlias" ]
						then
							echo "replace: ${key}" >>${FIC_MODIF_LDIF}
							for KEY_MAILALIAS in ${CHANGED[$key]}
							do
								echo "${key}: ${KEY_MAILALIAS}" >>${FIC_MODIF_LDIF}
							done
							echo "-" >>${FIC_MODIF_LDIF}
						else
							echo "replace: ${key}" >>${FIC_MODIF_LDIF}
							echo "${key}: ${CHANGED[$key]}" >>${FIC_MODIF_LDIF}
							echo "-" >>${FIC_MODIF_LDIF}
						fi
					done
					cat ${FIC_MODIF_LDIF}
					sleep 3
					ldapmodify -c -H ldap://${LDAP_IP} -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" \
					-x -w ${ldap_LDAP_ADMIN_PASSWORD} \
					-f ${FIC_MODIF_LDIF}
					if [ ! -z ${MAILDESECOURS} ]
					then
						# suppression du mail de secours de la liste infos
						docker exec -ti sympaServ /usr/lib/sympa/bin/sympa_soap_client.pl --soap_url=${httpProto}://${URL_LISTE}/sympasoap --trusted_application=${sympa_SOAP_USER} --trusted_application_password=${sympa_SOAP_PASSWORD} --proxy_vars=USER_EMAIL=${LISTMASTER} --service=del --service_parameters="${NL_LIST},${MAILDESECOURSACTUEL}"
						# ajout de l' adresse  de la nouvelle adresse de secours
						docker exec -ti sympaServ /usr/lib/sympa/bin/sympa_soap_client.pl --soap_url=${httpProto}://${URL_LISTE}/sympasoap --trusted_application=${sympa_SOAP_USER} --trusted_application_password=${sympa_SOAP_PASSWORD} --proxy_vars=USER_EMAIL=${LISTMASTER} --service=add --service_parameters="${NL_LIST},${MAILDESECOURS}"
					fi
					updateUser
				fi
				;;
			"" | * )
				faitAttrib=0	
				;;
			esac
		done
	done

}
##############################################################################################################
##############################################################################################################
usage () {
        printKazMsg "$PRG [-h] [-v]"
        printKazMsg "script de gestion des kaznautes"
        printKazMsg "   -h aide sur ce script"
        printKazMsg "   -v version du script."
}
##############################################################################################################
# test des dépendances
type -P jq | grep -i jq >/dev/null || { echo "erreur jq n' est pas installé ( apt install jq ? ) " ; exit;}
################ Main loop ############################################
Main() {

echo -e "${NC}"
while true
do
clear
echo "-------------------------------------------------"
echo "$PRG, version : $VERSION ($KAZ_ROOT)"
echo "-------------------------------------------------"
echo -e "${BOLD}COMPTES : "
echo -e "${NC}*********"
echo -e "${NC}"
echo -e " ${NC}1 - Information sur un compte (alias ou Mail )"
echo -e " ${YELLOW}2 - Modification d' un compte"
echo -e " ${NC}3 - Changement de mot de passe d' un compte"
echo -e " ${GREEN}4 - Suppression d' un compte "
echo -e " ${NC}5 - Création d' une adresse de messagerie"
echo -e "${NC}"
echo -e "${BOLD}ALIAS : "
echo -e "${NC}*******"
echo -e "${NC}"
echo -e " ${NC}6 - Création d' un alias Général pour mails Externes"
echo -e " ${YELLOW}7 - Modification d' un alias Général pour mails Externes"
echo -e " ${GREEN}8 - Suppression d' un alias Général pour mails Externes"
echo -e "${NC}"
echo "-------------------------------------------------"
echo -e "${NC}"
read -p "Choix ? (Q pour quitter) ==> : " CHOICE
case "$CHOICE" in
        '1' )
		infoEmail
		;;
        '2' )
		updateUser
		;;
        '3' )
		gestPassword
		;;
        '4' )
		searchDestroy
		;;	
        '5' )
		createMail
		;;	
        '6' )
		createAlias
		;;	
        '7' )
		modifyAlias
		;;	
        '8' )
		delAlias
		;;	
        'h'| "H" )
		clear
		echo "--------------------------------------------"
                usage
		echo "--------------------------------------------"
		;;
        'q'| "Q" )
                exit
		;;
	""|'*' )
		clear
		;;
esac
done
}


##################################################################################################################################

# Lancement de la boucle principale
#traitement des options ( usage )

case "$1" in
	'-h' )
		usage
		exit
		;;
	'-v' )
		printKazMsg "$PRG version $VERSION"
		exit
		;;
esac

# on teste la présence du domaine du site dans le fichier autorized-domains.txt
[ ! -e ${KAZ_CONF_DIR}/autorized-domains.txt ] && { echo "création de ${KAZ_CONF_DIR}/autorized-domains.txt" ; touch ${KAZ_CONF_DIR}/autorized-domains.txt;}
! grep $domain ${KAZ_CONF_DIR}/autorized-domains.txt && echo $domain >> ${KAZ_CONF_DIR}/autorized-domains.txt
Main

