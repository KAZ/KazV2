#!/bin/bash

KAZ_ROOT=$(cd "$(dirname $0)/.."; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

cd "${KAZ_ROOT}"

MY_MAIN_IP=$(ip a | grep "inet " | head -2 | tail -1 | sed "s%.*inet *\([0-9.]*\)/.*%\1%")
MY_SECOND_IP=$(ip a | grep "inet " | head -3 | tail -1 | sed "s%.*inet *\([0-9.]*\)/.*%\1%")

DOMAIN="kaz.local"
DOMAIN_SYMPA="kaz.local"
HTTP_PROTO="https"
MAIN_IP="${MY_MAIN_IP}"
SYMPA_IP="MY_SECOND_IP"
RESTART_POLICY="no"
JIRAFEAU_DIR="/var/jirafeauData/$(apg -n 1 -m 16 -M NCL)/"

DOCKERS_TMPL_ENV="${KAZ_CONF_DIR}/dockers.tmpl.env"

RESET_ENV="true"
if [ -f "${DOCKERS_ENV}" ]; then
    DOMAIN=$(getValInFile "${DOCKERS_ENV}" "domain")
    DOMAIN_SYMPA=$(getValInFile "${DOCKERS_ENV}" "domain_sympa")
    HTTP_PROTO=$(getValInFile "${DOCKERS_ENV}" "httpProto")
    MAIN_IP=$(getValInFile "${DOCKERS_ENV}" "MAIN_IP")
    SYMPA_IP=$(getValInFile "${DOCKERS_ENV}" "SYMPA_IP")
    RESTART_POLICY=$(getValInFile "${DOCKERS_ENV}" "restartPolicy")
    JIRAFEAU_DIR=$(getValInFile "${DOCKERS_ENV}" "jirafeauDir")
    while : ; do
	read -p "Change '${DOCKERS_ENV}'? " resetEnv
	case "${resetEnv}" in
            [yYoO]* )
		break
		;;
	    ""|[Nn]* )
		RESET_ENV=""
		break
		;;
            * )
		echo "Please answer yes no."
		;;
	esac
    done
fi

[ -n "${RESET_ENV}" ] && {
    echo "Reset '${DOCKERS_ENV}'"
    read -p "  * domain (kaz.bzh / dev.kaz.bzh / kaz.local)? [${YELLOW}${DOMAIN}${NC}] " domain
    case "${domain}" in
	"" )
	    DOMAIN="${DOMAIN}"
	    ;;
        * )
	    # XXX ne conserver que .-0-9a-z
	    DOMAIN=$(sed 's/[^a-z0-9.-]//g' <<< "${domain}")
	    ;;
    esac

    read -p "  * lists domain (kaz.bzh / kaz2.ovh / kaz.local)? [${YELLOW}${DOMAIN_SYMPA}${NC}] " domain
    case "${domain}" in
	"" )
	    DOMAIN_SYMPA="${DOMAIN_SYMPA}"
	    ;;
        * )
	    DOMAIN_SYMPA="${domain}"
	    ;;
    esac

    while : ; do
	read -p "  * protocol (https / http)? [${YELLOW}${HTTP_PROTO}${NC}] " proto
	case "${proto}" in
	    "" )
		HTTP_PROTO="${HTTP_PROTO}"
		break
		;;
            "https"|"http" )
		HTTP_PROTO="${proto}"
		break
		;;
	    * ) echo "Please answer joe, emacs, vim or no."
		;;
	esac
    done

    while : ; do
	read -p "  * main IP (ip)? [${YELLOW}${MAIN_IP}${NC}] " ip
	case "${ip}" in
	    "" )
		MAIN_IP="${MAIN_IP}"
		break
		;;
            * )
		if testValidIp "${ip}" ; then
		    MAIN_IP="${ip}"
		    break
		else
		    echo "Please answer x.x.x.x format."
		fi
		;;
	esac
    done

    while : ; do
	read -p "  * lists IP (ip)? [${YELLOW}${SYMPA_IP}${NC}] " ip
	case "${ip}" in
	    "" )
		SYMPA_IP="${SYMPA_IP}"
		break
		;;
            * )
		if testValidIp "${ip}" ; then
		    SYMPA_IP="${ip}"
		    break
		else
		    echo "Please answer x.x.x.x format."
		fi
		;;
	esac
    done

    while : ; do
	read -p "  * restart policy (always / unless-stopped / no)? [${YELLOW}${RESTART_POLICY}${NC}] " policy
	case "${policy}" in
	    "" )
		RESTART_POLICY="${RESTART_POLICY}"
		break
		;;
            "always"|"unless-stopped"|"no")
		RESTART_POLICY="${policy}"
		break
		;;
	    * ) echo "Please answer always, unless-stopped or no."
		;;
	esac
    done

    while : ; do
	read -p "  * Jirafeau dir? [${YELLOW}${JIRAFEAU_DIR}${NC}] " jirafeauDir
	case "${jirafeauDir}" in
	    "" )
		JIRAFEAU_DIR="${JIRAFEAU_DIR}"
		break
		;;
            * )
		if [[ "${jirafeauDir}" =~ ^/var/jirafeauData/[0-9A-Za-z]{1,16}/$ ]]; then
		    JIRAFEAU_DIR="${jirafeauDir}"
		    break
		else
		    echo "Please give dir name (/var/jirafeauData/[0-9A-Za-z]{1,3}/)."
		fi
		;;
	esac
    done

    [ -f "${DOCKERS_ENV}" ] || cp "${DOCKERS_TMPL_ENV}" "${DOCKERS_ENV}"

    sed -i "${DOCKERS_ENV}" \
	-e "s%^\s*domain\s*=.*$%domain=${DOMAIN}%" \
	-e "s%^\s*domain_sympa\s*=.*$%domain_sympa=${DOMAIN_SYMPA}%" \
	-e "s%^\s*httpProto\s*=.*$%httpProto=${HTTP_PROTO}%" \
	-e "s%^\s*MAIN_IP\s*=.*$%MAIN_IP=${MAIN_IP}%" \
	-e "s%^\s*SYMPA_IP\s*=.*$%SYMPA_IP=${SYMPA_IP}%" \
	-e "s%^\s*restartPolicy\s*=.*$%restartPolicy=${RESTART_POLICY}%" \
  -e "s%^\s*ldapRoot\s*=.*$%ldapRoot=dc=${DOMAIN_SYMPA/\./,dc=}%" \
	-e "s%^\s*jirafeauDir\s*=.*$%jirafeauDir=${JIRAFEAU_DIR}%"
}

if [ ! -f "${KAZ_CONF_DIR}/container-mail.list" ]; then
    cat > "${KAZ_CONF_DIR}/container-mail.list"  <<EOF
# e-mail server composer
postfix
ldap
#sympa
EOF
fi

if [ ! -f "${KAZ_CONF_DIR}/container-orga.list" ]; then
    cat > "${KAZ_CONF_DIR}/container-orga.list"  <<EOF
# orga composer
EOF
fi

if [ ! -f "${KAZ_CONF_DIR}/container-proxy.list" ]; then
    cat > "${KAZ_CONF_DIR}/container-proxy.list"  <<EOF
proxy
EOF
fi

if [ ! -f "${KAZ_CONF_DIR}/container-withMail.list" ]; then
    cat > "${KAZ_CONF_DIR}/container-withMail.list"  <<EOF
web
etherpad
roundcube
framadate
paheko
dokuwiki
gitea
mattermost
cloud
EOF
fi

if [ ! -f "${KAZ_CONF_DIR}/container-withoutMail.list" ]; then
    cat > "${KAZ_CONF_DIR}/container-withoutMail.list"  <<EOF
jirafeau
ethercalc
collabora
#vigilo
#grav
EOF
fi

if [ ! -d "${KAZ_ROOT}/secret" ]; then
    rsync -a "${KAZ_ROOT}/secret.tmpl/" "${KAZ_ROOT}/secret/"
    . "${KAZ_ROOT}/secret/SetAllPass.sh"
    "${KAZ_BIN_DIR}/secretGen.sh"
    "${KAZ_BIN_DIR}/updateDockerPassword.sh"
fi
