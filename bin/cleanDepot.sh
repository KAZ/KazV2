#!/bin/bash

#SIMU=echo

cd /var/lib/docker/volumes/jirafeau_fileData/_data

find links/ -type f -print | while read link ; do
    name=$(head -1 "${link}")
    #if [[ -z $(head -1 "${link}" | grep "7z$") ]]; then
    if [[ -z $(head -9 "${link}" | tail -1) ]]; then
	# si c'est pas un 7z on continue
	continue;
    fi
    # recherche le fichier de contenu
    filename=$(head -6 "${link}" | tail -1)
    l1=$(echo $filename | cut -c 1-8)
    l2=$(echo $filename | cut -c 9-16)
    l3=$(echo $filename | cut -c 17-24)
    l4=$(echo $filename | cut -c 25-32)

    # supprime le fichier de contenu
    ${SIMU} rm -f "files/${l1}/${l2}/${l3}/${l4}/${filename}"
    # coupe les branches mortes
    ${SIMU} rmdir -p "files/${l1}/${l2}/${l3}/${l4}" 2>/dev/null
    # supprime le lien
    ${SIMU} rm -f "${link}"

    # log
    echo "$(date +%d-%m-%Y-%H-%M-%S) Find ${link} <${name}>"
done
