#!/bin/bash
#  supervision de postfix

KAZ_ROOT=$(cd "$(dirname $0)"/..; pwd)
. $KAZ_ROOT/bin/.commonFunctions.sh
setKazVars

. $DOCKERS_ENV
. $KAZ_ROOT/secret/SetAllPass.sh

URL_AGORA=$(echo $matterHost).$(echo $domain)
MAX_QUEUE=50

OLDIFS=$IFS
IFS=" "
COUNT_MAILQ=$(docker exec -t mailServ mailq | tail -n1 | gawk '{print $5}')

docker exec ${mattermostServName} bin/mmctl --suppress-warnings auth login $httpProto://$URL_AGORA --name local-server --username $mattermost_user --password $mattermost_pass >/dev/null 2>&1

if [ "${COUNT_MAILQ}" -gt "${MAX_QUEUE}" ]; then
   echo "---------------------------------------------------------- "
   echo -e "Mail queue Postfix ALert, Messages: ${RED}${COUNT_MAILQ}${NC}"
   echo "---------------------------------------------------------- "
   docker exec mattermostServ bin/mmctl post create kaz:Sysadmin-alertes --message "Alerte mailq Postfix : La file d' attente est de ${COUNT_MAILQ} messages" >/dev/null 2>&1
fi

IFS=${OLDIFS}
