# commun fonctions for KAZ

#TODO; toutes les fonctions ci-dessous devraient être commentées

#KI  : françois
#KOI : tout un tas de trucs utiles pour la gestion de l'infra kaz (à mettre dans chaque script)
#KAN :
# maj le 27/01/2024 by FAB: recherche de tous les srv kaz dispo (via le DNS)
# maj le 15/04/2024 by FAB: correction getPahekoOrgaList

# https://wiki.bash-hackers.org/scripting/terminalcodes
BOLD='[1m'
RED='[0;31m'
GREEN='[0;32m'
YELLOW='[0;33m'
BLUE='[0;34m'
MAGENTA='[0;35m'
CYAN='[0;36m'
NC='[0m' # No Color
NL='
'

########################################
setKazVars () {
    # KAZ_ROOT must be set
    if [ -z "${KAZ_ROOT}" ]; then
	printKazError "\n\n   ***   KAZ_ROOT not defined!   ***\n"
	exit
    fi
    export KAZ_KEY_DIR="${KAZ_ROOT}/secret"
    export KAZ_BIN_DIR="${KAZ_ROOT}/bin"
    export KAZ_CONF_DIR="${KAZ_ROOT}/config"
    export KAZ_CONF_PROXY_DIR="${KAZ_CONF_DIR}/proxy"
    export KAZ_COMP_DIR="${KAZ_ROOT}/dockers"
    export KAZ_STATE_DIR="${KAZ_ROOT}/state"

    export KAZ_GIT_DIR="${KAZ_ROOT}/git"
    export KAZ_DNLD_DIR="${KAZ_ROOT}/download"
    export KAZ_DNLD_PAHEKO_DIR="${KAZ_DNLD_DIR}/paheko"

    export APPLY_TMPL=${KAZ_BIN_DIR}/applyTemplate.sh
    export DOCKERS_ENV="${KAZ_CONF_DIR}/dockers.env"

    export DOCK_LIB="/var/lib/docker"
    export DOCK_VOL="${DOCK_LIB}/volumes"
    export DOCK_VOL_PAHEKO_ORGA="${DOCK_LIB}/volumes/paheko_assoUsers/_data/"

    export NAS_VOL="/mnt/disk-nas1/docker/volumes/"
}

########################################

printKazMsg () {
    # $1 msg
    echo -e "${CYAN}${BOLD}$1${NC}"
}

printKazError () {
    # $1 msb
    echo -e "${RED}${BOLD}$1${NC}"
}

########################################
checkContinue () {
    local rep
    while : ; do
	read -p "Do you want to continue? [yes]" rep
	case "${rep}" in
            ""|[yYoO]* )
		break
		;;
	    [Nn]* )
		exit
		;;
            * )
		echo "Please answer yes or no."
		;;
	esac
    done
}

checkDockerRunning () {
    # $1 docker name
    # $2 service name
    if ! [[ "$(docker ps -f "name=$1" | grep -w "$1")" ]]; then
	printKazError "$2 not running... abort"
	return 1
    fi
    return 0
}

########################################
testValidIp () {
    # $1 ip
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

########################################
getValInFile () {
    # $1 filename
    # $2 varname
    grep "^\s*$2\s*=" $1 2>/dev/null | head -1 | sed "s%^\s*$2\s*=\(.*\)$%\1%"
}

getList () {
    # $1 filename
    (cat  "$1"|sort; echo) | sed -e "s/\(.*\)[ \t]*#.*$/\1/" -e "s/^[ \t]*\(.*\)$/\1/" -e "/^$/d"
}

getPahekoPluginList () {
    ls "${KAZ_DNLD_PAHEKO_DIR}" | grep -v "paheko-"
}

getPahekoOrgaList () {
#    ls "${DOCK_VOL_PAHEKO_ORGA}"
    find ${DOCK_VOL_PAHEKO_ORGA} -mindepth 1 -maxdepth 1 -type d -printf '%f\n' | sort
}

getAvailableComposes () {
    ls "${KAZ_COMP_DIR}" | grep -v -- "^.*-orga$"
}

getAvailableOrgas () {

#KI  : Fab
#KOI : donne la liste de toutes les orgas pour un serveur donné sinon serveur courant
#KAN : 27/01/2024

#en entrée
SITE_DST="$1"

    if [ -n "${SITE_DST}" ];then
      ssh -p 2201 root@${SITE_DST}.${domain} "ls \"${KAZ_COMP_DIR}\" | grep -- \"^.*-orga$\""
    else
      ls "${KAZ_COMP_DIR}" | grep -- "^.*-orga$"
    fi

}

getAvailableServices () {
    local service
    for service in paheko cloud collabora agora wiki wp; do
	echo "${service}"
    done
}

########################################
filterInList () {
    # $* ref list filter
    # stdin candidats
    local compose
    while read compose ; do
	if [[ " $* " =~ " ${compose} " ]]; then
	    echo ${compose}
	fi
    done | sort -u
}

filterNotInList () {
    # $* ref list filter
    # stdin candidats
    local compose
    while read compose ; do
	if [[ ! " $* " =~ " ${compose} " ]]; then
	    echo ${compose}
	fi
    done | sort -u
}

filterAvailableComposes () {
    # $* candidats
    local AVAILABLE_COMPOSES=$(getAvailableComposes;getAvailableOrgas)
    if [ $# -eq 0 ] ; then
	echo ${AVAILABLE_COMPOSES}
    fi
    local compose
    for compose in $*
    do
	compose=${compose%/}
	if [[ ! "${NL}${AVAILABLE_COMPOSES}${NL}" =~ "${NL}${compose}${NL}" ]]; then
	    local subst=""
	    for item in ${AVAILABLE_COMPOSES}; do
		[[ "${item}" =~ "${compose}" ]] && echo ${item} && subst="${subst} ${item}"
	    done
	    if [ -z "${subst}" ] ; then
		echo "${RED}${BOLD}Unknown compose: ${compose} not in "${AVAILABLE_COMPOSES}"${NC}" >&2
		#exit 1
	    else
		echo "${BLUE}${BOLD}substitute compose: ${compose} => "${subst}"${NC}" >&2
	    fi
	else
	    echo "${compose}"
	fi
    done | sort -u
}

########################################
serviceOnInOrga () {
    # $1 orga name
    # $2 service name
    # default value
    local composeFile="${KAZ_COMP_DIR}/$1-orga/docker-compose.yml"
    if [[ ! -f "${composeFile}" ]]
    then
	echo "$3"
    else
	grep -q "$2" "${composeFile}" 2>/dev/null && echo on || echo off
    fi
}

########################################
waitUrl () {
    # $1 URL to waitfor
    # $2 timeout en secondes (optional)
    starttime=$(date +%s)
    if [[ $(curl -k --connect-timeout 2 -s -D - "$1" -o /dev/null 2>/dev/null | head -n1) != *[23]0[0-9]* ]]; then
	printKazMsg "service not available ($1). Please wait..."
  echo curl -k --connect-timeout 2 -s -D - "$1" -o /dev/null \|  head -n1
  while [[ $(curl -k --connect-timeout 2 -s -D - "$1" -o /dev/null 2>/dev/null | head -n1) != *[23]0[0-9]* ]]
	do
	    sleep 5
        if [ $# -gt 1 ]; then
            actualtime=$(date +%s)
            delta=$(($actualtime-$starttime))
            [[ $2 -lt $delta ]] && return 1
        fi
	done
    fi
    return 0
}

########################################
waitContainerHealthy () {
    # $1 ContainerName
    # $2 timeout en secondes (optional)

    healthy="false"
    starttime=$(date +%s)
    running="false"
    [[ $(docker ps -f name="$1" | grep -w "$1") ]] && running="true"
    [[ $running == "true" && $(docker inspect -f {{.State.Health.Status}} "$1") == "healthy" ]] && healthy="true"
    if [[ ! $running == "true" || ! $healthy == "true" ]]; then
        printKazMsg "Docker not healthy ($1). Please wait..."
        while [[ ! $running == "true" || ! $healthy == "true" ]]
        do
            sleep 5
            if [ $# -gt 1 ]; then
                actualtime=$(date +%s)
                delta=$(($actualtime-$starttime))
                [[ $2 -lt $delta ]] && printKazMsg "Docker not healthy ($1)... abort..." && return 1
            fi
            [[ ! $running == "true" ]] && [[ $(docker ps -f name="$1" | grep -w "$1") ]] && running="true"
            [[ $running == "true" && $(docker inspect -f {{.State.Health.Status}} "$1") == "healthy" ]] && healthy="true"
        done
    fi
    return 0
}
########################################
waitContainerRunning () {
    # $1 ContainerName
    # $2 timeout en secondes (optional)

    starttime=$(date +%s)
    running="false"
    [[ $(docker ps -f name="$1" | grep -w "$1") ]] && running="true"
    if [[ ! $running == "true" ]]; then
        printKazMsg "Docker not running ($1). Please wait..."
        while [[ ! $running == "true" ]]
        do
            sleep 5
            if [ $# -gt 1 ]; then
                actualtime=$(date +%s)
                delta=$(($actualtime-$starttime))
                [[ $2 -lt $delta ]] && printKazMsg "Docker did not start ($1)... abort..." && return 1
            fi
            [[ ! $running == "true" ]] && [[ $(docker ps -f name="$1" | grep -w "$1") ]] && running="true"
	done
    fi
    return 0
}

########################################
downloadFile () {
    # $1 URL to download
    # $2 new filename (optional)
    if [ $# -lt 1 ] || [ $# -gt 2 ]; then
	printKazError "downloadFile: bad arg number"
	return
    fi
    URL=$1
    if [ -z "$2" ]; then
	FILENAME="$(basename $1)"
    else
	FILENAME="$2"
    fi

    if [ ! -f "${FILENAME}" ]; then
	printKazMsg "  - load ${URL}"
	curl -L -o "${FILENAME}" "${URL}"
    else
	TMP="${FILENAME}.tmp"
	rm -f "${TMP}"
	curl -L -o "${TMP}" "${URL}"
	if ! cmp -s "${TMP}" "${FILENAME}" 2>/dev/null; then
	    mv "${TMP}" "${FILENAME}"
	else
	    rm -f "${TMP}"
	fi
    fi
}

unzipInDir () {
    # $1 zipfile
    # $2 destDir

    if [ $# -ne 2 ]; then
	printKazError "unzipInDir: bad arg number"
	return
    fi
    if ! [[ $1 == *.zip ]]; then
	printKazError "unzipInDir: $1 is not a zip file"
	return
    fi
    if ! [[ -d $2 ]]; then
	printKazError "$2 is not destination dir"
	return
    fi

    destName="$2/$(basename "${1%.zip}")"
    if [[ -d "${destName}" ]]; then
	printKazError "${destName} already exist"
	return
    fi

    tmpDir=$2/tmp-$$
    trap 'rm -rf "${tmpDir}"' EXIT
    unzip "$1" -d "${tmpDir}"
    srcDir=$(ls -1 "${tmpDir}")
    case $(wc -l <<< $srcDir) in
	0)
	    printKazError "empty zip file : $1"
	    rmdir "${tmpDir}"
	    return
	    ;;
	1)
	    mv "${tmpDir}/${srcDir}" "${destName}"
	    rmdir "${tmpDir}"
	    ;;
	*)
	    printKazError "zip file $1 is not a tree (${srcDir})"
	    return
	    ;;
    esac
}
########################################

get_Serveurs_Kaz () {

#KI  : Fab
#KOI : donne la liste de tous les serveurs kaz sous le format srv1;srv2;srv3;.... en intérogeant le DNS
#KAN : 27/01/2024

  liste=`dig -t TXT srv.kaz.bzh +short`
  #on nettoie
  liste=$(echo "$liste" | sed 's/\;/ /g')
  liste=$(echo "$liste" | sed 's/\"//g')
  #renvoi srv1 srv2 srv3 ....
  echo ${liste}
}
########################################

