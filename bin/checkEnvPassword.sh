#!/bin/bash

KAZ_ROOT=$(cd $(dirname $0)/..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

for filename in "${KAZ_KEY_DIR}/"env-*Serv "${KAZ_KEY_DIR}/"env-*DB;  do
    if grep -q "^[^#=]*=\s*$" "${filename}" 2>/dev/null; then
	echo "${filename}"
    fi
done
