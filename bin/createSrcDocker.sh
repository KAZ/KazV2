#!/bin/bash

cd $(dirname $0)

./setOwner.sh
./createEmptyPasswd.sh

cd ../..

FILE_NAME="/tmp/$(date +'%Y%M%d')-KAZ.tar.bz2"

tar -cjf "${FILE_NAME}" --transform s/emptySecret/secret/ \
     ./kaz/emptySecret/ ./kaz/bin ./kaz/config ./kaz/dockers

ls -l "${FILE_NAME}"

