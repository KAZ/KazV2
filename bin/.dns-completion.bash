#/usr/bin/env bash

_dns_completions () {
    local cur find
    COMPREPLY=()
    cur=${COMP_WORDS[COMP_CWORD]}
    case "$cur" in
	-*)
	    COMPREPLY=( $(compgen -W "-h -n -f" -- "${cur}" ) ) ;;
	*)
	    find=""
	    for arg in ${COMP_WORDS[@]} ; do
		[[ " list add del " =~ " ${arg} " ]] && find="arg"
	    done
	    [ -z "${find}" ] && COMPREPLY=($(compgen -W "init list add del" -- "${cur}")) ;;
    esac
    return 0
}
complete -F _dns_completions dns.sh
