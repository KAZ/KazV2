#!/bin/bash

KAZ_ROOT=$(cd "$(dirname $0)"/..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

cd "${KAZ_BIN_DIR}/look"

THEMES=$(ls -F -- '.' | grep '/$' | sed 's%/%%' | tr '\n' '|' | sed 's%|$%%')

usage () {
    echo "usage $0 {${THEMES}}"
    exit
}

[ -z "$1" ]  && usage
[ -d "$1" ] || usage

cd $1

docker cp kaz-tete.png jirafeauServ:/var/jirafeau/media/kaz/kaz.png
docker cp kazdate.png framadateServ:/var/framadate/images/logo-framadate.png
docker cp kazmel.png roundcubeServ:/var/www/html/skins/elastic/images/kazmel.png
docker cp kaz-tete.png sympaServ:/usr/share/sympa/static_content/icons/logo_sympa.png
docker cp kaz-tete.png dokuwikiServ:/dokuwiki/lib/tpl/docnavwiki/images/logo.png
docker cp kaz-tete.png ldapUI:/var/www/html/images/ltb-logo.png
docker cp kaz-entier.svg webServ:/usr/share/nginx/html/images/logo.svg
docker cp kaz-signature.png webServ:/usr/share/nginx/html/m/logo.png

for cloud in nextcloudServ kaz-nextcloudServ; do
    docker cp kaz-entier.svg "${cloud}":/var/www/html/themes/kaz-entier.svg
    docker cp kaz-tete.svg "${cloud}":/var/www/html/themes/kaz-tete.svg
    docker exec -ti -u 33 "${cloud}"  /var/www/html/occ theming:config logo /var/www/html/themes/kaz-tete.svg # tete
    docker exec -ti -u 33 "${cloud}"  /var/www/html/occ theming:config logoheader /var/www/html/themes/kaz-entier.svg # entier
    # non #exec -ti -u 33 "${cloud}"  /var/www/html/occ theming:config favicon /var/www/html/themes/kaz-patte.svg # patte
done
