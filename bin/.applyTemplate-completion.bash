#/usr/bin/env bash

_applyTemplate_completions () {
    declare -a options
    OPTIONS=("-help" "-timestamp")
    COMPREPLY=()

    local CUR OPTIONS_COUNT=0
    CUR=${COMP_WORDS[COMP_CWORD]}

    # compte les options déjà utilisé et les retire de la liste des possible
    for ITEM in ${COMP_WORDS[@]:1}
    do
	if [[ " ${OPTIONS[*]} " =~ " ${ITEM} " ]] ; then
	    let "OPTIONS_COUNT++"
	    OPTIONS=(${OPTIONS[@]/${ITEM}})
	else
	    break
	fi
    done

    # si la position est dans des options "ou juste après"
    ((COMP_CWORD <= OPTIONS_COUNT+1)) && [[ "${CUR}" =~ ^- ]] && COMPREPLY=( $(compgen -W "${OPTIONS[*]}" -- "${CUR}" ) ) && return 0

    # si la position est après des options ou que l'on ne commence pas par "-" on cherche des fichiers
    ((COMP_CWORD <= OPTIONS_COUNT+2)) && COMPREPLY=($(compgen -f -- "${CUR}")) && return 0

    return 0
}
complete -F _applyTemplate_completions applyTemplate.sh
