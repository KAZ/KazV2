#! /bin/sh
# date: 30/03/2022
# koi: récupérer du swap et de la ram (uniquement sur les services qui laissent filer la mémoire)
# ki: fab

#pour vérifier les process qui prennent du swap : for file in /proc/*/status ; do awk '/Tgid|VmSwap|Name/{printf $2 " " $3}END{ print ""}' $file; done | grep kB  | sort -k 3 -n

# Ces commandes donnent le nom du process, son PID et la taille mémoire en swap. Par exemple :
# dans /proc/<PID>/status y'a un VMSwap qui est la taille de swap utilisée par le process.

#calc
docker restart ethercalcDB ethercalcServ

#sympa
#docker restart sympaServ
#/kaz/dockers/sympa/reload.sh
# --> bof, ça arrête mal le bazar (4 mails d'ano autour de sympa_msg.pl / bounced.pl / task_manager.pl / bulk.pl / archived.pl)

#sympa
# restart de sympa et relance du script de copie des librairies du filtre de messages
#docker exec -it sympaServ service sympa stop
#sleep 5
#docker exec -it sympaServ service sympa start
#sleep 5
#/kaz/dockers/sympa/reload.sh
#sleep 2
#docker exec  sympaServ chmod 777 /home/filter/filter.sh
#docker exec  sympaServ sendmail -q

#pour restart cette s.... de collabora
/kaz/bin/gestContainers.sh -office -m -r 

#postfix
docker exec -it mailServ supervisorctl restart changedetector

#proxy
docker exec -i proxyServ bash -c "/etc/init.d/nginx reload"
