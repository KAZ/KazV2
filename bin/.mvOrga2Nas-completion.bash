#/usr/bin/env bash

_mv_orga_nas_completion () {
    KAZ_ROOT=$(cd "$(dirname ${COMP_WORDS[0]})"/..; pwd)
    COMPREPLY=()
    . "${KAZ_ROOT}/bin/.commonFunctions.sh"
    setKazVars
    local cword=${COMP_CWORD} cur=${COMP_WORDS[COMP_CWORD]} card=${#COMP_WORDS[@]} i w skip=0
    for ((i=1 ; i<cword; i++)) ; do
	w="${COMP_WORDS[i]}"
        [[ "${w}" == -* ]] && ((skip++))
    done
    local arg_pos w i names=
    ((arg_pos = cword - skip))
    for ((i=1 ; i<card; i++)) ; do
	w="${COMP_WORDS[i]}"
	if [[ "${w}" == -* ]]; then
	    continue
	fi
	names="${names} ${w}"
    done
    local KAZ_LIST="${KAZ_BIN_DIR}/kazList.sh"
    case "$cur" in
	-*)
	    local proposal="-h -n"
	    COMPREPLY=( $(compgen -W "${proposal}" -- "${cur}" ) )
	    ;;
	*)
	    local available_orga=$("${KAZ_LIST}" "compose" "enable" "orga" 2>/dev/null | sed "s/-orga\b//g")
	    local proposal= item
	    for item in ${available_orga} ; do
		[[ " ${names} " =~ " ${item} " ]] || proposal="${proposal} ${item}"
	    done
	    COMPREPLY=($(compgen -W "${proposal}" -- "${cur}"))
	    ;;
    esac
    return 0
}
complete -F _mv_orga_nas_completion mvOrga2Nas.sh
