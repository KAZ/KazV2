#!/bin/bash
# Script de manipulation d'un wordpress'
# init /versions / restart ...
#

KAZ_ROOT=$(cd "$(dirname $0)"/..; pwd)
. $KAZ_ROOT/bin/.commonFunctions.sh
setKazVars
. $DOCKERS_ENV
. $KAZ_ROOT/secret/SetAllPass.sh

#GLOBAL VARS
PRG=$(basename $0)

availableOrga=($(getList "${KAZ_CONF_DIR}/container-orga.list"))
AVAILABLE_ORGAS=${availableOrga[*]//-orga/}

QUIET="1"
ONNAS=

WPCOMMUN="OUI_PAR_DEFAUT"
DockerServName=${wordpressServName}

declare -A Posts

usage() {
echo "${PRG} [OPTION] [COMMANDES] [ORGA]
Manipulation d'un wordpress

OPTIONS 
 -h|--help          Cette aide :-)
 -n|--simu          SIMULATION
 -q|--quiet         On ne parle pas (utile avec le -n pour avoir que les commandes)
 --nas              L'orga se trouve sur le NAS !

COMMANDES (on peut en mettre plusieurs dans l'ordre souhaité)
 -I|--install       L'initialisation du wordpress
 -v|--version       Donne la version du wordpress et signale les MàJ

ORGA                parmi : ${AVAILABLE_ORGAS}
                    ou vide si wordpress commun
"
}



Init(){
    PHP_CONF="${DOCK_VOL}/orga_${ORGA}-wordpress/_data/wp-config.php"
    WP_URL="${httpProto}://${ORGA}-${wordpressHost}.${domain}"

    if [ -n "${ONNAS}" ]; then 
        PHP_CONF="${NAS_VOL}/orga_${ORGA}-wordpress/_data/wp-config.php"
    fi

    if ! [[ "$(docker ps -f name=${DockerServName} | grep -w ${DockerServName})" ]]; then
        printKazError "Wordpress not running... abort"
        exit
    fi

    # XXX trouver un test du genre if ! grep -q "'installed' => true," "${PHP_CONF}" 2> /dev/null; then
    echo "\n  *** Premier lancement de WP" >& $QUIET

    ${SIMU} waitUrl "${WP_URL}"

    ${SIMU} curl -X POST \
        -d "user_name=${wp_WORDPRESS_ADMIN_USER}" \
        -d "admin_password=${wp_WORDPRESS_ADMIN_PASSWORD}" \
        -d "admin_password2=${wp_WORDPRESS_ADMIN_PASSWORD}" \
        -d "pw_weak=true" \
        -d "admin_email=admin@kaz.bzh" \
        -d "blog_public=0" \
        -d "language=fr_FR" \
        "${WP_URL}/wp-admin/install.php?step=2"

    #/* pour forcer les maj autrement qu'en ftp */
    _addVarBeforeInConf "FS_METHOD" "define('FS_METHOD', 'direct');" "\/\* That's all, stop editing! Happy publishing. \*\/" "$PHP_CONF"
}

Version(){
    VERSION=$(docker exec $DockerServName cat /var/www/html/wp-includes/version.php | grep "wp_version " | sed -e "s/.*version\s*=\s*[\"\']//" | sed "s/[\"\'].*//")
    echo "Version $DockerServName : ${GREEN}${VERSION}${NC}"
}


_addVarBeforeInConf(){
    # $1 key
    # $2 ligne à ajouter avant la ligne
    # $3 where
    # $4 fichier de conf php
    if ! grep -q "$1" "${4}" ; then
	echo -n " ${CYAN}${BOLD}$1${NC}" >& $QUIET
	${SIMU} sed -i -e "s/$3/$2\\n$3/" "${4}"
    fi
}

########## Main #################
for ARG in "$@"; do 
    case "${ARG}" in
        '-h' | '--help' )
        usage && exit ;;
        '-n' | '--simu')
        SIMU="echo" ;;
        '-q' )
        QUIET="/dev/null" ;;
        '--nas' | '-nas' )
        ONNAS="SURNAS" ;; 
        '-v' | '--version')
        COMMANDS="$(echo "${COMMANDS} VERSION" | sed "s/\s/\n/g" | sort | uniq)" ;; 
        '-I' | '--install' )
        COMMANDS="$(echo "${COMMANDS} INIT" | sed "s/\s/\n/g" | sort | uniq)" ;; # le sed sort uniq, c'est pour pas l'avoir en double
        '-*' ) # ignore
        ;; 
        *)
        ORGA="${ARG%-orga}"
        DockerServName="${ORGA}-${wordpressServName}"
        WPCOMMUN=
        ;;
    esac
done

if [ -z "${COMMANDS}" ]; then usage && exit ; fi

for COMMAND in ${COMMANDS}; do
    case "${COMMAND}" in
        'VERSION' )
         Version && exit ;;
        'INIT' )
         Init ;;
    esac
done