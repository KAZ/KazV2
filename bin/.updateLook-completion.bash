#/usr/bin/env bash

_update_look_nas_completion () {
    COMPREPLY=()
    local cur=${COMP_WORDS[COMP_CWORD]}
    local THEMES=$(cd "$(dirname ${COMP_WORDS[0]})"/look ; ls -F -- '.' | grep '/$' | sed 's%/%%' | tr '\n' ' ' | sed 's% $%%')
    
    COMPREPLY=($(compgen -W "${THEMES}" -- "${cur}"))
    return 0
}
complete -F _update_look_nas_completion updateLook.sh
