#!/bin/bash

KAZ_ROOT=$(cd "$(dirname $0)/.."; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

. "${DOCKERS_ENV}"

for dockerToTest in $KAZ_ROOT/dockers/*/alerting/; do
  for aTest in $dockerToTest/*; do
    res=$($aTest)
    if [ -n "$res" ]; then
      echo $res
      docker exec -ti mattermostServ bin/mmctl post create kaz:Sysadmin-alertes --message "${res:0:1000}"
    fi
  done
done
