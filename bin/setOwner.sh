#!/bin/bash

cd $(dirname $0)/..
KAZ=$(pwd)
owner=root

usage(){
    echo "Usage: $0 [root|user]"
    exit 1
}

case $# in
    0)
	;;
    1)
	owner=$1
	;;
    *)
	usage
	;;
esac

####################
# config
cd ${KAZ}
DIRS="config secret bin"

chown -hR ${owner}: ${DIRS}
find ${DIRS} -type f -exec chmod a-x {} \;
find ${DIRS} -type f -name \*.sh -exec chmod a+x {} \;
chmod -R a+X ${DIRS}
chmod -R go= ${DIRS}

chmod a+x bin/*.sh
chown -hR www-data: config/orgaTmpl/wiki-conf/

####################
# dockers
cd ${KAZ}/dockers

chown -h ${owner}: . * */.env */* */config/*
chmod a-x,a+r * */*
chmod a+X . * */*
chmod a+x */*.sh

chown -hR ${owner}: \
      etherpad/etherpad-lite/ \
      paheko/extensions paheko/paheko-* \
      jirafeau/Jirafeau \
      mattermost/app

chown -hR www-data: \
      vigilo \
      web/html

chmod -R a+rX web/html


