#!/bin/bash

KAZ_ROOT=$(cd "$(dirname $0)"/..; pwd)
. $KAZ_ROOT/bin/.commonFunctions.sh
setKazVars

. $DOCKERS_ENV
. $KAZ_ROOT/secret/SetAllPass.sh

URL_PAHEKO="$httpProto://${paheko_API_USER}:${paheko_API_PASSWORD}@kaz-paheko.$(echo $domain)"

PRG=$(basename $0)
RACINE=$(echo $PRG | awk '{print $1}')

TFILE_INT_PAHEKO_ACTION=$(mktemp /tmp/XXXXXXXX_INT_PAHEKO_ACTION.json)
TFILE_INT_PAHEKO_IDFILE=$(mktemp /tmp/XXXXXXXX_TFILE_INT_PAHEKO_IDFILE.json)
FILE_CREATEUSER="$KAZ_ROOT/tmp/createUser.txt"

sep=' '

#trap "rm -f ${TFILE_INT_PAHEKO_IDFILE} ${TFILE_INT_PAHEKO_ACTION} " 0 1 2 3 15

############################################ Fonctions #######################################################
TEXTE="
#   -- fichier de création des comptes KAZ
#   --
#   -- 1 ligne par compte
#   -- champs séparés par ;. les espaces en début et en fin sont enlevés
#   -- laisser vide si pas de donnée
#   -- pas d'espace dans les variables
#   --
#   -- ORGA: nom de l'organisation (max 15 car), vide sinon
#   -- ADMIN_ORGA: O/N indique si le user est admin de l'orga (va le créer comme admin du NC de l'orga et admin de l'équipe agora)
#   -- NC_ORGA: O/N indique si l'orga a demandé un NC
#   -- PAHEKO_ORGA: O/N indique si l'orga a demandé un paheko
#   -- WP_ORGA: O/N indique si l'orga a demandé un wp
#   -- AGORA_ORGA: O/N indique si l'orga a demandé un mattermost
#   -- WIKI_ORGA: O/N indique si l'orga a demandé un wiki
#   -- NC_BASE: O/N indique si le user doit être inscrit dans le NC de base
#   -- GROUPE_NC_BASE: soit null soit le groupe dans le NC de base
#   -- EQUIPE_AGORA: soit null soit equipe agora  (max 15 car)
#   -- QUOTA=(1/10/20/...) en GB
#   --
#   NOM ; PRENOM ; EMAIL_SOUHAITE ; EMAIL_SECOURS ; ORGA ; ADMIN_ORGA ; NC_ORGA ; PAHEKO_ORGA ; WP_ORGA ; AGORA_ORGA ; WIKI_ORGA ; NC_BASE ; GROUPE_NC_BASE ; EQUIPE_AGORA ; QUOTA
#
#  exemple pour un compte découverte:
#  dupont ; jean-louis; jean-louis.dupont@kaz.bzh ; gregomondo@kaz.bzh; ; N; N; N; N; N; N; O; ; ;1
#
#  exemple pour un compte asso de l'orga gogol avec le service dédié NC uniquement + une équipe dans l'agora
#  dupont ; jean-louis; jean-louis.dupont@kaz.bzh  ; gregomondo@kaz.bzh; gogol ; O; O; N; N; N; N;N;;gogol_team; 10
" 

Int_paheko_Action() {
    # $1 est une action;
    ACTION=$1
    OPTION=$2
    # on envoie la requête sur le serveur paheko avec la clause à créer
    # problème de gestion de remontée de données dans la table services_users quand le compte a plus de 2 activités
    #curl -s ${URL_PAHEKO}/api/sql -d "SELECT * from users cross join services_users on users.id = services_users.id_user where users.action_auto='${ACTION}';" >>${TFILE_INT_PAHEKO_ACTION}
    curl -s ${URL_PAHEKO}/api/sql -d "SELECT * from users where action_auto='${ACTION}';" >>${TFILE_INT_PAHEKO_ACTION}
    [ ! -z ${TFILE_INT_PAHEKO_ACTION} ] || { echo "probleme de fichier ${TFILE_INT_PAHEKO_ACTION}" ; exit 1;}
    REP_ID=$(jq -c '.results[].id ' ${TFILE_INT_PAHEKO_ACTION} 2>/dev/null)
    if [ ! -z "${REP_ID}" ]
    then
	[ "$OPTION" = "silence" ] || echo -e "${RED}Nombre de compte ${ACTION} ${NC}= ${GREEN} $(echo ${REP_ID} | wc -w) ${NC}"
	if [ -f "$FILE_CREATEUSER" ]
	then
	    mv $FILE_CREATEUSER $FILE_CREATEUSER.$(date +%d-%m-%Y-%H:%M:%S)
	fi
	echo "# -------- Fichier généré le $(date +%d-%m-%Y-%H:%M:%S) ----------">${FILE_CREATEUSER}
	echo "${TEXTE}" >>${FILE_CREATEUSER}
	for VAL_ID in ${REP_ID}
	do
	    jq -c --argjson val "${VAL_ID}" '.results[] | select (.id == $val)' ${TFILE_INT_PAHEKO_ACTION} > ${TFILE_INT_PAHEKO_IDFILE}
	    for VAL_GAR in id_category action_auto nom email email_secours quota_disque admin_orga nom_orga responsable_organisation responsable_email agora cloud wordpress garradin docuwiki id_service
	    do
		eval $VAL_GAR=$(jq .$VAL_GAR ${TFILE_INT_PAHEKO_IDFILE})
	    done
	    #comme tout va bien on continue
	    #on compte le nom de champs dans la zone nom pour gérer les noms et prénoms composés
	    # si il y a 3 champs, on associe les 2 premieres valeurs avec un - et on laisse le 3ème identique 
	    # si il y a 4 champs on associe les 1 et le 2 avec un tiret et le 3 et 4 avec un tiret
	    # on met les champs nom_ok et prenom_ok à blanc
	    nom_ok=""
	    prenom_ok=""
	    # on regarde si le nom de l' orga est renseigné ou si le nom de l' orga est null et l' activité de membre est 7 (membre rattaché)
	    # si c' est le cas alors le nom est le nom de l' orga et le prénom est forcé à la valeur Organisation
	    if [[ "$nom_orga" = null ]] || [[ "$nom_orga" != null && "$id_service" = "7" ]]
	    then
		[ "$OPTION" = "silence" ] || echo -e "${NC}Abonné ${GREEN}${nom}${NC}"
		#si lactivité est membre rattaché on affiche a quelle orga il est rattaché
		if [ "$id_service" = "7" ] && [ "$OPTION" != "silence" ] && [ "$nom_orga" != null ]
		then
		    echo -e "${NC}Orga Rattachée : ${GREEN}${nom_orga}${NC}"
		fi
		COMPTE_NOM=$(echo $nom | awk -F' ' '{for (i=1; i != NF; i++); print i;}')
		case "${COMPTE_NOM}" in
		    0|1)
			echo "Il faut corriger le champ nom (il manque un nom ou prénom) de paheko"
			echo "je quitte et supprime le fichier ${FILE_CREATEUSER}"
			rm -f $FILE_CREATEUSER
			exit 2
			;;
  		    2)
           		nom_ok=$(echo $nom | awk -F' ' '{print $1}')
           		prenom_ok=$(echo $nom | awk -F' ' '{print $2}')
           		;;
		    *)
			nom_ok=
			prenom_ok=
			for i in ${nom}; do grep -q '^[A-Z]*$' <<<"${i}" && nom_ok="${nom_ok}${sep}${i}" || prenom_ok="${prenom_ok}${sep}${i}"; done
			nom_ok="${nom_ok#${sep}}"
			prenom_ok="${prenom_ok#${sep}}"
			if [ -z "${nom_ok}" ] || [ -z "${prenom_ok}" ]; then
			    echo "Il faut corriger le champ nom (peut être un nom de famille avec une particule ?) de paheko"
			    echo "je quitte et supprime le fichier ${FILE_CREATEUSER}"
			    rm -f $FILE_CREATEUSER
			    exit
			fi
 		esac
		# comme l' orga est à null nom orga est a vide, pas d' admin orga, on met dans l' agora générale
		# pas d' équipe agora et de groupe nextcloud spécifique

		nom_orga=" "	
		admin_orga="N"
		nc_base="O"
		equipe_agora=" "
		groupe_nc_base=" "
	    else
		# L' orga est renseigné dans paheko donc les nom et prenoms sont forcé a nom_orga et Organisation
		# un équipe agora portera le nom de l' orga, le compte ne sera pas créé dans le nextcloud général
		# et le compte est admin de son orga
		nom_orga=$(echo $nom_orga | tr [:upper:] [:lower:])
		[ "$OPTION" = "silence" ] || echo -e "${NC}Orga : ${GREEN}${nom_orga}${NC}"
		nom_ok=$nom_orga
        	# test des caractères autorisés dans le nom d' orga: lettres, chiffres et/ou le tiret
		if ! [[ "${nom_ok}" =~ ^[[:alnum:]-]+$ ]]; then
            	    echo "Erreur : l' orga doit être avec des lettres et/ou des chiffres. Le séparateur doit être le tiret"
            	    rm -f $FILE_CREATEUSER�
            	    exit 2
        	fi
		prenom_ok=organisation
		equipe_agora=$nom_orga
		groupe_nc_base=" "
		nc_base="N"
		admin_orga="O"
	    fi
	    # Pour le reste on renomme les null en N ( non ) et les valeurs 1 en O ( Oui)
	    cloud=$(echo $cloud | sed -e 's/0/N/g' | sed -e 's/1/O/g')
	    paheko=$(echo $garradin | sed -e 's/0/N/g' | sed -e 's/1/O/g')
	    wordpress=$(echo $wordpress | sed -e 's/0/N/g' | sed -e 's/1/O/g')
	    agora=$(echo $agora | sed -e 's/0/N/g' | sed -e 's/1/O/g')
	    docuwiki=$(echo $docuwiki | sed -e 's/0/N/g' | sed -e 's/1/O/g')
	    # et enfin on écrit dans le fichier
	    echo "$nom_ok;$prenom_ok;$email;$email_secours;$nom_orga;$admin_orga;$cloud;$paheko;$wordpress;$agora;$docuwiki;$nc_base;$groupe_nc_base;$equipe_agora;$quota_disque">>${FILE_CREATEUSER}
	done
    else
	echo "Rien à créer"
	exit 2
    fi
}
#Int_paheko_Action "A créer" "silence"
Int_paheko_Action "A créer"
exit 0

