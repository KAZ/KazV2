#!/bin/bash

KAZ_ROOT=/kaz
. $KAZ_ROOT/bin/.commonFunctions.sh
setKazVars

FILE_LDIF=/home/sauve/ldap.ldif

. $DOCKERS_ENV
. $KAZ_ROOT/secret/SetAllPass.sh

docker exec -u 0 -i ${ldapServName} slapcat -F /opt/bitnami/openldap/etc/slapd.d -b ${ldap_root} | gzip >${FILE_LDIF}.gz
