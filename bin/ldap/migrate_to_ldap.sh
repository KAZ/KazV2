#!/bin/bash

echo "ATTENTION ! Il ne faut plus utiliser ce script, il est probable qu'il commence à mettre la grouille avec le LDAP qui vit sa vie..."
exit 1

KAZ_ROOT=/kaz
. $KAZ_ROOT/bin/.commonFunctions.sh
setKazVars

. $DOCKERS_ENV
. $KAZ_ROOT/secret/SetAllPass.sh

ACCOUNTS=/kaz/dockers/postfix/config/postfix-accounts.cf

LDAP_IP=$(docker inspect -f '{{.NetworkSettings.Networks.ldapNet.IPAddress}}' ldapServ)
URL_GARRADIN="$httpProto://${paheko_API_USER}:${paheko_API_PASSWORD}@kaz-paheko.$(echo $domain)"
# docker exec -i nextcloudDB mysql --user=${nextcloud_MYSQL_USER} --password=${nextcloud_MYSQL_PASSWORD} ${nextcloud_MYSQL_DATABASE} <<< "select * from oc_accounts;" > /tmp/oc_accounts

ERRORS="/tmp/ldap-errors.log"
> ${ERRORS}

mkdir -p /tmp/ldap/

# curl -s ${URL_GARRADIN}/api/sql -d "SELECT * from membres where emails_rattaches LIKE '%mailrattache%';"

for line in `cat ${ACCOUNTS}`
do
	mail=$(echo $line | awk -F '|' '{print $1}')
	user=$(echo $mail | awk -F '@' '{print $1}')
	domain=$(echo $mail | awk -F '@' '{print $2}')
	pass=$(echo $line | awk -F '|' '{print $2}' | sed -e "s/SHA512-//")
	IDENT_KAZ=
	if [ ${mode} = "prod" ]; then
		ficheGarradin=$(curl -s ${URL_GARRADIN}/api/sql -d "SELECT * from membres where email='${mail}';")
		mailDeSecours=$(echo ${ficheGarradin} | jq .results[0].email_secours | sed -e "s/\"//g")
		quota=$(echo ${ficheGarradin} | jq .results[0].quota_disque | sed -e "s/\"//g")
		nom=$(echo ${ficheGarradin} | jq .results[0].nom | sed -e "s/\"//g")
		nom_orga=$(echo ${ficheGarradin} | jq .results[0].nom_orga | sed -e "s/\"//g")
	else
		mailDeSecours=${mail}
		quota=1
		nom=${mail}
		nom_orga="null"
	fi

	if [ "${quota}" = "null" ]; then
		quota=1
	fi

	# nextcloudEnabled=MAYBE
	# IDENT_KAZ=$(grep -i \"${mail}\" /tmp/oc_accounts | cut -f1)
	#
	# if [ ! -z "${IDENT_KAZ}" ]; then # ident Kaz trouvé avec le mail Kaz
	# 	nextcloudEnabled=TRUE
	# else # pas trouvé avec le mail Kaz
	# 	if [ "${nom_orga}" != "null" ]; then # c'est une orga, pas de NC
	# 		IDENT_KAZ="null"
	# 		nextcloudEnabled=FALSE
	# 	else # pas trouvé avec le mail Kaz, pas une orga, on retente avec le mail de secours
	# 		IDENT_KAZ=$(grep -i \"${mailDeSecours}\" /tmp/oc_accounts | cut -f1 | head -n1)
	# 		if [ ! -z "${IDENT_KAZ}" ]; then # on a trouvé l'ident kaz chez NC avec le mail de secours
	# 			nextcloudEnabled=TRUE
	# 		else # pas trouvé avec le mail Kaz, pas une orga, pas trouvé avec le mail de secours
	# 			ficheRattache=$(curl -s ${URL_GARRADIN}/api/sql -d "SELECT * from membres where emails_rattaches LIKE '%${mail}%';" | jq ".results | length")
	# 			if [ $ficheRattache != "0" ]; then # c'est un mail rattaché, pas de NC c'est normal
	# 				IDENT_KAZ="null"
	# 				nextcloudEnabled=FALSE
	# 			else # pas trouvé, pas une orga, pas mail rattaché donc souci
	# 				echo "Pas trouvé l'identifiant Kaz nextcloud pour ${mail} / ${mailDeSecours}, on désactive nextcloud pour ce compte" >> ${ERRORS}
	# 				IDENT_KAZ="null"
	# 				nextcloudEnabled=FALSE
	# 			fi
	# 		fi
	# 	fi
	# fi


	echo -e "\n\ndn: cn=${mail},ou=users,${ldap_root}\n\
changeType: add\n\
objectClass: inetOrgPerson\n\
sn: ${nom}\n\
userPassword: ${pass}\n\
\n\n\
dn: cn=${mail},ou=users,${ldap_root}\n\
changeType: modify\n\
replace: objectClass\n\
objectClass: inetOrgPerson\n\
objectClass: kaznaute\n\
objectClass: PostfixBookMailAccount\n\
objectClass: nextcloudAccount\n\
-\n\
replace: sn\n\
sn: ${nom}\n\
-\n\
replace: mail\n\
mail: ${mail}\n\
-\n\
replace: mailEnabled\n\
mailEnabled: TRUE\n\
-\n\
replace: mailGidNumber\n\
mailGidNumber: 5000\n\
-\n\
replace: mailHomeDirectory\n\
mailHomeDirectory: /var/mail/${domain}/${user}/\n\
-\n\
replace: mailQuota\n\
mailQuota: ${quota}G\n\
-\n\
replace: mailStorageDirectory\n\
mailStorageDirectory: maildir:/var/mail/${domain}/${user}/\n\
-\n\
replace: mailUidNumber\n\
mailUidNumber: 5000\n\
-\n\
replace: nextcloudQuota\n\
nextcloudQuota: ${quota} GB\n\
-\n\
replace: mailDeSecours\n\
mailDeSecours: ${mailDeSecours}\n\
-\n\
replace: quota\n\
quota: ${quota}\n\
-\n\
replace: agoraEnabled\n\
agoraEnabled: TRUE\n\
-\n\
replace: mobilizonEnabled\n\
mobilizonEnabled: TRUE\n\n" | tee /tmp/ldap/${mail}.ldif | ldapmodify -c -H ldap://${LDAP_IP} -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" -x -w ${ldap_LDAP_ADMIN_PASSWORD}
done

#replace: nextcloudEnabled\n\
#nextcloudEnabled: ${nextcloudEnabled}\n\
#-\n\
#replace: identifiantKaz\n\
#identifiantKaz: ${IDENT_KAZ}\n\
#-\n\

OLDIFS=${IFS}
IFS=$'\n'
# ALIASES est le fichier d'entrée
ALIASES="/kaz/dockers/postfix/config/postfix-virtual.cf"
# ALIASES_WITHLDAP est le fichier de sortie des forwards qu'on ne met pas dans le ldap
ALIASES_WITHLDAP="/kaz/dockers/postfix/config/postfix-virtual-withldap.cf"
# On vide le fichier de sortie avant de commencer
> ${ALIASES_WITHLDAP}


for line in `cat ${ALIASES}`
do
	echo "Virtual line is $line"
	if [ `grep -v "," <<< $line` ]
	then
		echo "Alias simple"
		mail=$(echo $line | awk -F '[[:space:]]*' '{print $2}')
		if [ `grep $mail ${ACCOUNTS}` ]
		then
			echo "Alias vers un mail local, go ldap"
			LIST=""
			for alias in `grep ${mail} ${ALIASES} | grep -v "," | cut -d' ' -f1`
			do
				LIST=${LIST}"mailAlias: $alias\n"
			done
			echo -e "dn: cn=${mail},ou=users,${ldap_root}\n\
changeType: modify
replace: mailAlias\n\
$LIST\n\n" | ldapmodify -c -H ldap://${LDAP_IP} -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" -x -w ${ldap_LDAP_ADMIN_PASSWORD}
		else
			echo "Alias vers un mail externe, go fichier"
			echo $line >> ${ALIASES_WITHLDAP}
			echo "  + intégration LDAP"
			src=$(echo $line | awk -F '[[:space:]]*' '{print $1}')
			dst=$(echo $line | awk -F '[[:space:]]*' '{print $2}')
			echo -e "\n\ndn: cn=${src},ou=mailForwardings,${ldap_root}\n\
changeType: add\n\
objectClass: organizationalRole\n\
\n\n\
dn: cn=${src},ou=mailForwardings,${ldap_root}\n\
changeType: modify\n\
replace: objectClass\n\
objectClass: organizationalRole\n\
objectClass: PostfixBookMailForward\n\
-\n\
replace: mailAlias\n\
mailAlias: ${src}\n\
-\n\
replace: mail\n\
mail: ${dst}\n\n" | ldapmodify -c -H ldap://${LDAP_IP} -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" -x -w ${ldap_LDAP_ADMIN_PASSWORD}
		fi
	else
		echo "Forward vers plusieurs adresses, on met dans le fichier"
		echo $line >> ${ALIASES_WITHLDAP}
		echo "  + intégration LDAP"
		src=$(echo $line | awk -F '[[:space:]]*' '{print $1}')
		dst=$(echo $line | awk -F '[[:space:]]*' '{print $2}')
		OOLDIFS=${IFS}
		IFS=","
		LIST=""
		for alias in ${dst}
		do
			LIST=${LIST}"mail: $alias\n"
		done
		IFS=${OOLDIFS}
		echo -e "\n\ndn: cn=${src},ou=mailForwardings,${ldap_root}\n\
changeType: add\n\
objectClass: organizationalRole\n\
\n\n\
dn: cn=${src},ou=mailForwardings,${ldap_root}\n\
changeType: modify\n\
replace: objectClass\n\
objectClass: organizationalRole\n\
objectClass: PostfixBookMailForward\n\
-\n\
replace: mailAlias\n\
mailAlias: ${src}\n\
-\n\
replace: mail\n\
${LIST}\n\n" | ldapmodify -c -H ldap://${LDAP_IP} -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" -x -w ${ldap_LDAP_ADMIN_PASSWORD}

	fi
done
IFS=${OLDIFS}
