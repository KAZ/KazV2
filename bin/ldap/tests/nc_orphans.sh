#!/bin/bash

KAZ_ROOT=/kaz
. $KAZ_ROOT/bin/.commonFunctions.sh
setKazVars

. $DOCKERS_ENV
. $KAZ_ROOT/secret/SetAllPass.sh

LDAP_IP=$(docker inspect -f '{{.NetworkSettings.Networks.ldapNet.IPAddress}}' ldapServ)

docker exec -i nextcloudDB mysql --user=${nextcloud_MYSQL_USER} --password=${nextcloud_MYSQL_PASSWORD} ${nextcloud_MYSQL_DATABASE} <<< "select uid from oc_users;" > /tmp/nc_users.txt

OLDIFS=${IFS}
IFS=$'\n'
for line in `cat /tmp/nc_users.txt`; do
	result=$(ldapsearch -h $LDAP_IP -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" -w ${ldap_LDAP_ADMIN_PASSWORD} -b $ldap_root -x "(identifiantKaz=${line})" | grep numEntries)
	echo "${line} ${result}" | grep -v "numEntries: 1" | grep -v "^uid"
done
IFS=${OLDIFS}
