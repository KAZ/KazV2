#!/bin/bash

KAZ_ROOT=/kaz
. $KAZ_ROOT/bin/.commonFunctions.sh
setKazVars

. $DOCKERS_ENV
. $KAZ_ROOT/secret/SetAllPass.sh

LDAP_IP=$(docker inspect -f '{{.NetworkSettings.Networks.ldapNet.IPAddress}}' ldapServ)

read -p "quel éditeur ? [vi] " EDITOR
EDITOR=${EDITOR:-vi}

# if [ ${EDITOR} = 'emacs' ]; then
#   echo "ALERTE ALERTE !!! quelqu'un a voulu utiliser emacs :) :) :)"    
#   exit
# fi
 
EDITOR=${EDITOR:-vi}
export EDITOR=${EDITOR}

ldapvi -h $LDAP_IP -D "cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}" -w ${ldap_LDAP_ADMIN_PASSWORD} --discover
