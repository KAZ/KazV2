#!/bin/bash

#koi: pouvoir migrer une orga (data+dns) depuis PROD1 vers PRODx 
#kan: 07/12/2023
#ki: françois puis fab (un peu)

KAZ_ROOT=$(cd "$(dirname $0)/.."; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

. $DOCKERS_ENV
. $KAZ_ROOT/secret/SetAllPass.sh
. $KAZ_ROOT/secret/env-kaz


NAS_VOL="/mnt/disk-nas1/docker/volumes/"

tab_sites_destinations_possibles=${TAB_SITES_POSSIBLES}

#par défaut, on prend le premier site
SITE_DST="${tab_sites_destinations_possibles[0]}"

declare -a availableOrga
availableOrga=($(getList "${KAZ_CONF_DIR}/container-orga.list"))

export SIMU=""
export COPY=""

usage () {
    echo "Usage: $0 [-n] [-d host_distant] [-c] [orga]...[orga]"
    echo "    -h			: this help"
    echo "    -d host_distant	: ${SITE_DST} par défaut"
    echo "    -n			: simulation"
    echo "    -c			: only copy data but doesn't stop"
    echo "    [orgas]		: in ${availableOrga[@]}"
    echo "    example		: migVersProdX.sh -d kazoulet -c splann-orga && migVersProdX.sh -d kazoulet splann-orga"
    exit 1
}

while getopts "hncd:" option; do
	case ${option} in
	h)
		usage
		exit 0
		;;	
	n)
		SIMU="echo"
		;;
	c)
		COPY="true"
		;;
	d)
		SITE_DST=${OPTARG}
		;;		
	esac
done

# site distant autorisé ?
if [[ " ${tab_sites_destinations_possibles[*]} " == *" $SITE_DST "* ]]; then
	true
else
	echo
	echo "${RED}${BOLD}Sites distants possibles : ${tab_sites_destinations_possibles[@]}${NC}"
	echo
	usage
	exit 0
fi 

# Récupérer les orgas dans un tableau
shift $((OPTIND-1))
Orgas=("$@")

#ces orgas existent-elles sur PROD1 ?
for orga in "${Orgas[@]}"; do
	if [[ ! " ${availableOrga[@]} " =~ " ${orga} " ]]; then
		echo
		echo "Unknown orga: ${RED}${BOLD}${ARG}${orga}${NC}"
		echo
		usage
		exit 0
	fi
done

echo
echo "Site distant: ${GREEN}${BOLD}${SITE_DST}${NC}"
echo

#for orgaLong in ${Orgas}; do
#  echo ${Orgas}
#done
#exit

for orgaLong in ${Orgas}; do
    orgaCourt="${orgaLong%-orga}"
    orgaLong="${orgaCourt}-orga"
    echo "${BLUE}${BOLD}migration de ${orgaCourt}${NC}"

# if [ -d "${DOCK_VOL_PAHEKO_ORGA}/${orgaCourt}" ]; then
# 	if ! ssh -p 2201 root@${SITE_DST}.${domain} "test -d ${DOCK_VOL_PAHEKO_ORGA}/${orgaCourt}"; then
# 	    echo "${RED}${BOLD}   ... can't move paheko to ${SITE_DST}${NC}"
# 	    echo "    intall paheko in ${SITE_DST}.${domain} before!"
# 	    continue
# 	fi
# fi

		#on créé le répertoire de l'orga pour paheko sur SITE_DST s'il n'existe pas
		#pratique quand paheko n'est pas encore installé sur PROD1 mais commandé
		if [ -f "${KAZ_COMP_DIR}/${orgaLong}/usePaheko" ]; then
			${SIMU} ssh -p 2201 root@${SITE_DST}.${domain} "mkdir -p ${DOCK_VOL_PAHEKO_ORGA}/${orgaCourt} && chown www-data:www-data ${DOCK_VOL_PAHEKO_ORGA}/${orgaCourt}"	    	
			
			#ensuite, on peut refaire la liste des routes paheko pour traefik
			${SIMU} ssh -p 2201 root@${SITE_DST}.${domain} "cd ${KAZ_COMP_DIR}/paheko/ && ./docker-compose-gen.sh"	    
		fi
		
    if [ -z "${COPY}" ]; then
	cd "${KAZ_COMP_DIR}/${orgaLong}"
	docker-compose logs --tail 100| grep $(date "+ %Y-%m-%d")
	checkContinue
	${SIMU} docker-compose down
    fi

    if [ $(ls -d ${NAS_VOL}/orga_${orgaCourt}-* 2>/dev/null | wc -l) -gt 0 ]; then
	echo "${BLUE}${BOLD}   ... depuis nas${NC}"
	${SIMU} rsync -aAhHX --info=progress2 --delete ${NAS_VOL}/orga_${orgaCourt}-* -e "ssh -p 2201" root@${SITE_DST}.${domain}:${DOCK_VOL}
    else
	echo "${BLUE}${BOLD}   ... depuis disque${NC}"
        ${SIMU} rsync -aAhHX --info=progress2 --delete ${DOCK_VOL}/orga_${orgaCourt}-* -e "ssh -p 2201" root@${SITE_DST}.${domain}:${DOCK_VOL}
    fi

    if [ -z "${COPY}" ]; then
	echo "${BLUE}${BOLD}   ... config${NC}"
	if [ -d "${DOCK_VOL_PAHEKO_ORGA}/${orgaCourt}" ]; then
	    ${SIMU} rsync -aAhHX --info=progress2 --delete "${DOCK_VOL_PAHEKO_ORGA}/${orgaCourt}" -e "ssh -p 2201" root@${SITE_DST}.${domain}:"${DOCK_VOL_PAHEKO_ORGA}/"
	fi
	${SIMU} rsync -aAhHX --info=progress2 --delete ${KAZ_COMP_DIR}/${orgaLong} -e "ssh -p 2201" root@${SITE_DST}.${domain}:${KAZ_COMP_DIR}/
	${SIMU} ssh -p 2201 root@${SITE_DST}.${domain} "grep -q '^${orgaLong}\$' /kaz/config/container-orga.list || echo ${orgaLong} >> /kaz/config/container-orga.list"
	${SIMU} ssh -p 2201 root@${SITE_DST}.${domain} ${KAZ_COMP_DIR}/${orgaLong}/init-volume.sh

	cd "${KAZ_COMP_DIR}/${orgaLong}"
	${SIMU} ./orga-rm.sh
	${SIMU} ssh -p 2201 root@${SITE_DST}.${domain} "${KAZ_COMP_DIR}/${orgaLong}/orga-gen.sh" --create
	${SIMU} ssh -p 2201 root@${SITE_DST}.${domain} "${KAZ_BIN_DIR}/container.sh" start "${orgaLong}"

	${SIMU} ssh -p 2201 root@${SITE_DST}.${domain} "${KAZ_BIN_DIR}/manageCloud.sh" --officeURL "${orgaCourt}"
    fi


done
