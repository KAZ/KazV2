#!/bin/bash

PRG=$(basename $0)
SIMU=""
KAZ_ROOT=$(cd "$(dirname $0)"/..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars
. "${DOCKERS_ENV}"

cd "$(dirname $0)"

ALL_STATUS=$(docker ps -a --format "{{.ID}} {{.Names}} {{.Status}}")
SERVICES_CHOICE="$(getAvailableServices | tr "\n" "|")"
SERVICES_CHOICE="${SERVICES_CHOICE%|}"

usage () {
    echo "${RED}${BOLD}" \
	 "Usage: $0 [-h] {compose|orga|service} {available|validate|enable|disable|status} [names]...${NL}" \
	 "  -h help${NL}" \
 	 "  compose {available|validate|enable|disable} : list docker-compose name${NL}" \
	 "  compose status                              : status of docker-compose (default available)${NL}" \
	 "  service {available|validate}                : list services name${NL}" \
	 "  service {enable|disable}                    : list services in orga${NL}" \
	 "  service status                              : status of services in orga${NL}" \
	 "  service {${SERVICES_CHOICE}}${NL}" \
	 "                                              : list of orga enable a service ${NL}" \
	 "  [compose] in${NL}" \
	 "  ${CYAN}$((getAvailableComposes;getAvailableOrgas) | tr "\n" " ")${NC}${NL}"
    exit 1
}

# ========================================
compose_available () {
    echo $*
}

getComposeEnableByProxy () {
    onList=$(
	for type in ${KAZ_CONF_DIR}/container-*.list ; do
	    getList "${type}"
	done)
    local compose
    for compose in ${onList} ; do
	composeFlag="proxy_${compose//-/_}"
	[[ "${!composeFlag}" == "on" ]] && echo ${compose}
    done
}

compose_validate () {
    echo $(
	for type in ${KAZ_CONF_DIR}/container-*.list ; do
	    getList "${type}"
	done | filterInList $*)
}

compose_enable () {
    echo $(getComposeEnableByProxy | filterInList $*)
}

compose_disable () {
    echo $(getAvailableComposes | filterNotInList $(getComposeEnableByProxy) | filterInList $*)
}

compose_status () {
    for compose in $*; do
	cd "${KAZ_COMP_DIR}/${compose}"
	echo "${compose}:"
	for service in $(docker-compose ps --services 2>/dev/null); do
	    id=$(docker-compose ps -q "${service}" | cut -c 1-12)
	    if [ -z "${id}" ]; then
    		echo "  - ${RED}${BOLD}[Down]${NC} ${service}"
	    else
		status=$(grep "^${id}\b" <<< "${ALL_STATUS}" | sed "s/.*${id}\s\s*\S*\s\s*\(\S*.*\)/\1/")
		COLOR=$([[ "${status}" =~ Up ]] && echo "${GREEN}" || echo "${RED}")
		echo "  - ${COLOR}${BOLD}[${status}]${NC} ${service}"
	    fi
	done
    done
}

# ========================================
service_available () {
    echo $(getAvailableServices)
}

service_validate () {
    echo $(getAvailableServices)
}

getServiceInOrga () {
    for orga in $*; do
	[[ "${orga}" = *"-orga" ]] || continue
	local ORGA_DIR="${KAZ_COMP_DIR}/${orga}"
	ORGA_COMPOSE="${ORGA_DIR}/docker-compose.yml"
	[[ -f "${ORGA_COMPOSE}" ]] || continue
	for service in $(getAvailableServices); do
	    case "${service}" in
		paheko)
		    [ -f "${ORGA_DIR}/usePaheko" ] && echo "${service}"
		    ;;
		wiki)
		    grep -q "\s*dokuwiki:" "${ORGA_COMPOSE}" 2>/dev/null && echo "${service}"
		    ;;
		wp)
		    grep -q "\s*wordpress:" "${ORGA_COMPOSE}" 2>/dev/null && echo "${service}"
		    ;;
		*)
		    grep -q "\s*${service}:" "${ORGA_COMPOSE}" 2>/dev/null && echo "${service}"
	    esac
	done
    done
}

getOrgaWithService() {
    service="$1"
    shift
    case "${service}" in
	wiki) keyword="dokuwiki" ;;
	wp) keyword="wordpress" ;;
	*) keyword="${service}" ;;
    esac
    for orga in $*; do
	[[ "${orga}" = *"-orga" ]] || continue
	local ORGA_DIR="${KAZ_COMP_DIR}/${orga}"
	ORGA_COMPOSE="${ORGA_DIR}/docker-compose.yml"
	[[ -f "${ORGA_COMPOSE}" ]] || continue
	if [ "${service}" = "paheko" ]; then
	    [ -f "${ORGA_DIR}/usePaheko" ] && echo "${orga}"
	else
	    grep -q "\s*${keyword}:" "${ORGA_COMPOSE}" 2>/dev/null && echo "${orga}"
	fi
    done
}

service_enable () {
    echo $(getServiceInOrga $* | sort -u)
}

service_disable () {
    echo $(getAvailableServices | filterNotInList $(getServiceInOrga $*))
}

service_status () {
    # ps per enable
    echo "*** TODO ***"
}

# ========================================

KAZ_CMD=""
case "$1" in
    '-h' | '-help' )
	usage
	;;
    compose|service)
	KAZ_CMD="$1"
	shift
	;;
    *)
	usage
	;;
esac

KAZ_OPT=""
case "$1" in
    available|validate|enable|disable|status)
	KAZ_OPT="$1"
	shift
	;;
    *)
	if [ "${KAZ_CMD}" = "service" ] && [[ $1 =~ ^(${SERVICES_CHOICE})$ ]]; then
	    KAZ_OPT="$1"
	    shift
	    getOrgaWithService "${KAZ_OPT}" $(filterAvailableComposes $*)
	    exit
	fi
	usage
	;;
esac

${KAZ_CMD}_${KAZ_OPT} $(filterAvailableComposes $*)
