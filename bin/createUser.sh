#!/bin/bash

# kan: 30/03/2021
# koi: créer les users dans le système KAZ, le KazWorld, to become a kaznaute, a kaaaaaaaznaute!
# ki : fab
# test git du 02/10/2023 depuis snster

# !!! need by htpasswd
# apt-get install apache2-utils dos2unix

# rechercher tous les TODO du script pour le reste à faire
##########################################################

# fonctionnement :
# vérification de l'existence du fichier des demandes et création si absent
# on garnit les variables
# on vérifie les variables
# on créé un mdp utilisable par tous les services (identifiant : email kaz)
# pour chacun des services KAZ (NC / WP / DOKUWIKI)
# * on vérifie si le sous-domaine existe, on le créé sinon
# * on créé le user et le met admin si nécessaire
# * s'il existe déjà, rollback (y compris sur les autres services)
# pour paheko, on vérifie si le sous-domaine existe, on le créé sinon
# pour mattermost, on créé le user et l'équipe si nécé=essaire, sur l'agora de base
# tout est ok, on créé l'email
# on créé le mail d'inscription avec tout le détail des services créés (url/user)
# on inscrit le user dans la liste infos@${domain_sympa}
# on avertit contact@kaz.bzh et on post dans l'agora/creation_compte

# TODO : utilisez la req sql pour attaquer paheko et créer createUser.txt en auto et modifier le champ dans paheko ACTION de "à créer" à "aucune"

# on récupère toutes les variables et mdp
# on prend comme source des repertoire le dossier du dessus ( /kaz dans notre cas )
KAZ_ROOT=$(cd "$(dirname $0)/.."; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

cd "${KAZ_ROOT}"
. "${DOCKERS_ENV}"
. "${KAZ_KEY_DIR}/SetAllPass.sh"

# DOCK_DIR="${KAZ_COMP_DIR}" # ???

SETUP_MAIL="docker exec -ti mailServ setup"

# on détermine le script appelant, le fichier log et le fichier source, tous issus de la même racine
PRG=$(basename $0)
RACINE=${PRG%.sh}

CREATE_ORGA_CMD="${KAZ_CONF_DIR}/orgaTmpl/orga-gen.sh"

mkdir -p "${KAZ_ROOT}/tmp" "${KAZ_ROOT}/log"

# fichier source dans lequel se trouve les infos sur les utilisateurs à créer
FILE="${KAZ_ROOT}/tmp/${RACINE}.txt"
# fichier de log pour
LOG="${KAZ_ROOT}/log/${RACINE}.log"
# TODO : risque si 2 admins lance en même temps

CMD_LOGIN="${KAZ_ROOT}/tmp/${RACINE}_cmds_to_run-1-LOGIN.sh"
CMD_SYMPA="${KAZ_ROOT}/tmp/${RACINE}_cmds_to_run-2-SYMPA.sh"
CMD_ORGA="${KAZ_ROOT}/tmp/${RACINE}_cmds_to_run-3-ORGA.sh"
CMD_PROXY="${KAZ_ROOT}/tmp/${RACINE}_cmds_to_run-4-PROXY.sh"
CMD_FIRST="${KAZ_ROOT}/tmp/${RACINE}_cmds_to_run-5-FIRST.sh"
CMD_INIT="${KAZ_ROOT}/tmp/${RACINE}_cmds_to_run-6-INIT.sh"
CMD_PAHEKO="${KAZ_ROOT}/tmp/${RACINE}_cmds_to_run-7-PAHEKO.sh"
CMD_MSG="${KAZ_ROOT}/tmp/${RACINE}_cmds_to_run-8-MSG.sh"
TEMP_PAHEKO="${KAZ_ROOT}/tmp/${RACINE}.TEMP_PHAEKO.cvs"

URL_SITE="${domain}"
URL_WEBMAIL="${webmailHost}.${domain}"
URL_LISTE="${sympaHost}.${domain}"
URL_AGORA="${matterHost}.${domain}"
URL_MDP="${ldapUIHost}.${domain}"
# URL_PAHEKO="kaz-${pahekoHost}.${domain}"
URL_PAHEKO="${httpProto}://${paheko_API_USER}:${paheko_API_PASSWORD}@kaz-paheko.${domain}"

availableProxyComposes=($(getList "${KAZ_CONF_DIR}/container-proxy.list"))

NL_LIST=infos@${domain_sympa}

# indiqué dans le mail d'inscription
# (mail+cloud base+agora : max=3, min=2)
NB_SERVICES_BASE=0
# max : 5, min : 0
NB_SERVICES_DEDIES=0
# note qu'on rajoute dans le mail pour les orgas
MESSAGE_MAIL_ORGA_1=""
MESSAGE_MAIL_ORGA_2=""
MESSAGE_MAIL_ORGA_3=""

############################
# Traitement des arguments #
############################

CREATE_ORGA="true"
SIMULATION=YES

usage () {
    echo "${PRG} [-h] [-s] [-e] [-v] [-u]"
    echo " version 1.0"
    echo "    Create users in kaz world using ${FILE} as source file. All logs in ${LOG}"
    echo "    -h Display this help."
    echo "    -s Simulate. none user created but you can see the result in ${KAZ_ROOT}/tmp/${RACINE}_cmds_to_run-*.sh"
    echo "    -e Execute commands. user or orga will be created !!!"
    echo "    -u create only user (don't create orga)"
    echo "    -v or -V display site informations"
}

for ARG in $*; do
    case "${ARG}" in
	'-h' | '-help' )
	    usage
	    shift
	    exit;;
	-s)
	    shift;;
	-e)
	    SIMULATION=NO
	    shift;;
	-u)
	    # only user => no orga
	    CREATE_ORGA=
	    shift;;
	'-v' | '-V' )
	    echo "${PRG}, root : ${KAZ_ROOT}, on domain : ${URL_SITE}"
	    exit
	    ;;
	*)
	    usage
	    echo "${PRG}: ${RED}unknown parameter${NC}"
	    shift
	    exit;;
    esac
done

##################################
# Inventaire des comptes à créer #
##################################

# la recherche des comptes à créé avec la commande :
# bin/interoPaheko.sh

# création d'un fichier vide
# TODO : même code ici et dans interoPaheko.sh => risque de divergence
if [ ! -s "${FILE}" ];then
    echo "${RED}"
    echo "ERREUR : le fichier ${FILE} n'existait pas"
    echo "Il vient d'être créé. Vous pouvez le compléter."
    echo "${NC}"
    cat > "${FILE}"  <<EOF
# -- fichier de création des comptes KAZ
# --
# -- 1 ligne par compte
# -- champs séparés par ";". les espaces en début et en fin sont enlevés
# -- laisser vide si pas de donnée
# -- pas d'espace dans les variables
# --
# -- ORGA           : nom de l'organisation (max 23 car), vide sinon
# -- ADMIN_ORGA     : O/N indique si le user est admin de l'orga (va le créer comme admin du NC de l'orga et admin de l'équipe agora)
# -- NC_ORGA        : O/N indique si l'orga a demandé un NC
# -- PAHEKO_ORGA  : O/N indique si l'orga a demandé un paheko
# -- WP_ORGA        : O/N indique si l'orga a demandé un wp
# -- AGORA_ORGA     : O/N indique si l'orga a demandé un mattermost
# -- WIKI_ORGA      : O/N indique si l'orga a demandé un wiki
# -- NC_BASE        : O/N indique si le user doit être inscrit dans le NC de base
# -- GROUPE_NC_BASE : soit null soit le groupe dans le NC de base
# -- EQUIPE_AGORA   : soit null soit equipe agora  (max 23 car)
# -- QUOTA          = (1/10/20/...) en GB
# --
# NOM ; PRENOM ; EMAIL_SOUHAITE ; EMAIL_SECOURS ; ORGA ; ADMIN_ORGA ; NC_ORGA ; PAHEKO_ORGA ; WP_ORGA ; AGORA_ORGA ; WIKI_ORGA ; NC_BASE ; GROUPE_NC_BASE ; EQUIPE_AGORA ; QUOTA

# exemple pour un compte découverte :
# loufoque ;   le_mec; loufoque.le-mec@kaz.bzh ; gregomondo@kaz.bzh; ; N; N; N; N; N; N;N;;; 1

# exemple pour un compte asso de l'orga gogol avec le service dédié NC uniquement + une équipe dans l'agora
# loufoque ;   le_mec; loufoque.le-mec@kaz.bzh ; gregomondo@kaz.bzh; gogol ; O; O; N; N; N; N;N;;gogol_team; 10

EOF
    exit
fi
ALL_LINES=$(sed -e "/^[ \t]*#.*$/d" -e "/^[ \t]*$/d" "${FILE}")
if [ -z "${ALL_LINES}" ];then
    usage
    echo "${PRG}: ${RED}nothing to do in ${FILE}${NC}"
    exit
fi

###################
# Initialisations #
###################

# emails et les alias KAZ déjà créés
TFILE_EMAIL="$(mktemp /tmp/${RACINE}.XXXXXXXXX.TFILE_EMAIL)"
# comptes mattermost
TFILE_MM="$(mktemp /tmp/${RACINE}.XXXXXXXXX.TFILE_MM)"
# l'ident NextCloud
TEMP_USER_NC="$(mktemp /tmp/${RACINE}.XXXXXXXXX.TEMP_USER_NC)"
# le groupe NextCloud
TEMP_GROUP_NC="$(mktemp /tmp/${RACINE}.XXXXXXXXX.TEMP_GROUP_NC)"
# l'ident WP
TEMP_USER_WP="$(mktemp /tmp/${RACINE}.XXXXXXXXX.TEMP_USER_WP)"

trap "rm -f '${TFILE_EMAIL}' '${TFILE_MM}' '${TEMP_USER_NC}' '${TEMP_GROUP_NC}' '${TEMP_USER_WP}'" 0 1 2 3 15

for i in "${CMD_LOGIN}" "${CMD_SYMPA}" "${CMD_ORGA}" "${CMD_PROXY}" "${CMD_FIRST}" "${CMD_INIT}" "${CMD_PAHEKO}" "${CMD_MSG}"; do
    echo "#!/bin/bash" > "${i}" && chmod +x "${i}"
done

echo "numero,nom,quota_disque,action_auto" > "${TEMP_PAHEKO}"
echo "curl \"https://${paheko_API_USER}:${paheko_API_PASSWORD}@kaz-paheko.kaz.bzh/api/user/import\" -T \"${TEMP_PAHEKO}\"" >> "${CMD_PAHEKO}"

#echo "récupération des login postfix... "
## on stocke les emails et les alias KAZ déjà créés
#(
#    ${SETUP_MAIL} email list
#    ${SETUP_MAIL} alias list
#) | cut -d ' ' -f 2 | grep @ | sort > "${TFILE_EMAIL}"
# did on supprime le ^M en fin de fichier pour pas faire planter les grep
#dos2unix "${TFILE_EMAIL}"

echo "on récupère tous les emails (secours/alias/kaz) sur le ldap"
FILE_LDIF=/home/sauve/ldap.ldif
/kaz/bin/ldap/ldap_sauve.sh
gunzip ${FILE_LDIF}.gz -f
grep -aEiorh '([[:alnum:]]+([._-][[:alnum:]]+)*@[[:alnum:]]+([._-][[:alnum:]]+)*\.[[:alpha:]]{2,6})' ${FILE_LDIF} | sort -u > ${TFILE_EMAIL}

echo "récupération des login mattermost... "
docker exec -ti mattermostServ bin/mmctl user list --all | grep ":.*(" | cut -d ':' -f 2 | cut -d ' ' -f 2 | sort > "${TFILE_MM}"

dos2unix "${TFILE_MM}"
echo "done"

# se connecter à l'agora pour ensuite pouvoir passer toutes les commandes mmctl
echo "docker exec -ti mattermostServ bin/mmctl auth login ${httpProto}://${URL_AGORA} --name local-server --username ${mattermost_user} --password ${mattermost_pass}" | tee -a "${CMD_INIT}"

# vérif des emails
regex="^(([A-Za-z0-9]+((\.|\-|\_|\+)?[A-Za-z0-9]?)*[A-Za-z0-9]+)|[A-Za-z0-9]+)@(([A-Za-z0-9]+)+((\.|\-|\_)?([A-Za-z0-9]+)+)*)+\.([A-Za-z]{2,})+$"
function validator {
    if ! [[ "$1" =~ ${regex} ]]; then
	# printf "* %-48s \e[1;31m[fail]\e[m\n" "${1}"
	(
	    echo
	    echo "ERREUR : le paramètre ${RED}${BOLD}$1 n'est pas un email valide${NC} - on stoppe tout - aucun utilisateur de créé"
	    echo
	) | tee -a "${LOG}"
	exit 1
    fi
}

######################################
# Boucle lecture des comptes à créer #
######################################
echo -e "$(date '+%Y-%m-%d %H:%M:%S') : ${PRG} - sauvegarde des utilisateurs à créer" | tee "${LOG}"
cat "${FILE}" >> "${LOG}"

LDAP_IP=$(docker inspect -f '{{.NetworkSettings.Networks.ldapNet.IPAddress}}' ldapServ)
ALL_ORGA=

while read ligne; do

    # | xargs permet de faire un trim
    NOM=$(awk -F ";" '{print $1}' <<< "${ligne}" | xargs)
    PRENOM=$(awk -F ";" '{print $2}' <<< "${ligne}" | xargs)

    declare -A tab_email
    tab_email[EMAIL_SOUHAITE]=$(awk -F ";" '{print $3}' <<< "${ligne}" | xargs)
    tab_email[EMAIL_SECOURS]=$(awk -F ";" '{print $4}' <<< "${ligne}" | xargs)

    ORGA=$(awk -F ";" '{print $5}' <<< "${ligne}" | xargs)
    ORGA=${ORGA,,}

    declare -A service
    service[ADMIN_ORGA]=$(awk -F ";" '{print $6}' <<< "${ligne}" | xargs)
    service[NC_ORGA]=$(awk -F ";" '{print $7}' <<< "${ligne}" | xargs)
    service[PAHEKO_ORGA]=$(awk -F ";" '{print $8}' <<< "${ligne}" | xargs)
    service[WP_ORGA]=$(awk -F ";" '{print $9}' <<< "${ligne}" | xargs)
    service[AGORA_ORGA]=$(awk -F ";" '{print $10}' <<< "${ligne}" | xargs)
    service[WIKI_ORGA]=$(awk -F ";" '{print $11}' <<< "${ligne}" | xargs)
    service[NC_BASE]=$(awk -F ";" '{print $12}' <<< "${ligne}" | xargs)

    GROUPE_NC_BASE=$(awk -F ";" '{print $13}' <<< "${ligne}" | xargs)
    GROUPE_NC_BASE="${GROUPE_NC_BASE,,}"
    EQUIPE_AGORA=$(awk -F ";" '{print $14}' <<< "${ligne}" | xargs)
    EQUIPE_AGORA=${EQUIPE_AGORA,,}
    QUOTA=$(awk -F ";" '{print $15}' <<< "${ligne}" | xargs)
    PASSWORD=$(awk -F ";" '{print $16}' <<< "${ligne}" | xargs)

    IDENT_KAZ=$(unaccent utf8 "${PRENOM,,}.${NOM,,}")
    #email en minuscule
    EMAIL_SOUHAITE=${tab_email[EMAIL_SOUHAITE],,}
    EMAIL_SECOURS=${tab_email[EMAIL_SECOURS]}

    echo -e "${NL}***************************** traitement de ${ligne}" | tee -a "${LOG}"

    ###########################
    # Vérification des champs #
    ###########################

    for k in "${!tab_email[@]}"; do
	validator "${tab_email[${k}]}"
    done

    # vérif des champs O/N
    for k in "${!service[@]}"; do
	if [ "${service[${k}]}" != "O" -a "${service[${k}]}" != "N" ]; then
	    (
		echo "${RED}"
		echo "${k} : ${service[${k}]}"
		echo "ERREUR : le paramètre ${k} accepte O ou N - on stoppe tout - aucun utilisateur de créé"
		echo "${NC}"
	    ) | tee -a "${LOG}"
	    exit 1
	fi
    done

    # taille ORGA et EQUIPE_AGORA
    TAILLE_MAX="23"
    if [ "${#ORGA}" -gt "${TAILLE_MAX}" ]; then
	(
	    echo "${RED}"
	    echo "ERREUR : le paramètre ORGA est trop grand : ${ORGA} , taille max : ${TAILLE_MAX} - on stoppe tout - aucun utilisateur de créé"
	    echo "${NC}"
	) | tee -a "${LOG}"
	exit 1
    fi
    if [ "${#ORGA}" -gt "0" ]; then
	if [[ "${ORGA}" =~ ^[[:alnum:]-]+$ ]]; then
	    echo "ok"
	else
	    (
		echo "${RED}"
		echo "ERREUR : le paramètre ORGA ne contient pas les caractères autorisés : ${ORGA} - on stoppe tout - aucun utilisateur de créé"
		echo "${NC}"
	    ) | tee -a "${LOG}"
	    exit 1
	fi
    fi

    if [ "${#EQUIPE_AGORA}" -gt "${TAILLE_MAX}" ]; then
	(
	    echo "${RED}"
	    echo "ERREUR : le paramètre EQUIPE_AGORA est trop grand : ${EQUIPE_AGORA} , taille max : ${TAILLE_MAX} - on stoppe tout - aucun utilisateur de créé"
	    echo "${NC}"
	) | tee -a "${LOG}"
	exit 1
    fi

    # vérif quota est entier
    if ! [[ "${QUOTA}" =~ ^[[:digit:]]+$ ]]; then
	(
	    echo
	    echo "ERREUR : ${RED}${BOLD}QUOTA n'est pas numérique : ${QUOTA}${NC} - on stoppe tout - aucun utilisateur de créé"
	) | tee -a "${LOG}"
    fi

    ####################################################
    # cree un mdp acceptable par postfix/nc/mattermost #
    ####################################################
    if [ -z ${PASSWORD} ]; then
      PASSWORD=_`apg -n 1 -m 10 -M NCL -d`_
    fi
    SEND_MSG_CREATE=
    if [ -n "${ORGA}" -a -z "${CREATE_ORGA}" ]; then
	# skeep orga
	continue
    fi

    ####################################################################
    # TODO: Test de l'identKAZ du ldap, il faut l'unicité. si KO, STOP #
    ####################################################################

    ###################################
    # Création du compe de messagerie #
    ###################################
    # le mail existe t-il déjà ?
    if grep -q "^${EMAIL_SOUHAITE}$" "${TFILE_EMAIL}"; then
	echo "${EMAIL_SOUHAITE} existe déjà" | tee -a "${LOG}"
    else
	SEND_MSG_CREATE=true
	echo "${EMAIL_SOUHAITE} n'existe pas" | tee -a "${LOG}"
	echo "${SETUP_MAIL} email add ${EMAIL_SOUHAITE} ${PASSWORD}" | tee -a "${CMD_LOGIN}"
	echo "${SETUP_MAIL} quota set ${EMAIL_SOUHAITE} ${QUOTA}G" | tee -a "${CMD_LOGIN}"
	# LDAP, à tester
	user=$(echo ${EMAIL_SOUHAITE} | awk -F '@' '{print $1}')
	domain=$(echo ${EMAIL_SOUHAITE} | awk -F '@' '{print $2}')
	pass=$(mkpasswd -m sha512crypt ${PASSWORD})
	echo "echo -e '\n\ndn: cn=${EMAIL_SOUHAITE},ou=users,${ldap_root}\n\
changeType: add\n\
objectclass: inetOrgPerson\n\
objectClass: PostfixBookMailAccount\n\
objectClass: nextcloudAccount\n\
objectClass: kaznaute\n\
sn: ${PRENOM} ${NOM}\n\
mail: ${EMAIL_SOUHAITE}\n\
mailEnabled: TRUE\n\
mailGidNumber: 5000\n\
mailHomeDirectory: /var/mail/${domain}/${user}/\n\
mailQuota: ${QUOTA}G\n\
mailStorageDirectory: maildir:/var/mail/${domain}/${user}/\n\
mailUidNumber: 5000\n\
mailDeSecours: ${EMAIL_SECOURS}\n\
identifiantKaz: ${IDENT_KAZ}\n\
quota: ${QUOTA}\n\
nextcloudEnabled: TRUE\n\
nextcloudQuota: ${QUOTA} GB\n\
mobilizonEnabled: TRUE\n\
agoraEnabled: TRUE\n\
userPassword: {CRYPT}${pass}\n\n' | ldapmodify -c -H ldap://${LDAP_IP} -D \"cn=${ldap_LDAP_ADMIN_USERNAME},${ldap_root}\" -x -w ${ldap_LDAP_ADMIN_PASSWORD}" | tee -a "${CMD_LOGIN}"
    fi
#userPassword: {CRYPT}\$6\$${pass}\n\n\" | ldapmodify -c -H ldap://${LDAP_IP} -D \"cn=${ldap_LDAP_CONFIG_ADMIN_USERNAME},${ldap_root}\" -x -w ${ldap_LDAP_CONFIG_ADMIN_PASSWORD}" | tee -a "${CMD_LOGIN}"

    CREATE_ORGA_SERVICES=""

    #############
    # NEXTCLOUD #
    #############
    # on recalcul l'url de NC
    if [ "${ORGA}" != "" -a "${service[NC_ORGA]}" == "O" ]; then
	URL_NC="${ORGA}-${cloudHost}.${domain}"

	# si le cloud de l'orga n'est pas up alors on le créé
	nb=$(docker ps | grep "${ORGA}-${cloudHost}" | wc -l)
	if [ "${nb}" == "0" ];then
	    echo " * +cloud +collabora ${ORGA}"
	    CREATE_ORGA_SERVICES="${CREATE_ORGA_SERVICES} +cloud +collabora"
	    # installe les plugins initiaux dans "/kaz/bin/gestClouds.sh"
	fi

	NB_SERVICES_DEDIES=$((NB_SERVICES_DEDIES+1))

    else
	URL_NC="${cloudHost}.${domain}"
	NB_SERVICES_BASE=$((NB_SERVICES_BASE+1))
    fi

    MESSAGE_MAIL_ORGA_1="${MESSAGE_MAIL_ORGA_1}${NL}* un bureau virtuel pour stocker des fichiers/calendriers/contacts et partager avec vos connaissances : ${httpProto}://${URL_NC}"

    # le user existe t-il déjà sur NC ?
    curl -o "${TEMP_USER_NC}" -X GET -H 'OCS-APIRequest:true' "${httpProto}://admin:${nextcloud_NEXTCLOUD_ADMIN_PASSWORD}@${URL_NC}/ocs/v1.php/cloud/users?search=${IDENT_KAZ}"
    if grep -q "<element>${IDENT_KAZ}</element>" "${TEMP_USER_NC}"; then
	echo "${IDENT_KAZ} existe déjà sur ${URL_NC}" | tee -a "${LOG}"
    else

      # on créé l'utilisateur sur NC sauf si c'est le NC général, on ne créé jamais l'utilisateur7
	  if [ ${URL_NC} != "${cloudHost}.${domain}" ]; then
	
	    echo "curl -X POST -H 'OCS-APIRequest:true' ${httpProto}://admin:${nextcloud_NEXTCLOUD_ADMIN_PASSWORD}@${URL_NC}/ocs/v1.php/cloud/users \
-d userid='${IDENT_KAZ}' \
-d displayName='${PRENOM} ${NOM}' \
-d password='${PASSWORD}' \
-d email='${EMAIL_SOUHAITE}' \
-d quota='${QUOTA}GB' \
-d language='fr' \
" | tee -a "${CMD_INIT}"

      fi

	# s'il est admin de son orga, on le met admin
	if [ "${service[ADMIN_ORGA]}" == "O" -a "${ORGA}" != "" -a "${service[NC_ORGA]}" == "O" ]; then
	    echo "curl -X POST -H 'OCS-APIRequest:true' ${httpProto}://${nextcloud_NEXTCLOUD_ADMIN_USER}:${nextcloud_NEXTCLOUD_ADMIN_PASSWORD}@${URL_NC}/ocs/v1.php/cloud/users/${IDENT_KAZ}/groups -d groupid='admin'" | tee -a "${CMD_INIT}"
	fi

	# faut-il mettre le user NC dans un groupe particulier sur le NC de base ?
	if [ "${GROUPE_NC_BASE}" != "" -a  "${service[NC_BASE]}" == "O" ]; then
	    # le groupe existe t-il déjà ?
	    curl -o "${TEMP_GROUP_NC}" -X GET -H 'OCS-APIRequest:true' "${httpProto}://admin:${nextcloud_NEXTCLOUD_ADMIN_PASSWORD}@${URL_NC}/ocs/v1.php/cloud/groups?search=${GROUPE_NC_BASE}"
	    nb=$(grep "<element>${GROUPE_NC_BASE}</element>" "${TEMP_GROUP_NC}" | wc -l)
	    if [ "${nb}" == "0" ];then
		echo "curl -X POST -H 'OCS-APIRequest:true' ${httpProto}://admin:${nextcloud_NEXTCLOUD_ADMIN_PASSWORD}@${URL_NC}/ocs/v1.php/cloud/groups -d groupid=${GROUPE_NC_BASE}" | tee -a "${CMD_INIT}"
	    fi
	    # puis attacher le user au groupe
	    echo "curl -X POST -H 'OCS-APIRequest:true' ${httpProto}://admin:${nextcloud_NEXTCLOUD_ADMIN_PASSWORD}@${URL_NC}/ocs/v1.php/cloud/users/${IDENT_KAZ}/groups -d groupid=${GROUPE_NC_BASE}" | tee -a "${CMD_INIT}"
	fi
    fi

    #############
    # WORDPRESS #
    #############

    # TODO : pour l'utilisation de l'api : https://www.hostinger.com/tutorials/wordpress-rest-api

    if [ "${ORGA}" != "" -a "${service[WP_ORGA]}" == "O" ]; then

	URL_WP_ORGA="${ORGA}-${wordpressHost}.${domain}"

	# si le wp de l'orga n'est pas up alors on le créé
	nb=$(docker ps | grep "${ORGA}-${wordpressHost}" | wc -l)
	if [ "${nb}" == "0" ];then
	    echo " * +wp ${ORGA}"
	    CREATE_ORGA_SERVICES="${CREATE_ORGA_SERVICES} +wp"
	fi

	NB_SERVICES_DEDIES=$((NB_SERVICES_DEDIES+1))
	MESSAGE_MAIL_ORGA_1="${MESSAGE_MAIL_ORGA_1}${NL}* un site web de type wordpress : ${httpProto}://${URL_WP_ORGA}/wp-admin/"

	# TODO : vérif existance user
	# 			# le user existe t-il déjà sur le wp ?
	# 			curl -o "${TEMP_USER_WP}" -X GET "${httpProto}://${wp_WORDPRESS_ADMIN_USER}:${wp_WORDPRESS_ADMIN_PASSWORD}@${URL_WP_ORGA}/ocs/v1.php/cloud/users?search=${IDENT_KAZ}"
	# 			nb_user_wp_orga=$(grep "<element>${IDENT_KAZ}</element>" "${TEMP_USER_WP}" | wc -l)
	# 			if [ "${nb_user_wp_orga}" != "0" ];then
	# 				(
	# 						echo "${RED}"
	# 						echo "ERREUR : ${IDENT_KAZ} existe déjà sur ${URL_WP_ORGA} - on stoppe tout - aucun utilisateur de créé"
	# 						echo "${NC}"
	# 				) | tee -a "${LOG}"
	#
	# 				# ROLLBACK - on vire le user de NC
	# 				if [ "${nb_user_nc_orga}" != "0" ];then
	# 					(
	# 						echo "${RED}"
	# 						echo "ERREUR : ${IDENT_KAZ} existe déjà sur ${URL_NC} - on stoppe tout - aucun utilisateur de créé"
	# 						echo "${NC}"
	# 					) | tee -a "${LOG}"
	#
	# 					# on supprime l'utilisateur sur NC.
	# 					echo "curl -X DELETE -H 'OCS-APIRequest:true' ${httpProto}://admin:${nextcloud_NEXTCLOUD_ADMIN_PASSWORD}@${URL_NC}/ocs/v1.php/cloud/users \
	    # 					-d userid='${IDENT_KAZ}' \
	    # 					" | tee -a "${CMD_INIT}"
	# 				fi
	#
	# 				exit 1
	# 			fi

	# TODO : créer le user et le mettre admin si nécessaire
	# 			if [ "${service[ADMIN_ORGA]}" == "O" ]; then
	# 				:
	# 			else
	# 				:
	# 			fi
    fi

    ############
    # PAHEKO #
    ############


    if [ "${ORGA}" != "" -a "${service[PAHEKO_ORGA]}" == "O" ]; then

	URL_PAHEKO_ORGA="${ORGA}-${pahekoHost}.${domain}"

	# il n'y a pas de docker spécifique paheko (je cree toujours paheko)
	echo " * +paheko ${ORGA}"
	CREATE_ORGA_SERVICES="${CREATE_ORGA_SERVICES} +paheko"

	NB_SERVICES_DEDIES=$((NB_SERVICES_DEDIES+1))
	MESSAGE_MAIL_ORGA_1="${MESSAGE_MAIL_ORGA_1}${NL}* un service de gestion adhérents/clients : ${httpProto}://${URL_PAHEKO_ORGA}"

	if [ "${service[ADMIN_ORGA]}" == "O" ]; then
	    MESSAGE_MAIL_ORGA_1="${MESSAGE_MAIL_ORGA_1} (l'installation est à terminer en vous rendant sur le site)"
	fi
    fi


    ############
    # DOKUWIKI #
    ############

    if [ "${ORGA}" != "" -a "${service[WIKI_ORGA]}" == "O" ]; then

	URL_WIKI_ORGA="${ORGA}-${dokuwikiHost}.${domain}"

	# si le wiki de l'orga n'est pas up alors on le créé
	nb=$(docker ps | grep "${ORGA}-${dokuwikiHost}" | wc -l)
	if [ "${nb}" == "0" ];then
	    echo " * +wiki ${ORGA}"
	    CREATE_ORGA_SERVICES="${CREATE_ORGA_SERVICES} +wiki"
	fi

	NB_SERVICES_DEDIES=$((NB_SERVICES_DEDIES+1))
	MESSAGE_MAIL_ORGA_1="${MESSAGE_MAIL_ORGA_1}${NL}* un wiki dédié pour votre documentation : ${httpProto}://${URL_WIKI_ORGA}"


	# TODO : ??? à voir https://www.dokuwiki.org/devel:xmlrpc:clients

	if grep -q "^${IDENT_KAZ}:" "${DOCK_VOL}/orga_${ORGA}-wikiConf/_data/users.auth.php" 2>/dev/null; then
	    echo "${IDENT_KAZ} existe déjà sur ${URL_WIKI_ORGA}" | tee -a "${LOG}"
	else
	    echo "echo \"${IDENT_KAZ}:$(htpasswd -bnBC 10 "" ${PASSWORD}):${PRENOM} ${NOM}:${EMAIL_SOUHAITE}:admin,user\" >> \"${DOCK_VOL}/orga_${ORGA}-wikiConf/_data/users.auth.php\"" | tee -a "${CMD_INIT}"
	fi
    fi

    ##############
    # MATTERMOST #
    ##############

    # on ne gère pas la création du docker dédié mattermost
    if [ "${ORGA}" != "" -a "${service[AGORA_ORGA]}" == "O" ]; then
	echo "# ******************************************************************************" | tee -a "${CMD_INIT}"
	echo "# **************************** ATTENTION ***************************************" | tee -a "${CMD_INIT}"
	echo "# ******************************************************************************" | tee -a "${CMD_INIT}"
	echo "# Mattermost dédié : on ne fait rien." | tee -a "${CMD_INIT}"
	echo "# ******************************************************************************" | tee -a "${CMD_INIT}"
    fi

    if grep -q "^${IDENT_KAZ}$" "${TFILE_MM}" 2>/dev/null; then
	echo "${IDENT_KAZ} existe déjà sur mattermost" | tee -a "${LOG}"
    else
	# on créé le compte mattermost
	echo "docker exec -ti mattermostServ bin/mmctl user create --email ${EMAIL_SOUHAITE} --username ${IDENT_KAZ} --password ${PASSWORD}" | tee -a "${CMD_LOGIN}"
	# et enfin on ajoute toujours le user à l'équipe KAZ et aux 2 channels publiques
	echo "docker exec -ti mattermostServ bin/mmctl team users add kaz ${EMAIL_SOUHAITE}" | tee -a "${CMD_LOGIN}"
	echo "docker exec -ti mattermostServ bin/mmctl channel users add kaz:une-question--un-soucis ${EMAIL_SOUHAITE}" | tee -a "${CMD_LOGIN}"
	echo "docker exec -ti mattermostServ bin/mmctl channel users add kaz:cafe-du-commerce--ouvert-2424h ${EMAIL_SOUHAITE}" | tee -a "${CMD_LOGIN}"
	NB_SERVICES_BASE=$((NB_SERVICES_BASE+1))
    fi

    if [ "${EQUIPE_AGORA}" != "" -a "${EQUIPE_AGORA}" != "kaz" ]; then
	# l'équipe existe t-elle déjà ?
	nb=$(docker exec mattermostServ bin/mmctl team list | grep -w "${EQUIPE_AGORA}" | wc -l)
	if [ "${nb}" == "0" ];then # non, on la créé en mettant le user en admin de l'équipe
	    echo "docker exec -ti mattermostServ bin/mmctl team create --name ${EQUIPE_AGORA} --display_name ${EQUIPE_AGORA} --email ${EMAIL_SOUHAITE}" --private | tee -a "${CMD_INIT}"
	fi
	# puis ajouter le user à l'équipe
	echo "docker exec -ti mattermostServ bin/mmctl team users add ${EQUIPE_AGORA} ${EMAIL_SOUHAITE}" | tee -a "${CMD_INIT}"
    fi

    if [ -n "${CREATE_ORGA_SERVICES}" ]; then
	SEND_MSG_CREATE=true
	echo "${CREATE_ORGA_CMD}" --create ${CREATE_ORGA_SERVICES} "${ORGA}" | tee -a "${CMD_ORGA}"
	echo "${CREATE_ORGA_CMD}" --init ${CREATE_ORGA_SERVICES} "${ORGA}" | tee -a "${CMD_FIRST}"
	ALL_ORGA="${ALL_ORGA} ${ORGA}"
    fi

    ##########################
    # Inscription newsletter #
    ##########################

    # TODO : utiliser liste sur dev également

    # on inscrit le user sur sympa, à la liste infos@${domain_sympa}
    # docker exec -ti sympaServ /usr/lib/sympa/bin/sympa_soap_client.pl --soap_url=https://listes.kaz.sns/sympasoap --trusted_application=SOAP_USER --trusted_application_password=SOAP_PASSWORD --proxy_vars="USER_EMAIL=contact1@kaz.sns" --service=which
    if [[ "${mode}" = "dev" ]]; then
	echo "# DEV, on teste l'inscription à sympa"| tee -a "${CMD_SYMPA}"
	LISTMASTER=$(echo ${sympa_LISTMASTERS} | cut -d',' -f1)
	echo "docker exec -ti sympaServ /usr/lib/sympa/bin/sympa_soap_client.pl --soap_url=${httpProto}://${URL_LISTE}/sympasoap --trusted_application=${sympa_SOAP_USER} --trusted_application_password=${sympa_SOAP_PASSWORD} --proxy_vars=\"USER_EMAIL=${LISTMASTER}\" --service=add --service_parameters=\"${NL_LIST},${EMAIL_SOUHAITE}\""  | tee -a "${CMD_SYMPA}"
    else
	echo "# PROD, on inscrit à sympa"| tee -a "${CMD_SYMPA}"
	LISTMASTER=$(echo ${sympa_LISTMASTERS} | cut -d',' -f1)
	echo "docker exec -ti sympaServ /usr/lib/sympa/bin/sympa_soap_client.pl --soap_url=${httpProto}://${URL_LISTE}/sympasoap --trusted_application=${sympa_SOAP_USER} --trusted_application_password=${sympa_SOAP_PASSWORD} --proxy_vars=\"USER_EMAIL=${LISTMASTER}\" --service=add --service_parameters=\"${NL_LIST},${EMAIL_SOUHAITE}\""  | tee -a "${CMD_SYMPA}"
	echo "docker exec -ti sympaServ /usr/lib/sympa/bin/sympa_soap_client.pl --soap_url=${httpProto}://${URL_LISTE}/sympasoap --trusted_application=${sympa_SOAP_USER} --trusted_application_password=${sympa_SOAP_PASSWORD} --proxy_vars=\"USER_EMAIL=${LISTMASTER}\" --service=add --service_parameters=\"${NL_LIST},${EMAIL_SECOURS}\""  | tee -a "${CMD_SYMPA}"
    fi

    if [ "${service[ADMIN_ORGA]}" == "O" ]; then
	MESSAGE_MAIL_ORGA_2="${MESSAGE_MAIL_ORGA_2}Comme administrateur de votre organisation, vous pouvez créer des listes de diffusion en vous rendant sur ${httpProto}://${URL_LISTE}"
    fi

    ###################
    # update paheko #
    ###################

    # TODO : problème si 2 comptes partagent le même email souhaité (cela ne devrait pas arriver)
    curl -s "https://${paheko_API_USER}:${paheko_API_PASSWORD}@kaz-paheko.kaz.bzh/api/sql" -d "SELECT numero,nom,quota_disque from users WHERE email='${EMAIL_SOUHAITE}'" | jq '.results[] | .numero,.nom,.quota_disque ' | tr \\n ',' | sed 's/,$/,Aucune\n/' >> "${TEMP_PAHEKO}"

    ####################
    # Inscription MAIL #
    ####################

    if [ "${NB_SERVICES_DEDIES}" != "0" ];then
	MESSAGE_MAIL_ORGA_1="${NL}	dont ${NB_SERVICES_DEDIES} service(s) dédié(s) pour votre organisation:${NL}	${MESSAGE_MAIL_ORGA_1}"
    fi

    if [ -z "${SEND_MSG_CREATE}" ]; then
	# rien de créé => pas de message
	continue
    fi

	#si admin alors msg pour indiquer qu'il faut nous renvoyer ce qu'il souhaite comme service.
	if [ "${service[ADMIN_ORGA]}" == "O" ]; then
		MESSAGE_MAIL_ORGA_3="${MESSAGE_MAIL_ORGA_3}En tant qu'association/famille/société. Vous avez la possibilité d'ouvrir, quand vous le voulez, des services kaz, il vous suffit de nous le demander.

Pourquoi n'ouvrons-nous pas tous les services tout de suite ? parce que nous aimons la sobriété et que nous préservons notre espace disque ;) 
A quoi sert d'avoir un site web si on ne l'utilise pas, n'est-ce pas ? 

Par retour de mail, dites-nous de quoi vous avez besoin tout de suite entre:
* une comptabilité : un service de gestion adhérents/clients 
* un site web de type WordPress
* un cloud : bureau virtuel pour stocker des fichiers/calendriers/contacts et partager avec vos connaissances

Une fois que vous aurez répondu à ce mail, votre demande sera traitée manuellement.
"
	fi

    # on envoie le mail de bienvenue
    MAIL_KAZ="Bonjour,

Bienvenue chez KAZ!

Vous disposez de $((${NB_SERVICES_BASE} + ${NB_SERVICES_DEDIES})) services kaz avec authentification :

* une messagerie classique : ${httpProto}://${URL_WEBMAIL}
* une messagerie instantanée pour discuter au sein d'équipes : ${httpProto}://${URL_AGORA} 

Votre email et identifiant pour tous ces services : ${EMAIL_SOUHAITE}
Le mot de passe : ${PASSWORD}

Pour changer votre mot de passe de messagerie, c'est ici: ${httpProto}://${URL_MDP}
Si vous avez perdu votre mot de passe, c'est ici: ${httpProto}://${URL_MDP}/?action=sendtoken

Vous pouvez accéder à votre messagerie :
* soit depuis votre webmail : ${httpProto}://${URL_WEBMAIL}
* soit depuis votre bureau virtuel : ${httpProto}://${URL_NC}
* soit depuis un client de messagerie comme thunderbird

${MESSAGE_MAIL_ORGA_3}

Vous avez quelques docs intéressantes sur le wiki de kaz:

* Migrer son site internet wordpress vers kaz
https://wiki.kaz.bzh/wordpress/start#migrer_son_site_wordpress_vers_kaz

* Migrer sa messagerie vers kaz
https://wiki.kaz.bzh/messagerie/gmail/start

* Démarrer simplement avec son cloud
https://wiki.kaz.bzh/nextcloud/start

Votre quota est de ${QUOTA}GB. Si vous souhaitez plus de place pour vos fichiers ou la messagerie, faites-nous signe !

Pour accéder à la messagerie instantanée et communiquer avec les membres de votre équipe ou ceux de kaz : ${httpProto}://${URL_AGORA}/login

${MESSAGE_MAIL_ORGA_2}

Enfin, vous disposez de tous les autres services KAZ où l'authentification n'est pas nécessaire : ${httpProto}://${URL_SITE}

En cas de soucis, n'hésitez pas à poser vos questions sur le canal 'Une question ? un soucis' de l'agora dispo ici : ${httpProto}://${URL_AGORA}

Si vous avez besoin d'accompagnement pour votre site, votre cloud, votre compta, votre migration de messagerie,... nous proposons des formations mensuelles gratuites. Si vous souhaitez être accompagné par un professionnel, nous pouvons vous donner une liste de pros, référencés par KAZ.

À bientôt ;)

La collégiale de KAZ. "

    echo "docker exec -i mailServ mailx -a 'Content-Type: text/plain; charset=\"UTF-8\"' -r contact@kaz.bzh -s \"KAZ: confirmation d'inscription\" ${EMAIL_SOUHAITE} ${EMAIL_SECOURS} << EOF
${MAIL_KAZ}
EOF" | tee -a "${CMD_MSG}"

    # on envoie le mail de confirmation d'inscription à contact
    MAIL_KAZ="*****POST AUTOMATIQUE******
Hello,
${NOM} ${PRENOM} vient d'être inscrit avec l'email ${EMAIL_SOUHAITE}
quota             : ${QUOTA}GB

NC_BASE           : ${service[NC_BASE]}
groupe NC base    : ${GROUPE_NC_BASE}
équipe agora base : ${EQUIPE_AGORA}
email de secours  : ${EMAIL_SECOURS}

ORGA              : ${ORGA}
ADMIN_ORGA        : ${service[ADMIN_ORGA]}
NC_ORGA           : ${service[NC_ORGA]}
PAHEKO_ORGA       : ${service[PAHEKO_ORGA]}
WP_ORGA           : ${service[WP_ORGA]}
AGORA_ORGA        : ${service[AGORA_ORGA]}
WIKI_ORGA         : ${service[WIKI_ORGA]}

bisou!"
    echo "docker exec -i mailServ mailx -a 'Content-Type: text/plain; charset=\"UTF-8\"' -r contact@kaz.bzh -s \"KAZ: confirmation d'inscription\" ${EMAIL_CONTACT} << EOF
${MAIL_KAZ}
EOF" | tee -a "${CMD_MSG}"

    echo " # on envoie la confirmation d'inscription sur l'agora " | tee -a "${CMD_MSG}"
    echo "docker exec -ti mattermostServ bin/mmctl post create kaz:Creation-Comptes --message \"${MAIL_KAZ}\"" | tee -a "${CMD_MSG}"

    # fin des inscriptions
done <<< "${ALL_LINES}"

if [[ -n "${ALL_ORGA}" ]]; then
    echo "sleep 2" | tee -a "${CMD_PROXY}"
    echo "${KAZ_BIN_DIR}/container.sh start ${ALL_ORGA}" | tee -a "${CMD_PROXY}"
	for item in "${availableProxyComposes[@]}"; do
    	echo "cd \"${KAZ_COMP_DIR}/${item}/\"; ./proxy-gen.sh; docker-compose up -d; ./reload.sh " | tee -a "${CMD_PROXY}"
    done
fi

###########################
# Lancement des commandes #
###########################
if [ "${SIMULATION}" == "NO" ]; then
    echo "on exécute"
    "${CMD_LOGIN}"
    # on attend qques secondes que le mail soit bien créé avant de continuer (prob de lecture de la BAL : à investiguer)
    # je rallonge à 20s car je vois que le docker sympa ne connait pas toujours l'email kaz créé
    echo "on attend 20s pour que la création des emails soit certaine"
    sleep 20
    "${CMD_SYMPA}"
    "${CMD_ORGA}"
    "${CMD_PROXY}"
    "${CMD_FIRST}"
    "${CMD_INIT}"
    "${CMD_PAHEKO}"
    "${CMD_MSG}"
else
    echo "Aucune commande n'a été lancée : Possibilité de le faire à la main. cf ${KAZ_ROOT}/tmp/${RACINE}_cmds_to_run-*.sh"
fi

# END
