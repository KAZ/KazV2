#/usr/bin/env bash

_gestContainers_completion () {
    KAZ_ROOT=$(cd "$(dirname ${COMP_WORDS[0]})"/..; pwd)
    COMPREPLY=()
    . "${KAZ_ROOT}/bin/.commonFunctions.sh"
    setKazVars
    local cword=${COMP_CWORD} cur=${COMP_WORDS[COMP_CWORD]}
    case "$cur" in
	-*)
	    local proposal="-h --help -n --simu -q --quiet -m --main -M --nas --local -v --version -l --list -cloud -agora -wp -wiki -office -I --install -r -t -exec --optim -occ -u -i -a -U--upgrade -p --post -mmctl"
	    COMPREPLY=( $(compgen -W "${proposal}" -- "${cur}" ) )
	    ;;
	*)
	    # orga name
			local available_orga=$("${KAZ_BIN_DIR}/kazList.sh" "service" "enable" 2>/dev/null)
	    COMPREPLY=($(compgen -W "${available_orga}" -- "${cur}"))
	    ;;
    esac
    return 0
}
complete -F _gestContainers_completion gestContainers.sh
