#!/bin/bash

for cloud in $(docker ps | grep -i nextcloudServ |awk '{print $12}')
do
	docker exec -u www-data $cloud php cron.php

done
