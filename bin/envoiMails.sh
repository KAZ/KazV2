#!/bin/bash
 
#kan: 09/09/2022
#koi: envoyer un mail à une liste (sans utiliser sympa sur listes.kaz.bzh)
#ki : fab

#on récupère toutes les variables et mdp
# on prend comme source des repertoire le dossier du dessus ( /kaz dans notre cas )
KAZ_ROOT=$(cd "$(dirname $0)"/..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

SIMULATION=NO

CMD="/tmp/envoiMail_cmds_to_run.sh"
echo "#!/bin/bash" > ${CMD} && chmod +x ${CMD}

#################################################################################################################
MAIL_KAZ="
KAZ, association morbihannaise, propose de \"dégoogliser\" l’internet avec des solutions et services numériques libres alternatifs à ceux des GAFAM. Elle invite les habitants et les élus de Vannes et de sa région à une réunion publique d’information et d’échange :
Le jeudi 22 septembre 2022 à 18 heures
à la Maison des associations, 31 rue Guillaume Le Bartz à Vannes.

Cette invitation est destinée à toute personne sensible aux enjeux du numérique, aux risques pour la démocratie et les libertés, à sa participation au dérèglement climatique et à l’épuisement des ressources de notre planète.

Nous dirons qui nous sommes, quelles sont nos valeurs et quels sont concrètement les solutions et services que nous proposons, leurs conditions d’accès et l’accompagnement utile pour leur prise en main.

Les premières pierres de KAZ ont été posées voilà bientôt deux ans, en pleine pandémie de la COVID, par sept citoyens qui ont voulu répondre à l’appel de Framasoft de dégoogliser l’internet. Ne plus se lamenter sur la puissance des GAFAM, mais proposer des solutions et des services numériques alternatifs à ceux de Google, Amazon, Facebook, Apple et Microsoft en s’appuyant sur les fondamentaux Sobre, Libre, Éthique et Local.

A ce jour, près de 200 particuliers ont ouvert une adresse @kaz.bz, plus de 80 organisations ont souscrit au bouquet de services de KAZ et près de 800 collaborateurs d’organisations utilisatrices des services de KAZ participent peu ou prou au réseau de KAZ.
Beaucoup de services sont gratuits et accessibles sur le site https://kaz.bzh. D’autres demandent l’ouverture d’un compte moyennant une petite participation financière de 10€ par an pour les particuliers et de 30€ par an pour les organisations. Ceci est permis par le bénévolat des membres de la collégiale qui dirigent l’association et administrent les serveurs et les services.

A ce stade, de nombreuses questions se posent à KAZ. Quelle ambition de couverture de ses services auprès des particuliers et des organisations sur son territoire, le Morbihan ? Quel accompagnement dans la prise en main des outils ? Quels nouveaux services seraient utiles ?

Nous serions heureux de votre participation à notre réunion publique du 22 septembre d'une durée totale de 2h : 
  * Présentation valeurs / contexte (15mn)
  * Présentation outils (45mn)
  * Questions / Réponses (1h)

En restant à votre disposition,

La Collégiale de KAZ
"
#################################################################################################################

FIC_MAILS="/tmp/fic_mails"

while read EMAIL_CIBLE
do
     
  echo "docker exec -i mailServ mailx -a 'Content-Type: text/plain; charset=\"UTF-8\"' -r contact@kaz.bzh -s \"Invitation rencontre KAZ jeudi 22 septembre à 18h00 à la Maison des assos à Vannes\" ${EMAIL_CIBLE} << EOF
${MAIL_KAZ} 
EOF" | tee -a ${CMD}

  echo "sleep 2" | tee -a ${CMD}

done < ${FIC_MAILS} 

# des commandes à lancer ?
if [ "${SIMULATION}" == "NO" ];then
  echo "on exécute"
  ${CMD}
else
  echo "Aucune commande n'a été lancée: Possibilité de le faire à la main. cf ${CMD}"
fi

