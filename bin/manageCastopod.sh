#!/bin/bash
# Script de manipulation d'un wordpress'
# init /versions / restart ...
#

KAZ_ROOT=$(cd "$(dirname $0)"/..; pwd)
. $KAZ_ROOT/bin/.commonFunctions.sh
setKazVars
. $DOCKERS_ENV
. $KAZ_ROOT/secret/SetAllPass.sh

#GLOBAL VARS
PRG=$(basename $0)

availableOrga=($(getList "${KAZ_CONF_DIR}/container-orga.list"))
AVAILABLE_ORGAS=${availableOrga[*]//-orga/}

QUIET="1"
ONNAS=


CASTOPOD_COMMUN="OUI_PAR_DEFAUT"
DockerServName=${castopodServName}

declare -A Posts

usage() {
echo "${PRG} [OPTION] [COMMANDES] [ORGA]
Manipulation d'un castopod

OPTIONS 
 -h|--help          Cette aide :-)
 -n|--simu          SIMULATION
 -q|--quiet         On ne parle pas (utile avec le -n pour avoir que les commandes)
 --nas              L'orga se trouve sur le NAS !

COMMANDES (on peut en mettre plusieurs dans l'ordre souhaité)
 -I|--install       L'initialisation du castopod
 -v|--version       Donne la version du castopod et signale les MàJ

ORGA                parmi : ${AVAILABLE_ORGAS}
                    ou vide si castopod commun
"
}



Init(){
    
    POD_URL="${httpProto}://${ORGA}-${castopodHost}.${domain}"
    [ -n "${CASTOPOD_COMMUN}" ] && POD_URL="${httpProto}://${castopodHost}.${domain}"

    if ! [[ "$(docker ps -f name=${DockerServName} | grep -w ${DockerServName})" ]]; then
        printKazError "Castopod not running... abort"
        exit
    fi

    echo "\n  *** Premier lancement de Castopod" >& $QUIET

    ${SIMU} waitUrl "${POD_URL}"
    CI_SESSION=$(echo ${headers} | grep "ci_session" | sed "s/.*ci_session=//")

    cookies=$(curl -c - ${POD_URL})    
    CSRF_TOKEN=$(curl --cookie <(echo "$cookies") ${POD_URL}/cp-install | grep "csrf_test_name" | sed "s/.*value=.//" | sed "s/.>//") 

    #echo ${CSRF_TOKEN}
    ${SIMU} curl --cookie <(echo "$cookies") -X POST \
        -d "username=${castopod_ADMIN_USER}" \
        -d "password=${castopod_ADMIN_PASSWORD}" \
        -d "email=${castopod_ADMIN_MAIL}" \
        -d "csrf_test_name=${CSRF_TOKEN}" \
        "${POD_URL}/cp-install/create-superadmin"

}

Version(){
    VERSION="TODO"
    echo "Version $DockerServName : ${GREEN}${VERSION}${NC}"
}



########## Main #################
for ARG in "$@"; do 
    case "${ARG}" in
        '-h' | '--help' )
        usage && exit ;;
        '-n' | '--simu')
        SIMU="echo" ;;
        '-q' )
        QUIET="/dev/null" ;;
        '--nas' | '-nas' )
        ONNAS="SURNAS" ;; 
        '-v' | '--version')
        COMMANDS="$(echo "${COMMANDS} VERSION" | sed "s/\s/\n/g" | sort | uniq)" ;; 
        '-I' | '--install' )
        COMMANDS="$(echo "${COMMANDS} INIT" | sed "s/\s/\n/g" | sort | uniq)" ;; # le sed sort uniq, c'est pour pas l'avoir en double
        '-*' ) # ignore
        ;; 
        *)
        ORGA="${ARG%-orga}"
        DockerServName="${ORGA}-${castopodServName}"
        CASTOPOD_COMMUN=
        ;;
    esac
done

if [ -z "${COMMANDS}" ]; then usage && exit ; fi

for COMMAND in ${COMMANDS}; do
    case "${COMMAND}" in
        'VERSION' )
         Version && exit ;;
        'INIT' )
         Init ;;
    esac
done
