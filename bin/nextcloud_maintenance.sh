#!/bin/bash
#on récupère toutes les variables et mdp
KAZ_ROOT=/kaz
. $KAZ_ROOT/bin/.commonFunctions.sh
setKazVars
. $DOCKERS_ENV
. $KAZ_ROOT/secret/SetAllPass.sh

URL_AGORA=https://$matterHost.$domain/api/v4
EQUIPE=kaz

PostMattermost() {
        PostM=$1
        CHANNEL=$2
        TEAMID=$(curl -s -H "Authorization: Bearer ${mattermost_token}" "${URL_AGORA}/teams/name/${EQUIPE}" | jq .id | sed -e 's/"//g')
        CHANNELID=$(curl -s -H "Authorization: Bearer ${mattermost_token}" ${URL_AGORA}/teams/${TEAMID}/channels/name/${CHANNEL} | jq .id | sed -e 's/"//g')
        curl -s i-X POST -i -H "Authorization: Bearer ${mattermost_token}" -d "{\"channel_id\":\"${CHANNELID}\",\"message\":\"${PostM}\"}" "${URL_AGORA}/posts" >/dev/null 2>&1
}

LISTEORGA=$(ls -F1 /var/lib/docker/volumes/ | grep cloudData | sed -e 's/^orga_//g' -e 's/-cloudData\///g')
for CLOUD in ${LISTEORGA}
do
	/kaz/bin/gestContainers.sh -cloud -occ "maintenance:mode" ${CLOUD} | grep -i enable && PostMattermost "ERREUR : Le cloud ${CLOUD} sur ${site} est en mode maintenance" "Sysadmin-alertes"
done	
