#!/bin/bash
# 23/04/2021
# script de mise a jour du fichier de collecte pour future intégration dans la base de donneyyy
# did

KAZ_ROOT=$(cd $(dirname $0)/..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

FIC_COLLECTE=${KAZ_STATE_DIR}/collecte.csv
FIC_ACTIVITE_MAILBOX=${KAZ_STATE_DIR}/activites_mailbox.csv

mkdir -p ${KAZ_STATE_DIR}
mkdir -p ${KAZ_STATE_DIR}/metro

#Jirafeau
echo "$(date +%Y-%m-%d-%H-%M-%S);" \
     "depot-count;" \
     "$(find ${DOCK_VOL}/jirafeau_fileData/_data/files/ -name \*count| wc -l)" >> "${FIC_COLLECTE}"
echo "$(date +%Y-%m-%d-%H-%M-%S);" \
     "depot-size;" \
     "$(du -ks ${DOCK_VOL}/jirafeau_fileData/_data/files/ | awk -F " " '{print $1}')" >> "${FIC_COLLECTE}"

#PLACE DISQUE sur serveur
echo "$(date +%Y-%m-%d-%H-%M-%S);" \
     "disk-system-size-used;" \
     "$(df | grep sda | awk -F " " '{print $3}')" >> "${FIC_COLLECTE}"
echo "$(date +%Y-%m-%d-%H-%M-%S);" \
     "disk-system-size-used-human;" \
     "$(df -h | grep sda | awk -F " " '{print $3}')" >> "${FIC_COLLECTE}"

#nombre de mails kaz:
echo "$(date +%Y-%m-%d-%H-%M-%S);" \
     "mailboxes;" \
     "$(cat ${KAZ_COMP_DIR}/postfix/config/postfix-accounts.cf | wc -l)" >> "${FIC_COLLECTE}"

#nombre d'alias kaz:
echo "$(date +%Y-%m-%d-%H-%M-%S);" \
     "mail_alias;" \
     "$(cat ${KAZ_COMP_DIR}/postfix/config/postfix-virtual.cf  | wc -l)" >> "${FIC_COLLECTE}"
#Nombre d' orgas
echo "$(date +%Y-%m-%d-%H-%M-%S);" \
     "Orgas;" \
     "$(ls -l /kaz/dockers/ | grep orga | wc -l)" >> "${FIC_COLLECTE}"

#stats des 2 postfix (mail+sympa)
EXP=$(/usr/bin/hostname -s)

STATS1=$(cat ${DOCK_VOL}/sympa_sympaLog/_data/mail.log.1 | /usr/sbin/pflogsumm)
#docker exec -i mailServ mailx -r $EXP -s "stats Sympa" root <<DEB_MESS
#$STATS1
#DEB_MESS

STATS2=$(cat ${DOCK_VOL}/postfix_mailLog/_data/mail.log | /usr/sbin/pflogsumm)
#docker exec -i mailServ mailx -r $EXP -s "stats Postfix" root <<DEB_MESS
#$STATS2
#DEB_MESS

IFS=''
for line in $(ls -lt --time-style=long-iso "${DOCK_VOL}/postfix_mailData/_data/kaz.bzh/"); do
    echo "${line}" | awk '{print $6";"$7";"$8";"$9}' > "${FIC_ACTIVITE_MAILBOX}"
done

#pour pister les fuites mémoires
docker stats --no-stream --format "table {{.Name}}\t{{.Container}}\t{{.MemUsage}}" | sort -k 3 -h  > "${KAZ_STATE_DIR}/metro/$(date +"%Y%m%d")_docker_memory_kaz.log"
ps aux --sort -rss > "${KAZ_STATE_DIR}/metro/$(date +"%Y%m%d")_ps_kaz.log"
free -hlt > "${KAZ_STATE_DIR}/metro/$(date +"%Y%m%d")_mem_kaz.log"
systemd-cgls --no-pager > "${KAZ_STATE_DIR}/metro/$(date +"%Y%m%d")_cgls_kaz.log"
for i in $(docker container ls --format "{{.ID}}"); do docker inspect -f '{{.State.Pid}} {{.Name}}' $i; done > "${KAZ_STATE_DIR}/metro/$(date +"%Y%m%d")_dockerpid_kaz.log"

#on piste cette saloperie d'ethercalc
#echo $(date +"%Y%m%d") >> "${KAZ_STATE_DIR}/metro/docker_stats_ethercalc.log"
#docker stats --no-stream ethercalcServ ethercalcDB >> "${KAZ_STATE_DIR}/metro/docker_stats_ethercalc.log"

#fab le 04/10/2022
#taille des dockers
docker system df -v > "${KAZ_STATE_DIR}/metro/$(date +"%Y%m%d")_docker_size_kaz.log"
