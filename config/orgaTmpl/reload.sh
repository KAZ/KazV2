#!/bin/bash

PRG=$(basename $0)
KAZ_ROOT=$(cd "$(dirname $0)/../.."; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars
. "${DOCKERS_ENV}"

cd $(dirname $0)
PWD=$(pwd)
ORGA_DIR=$(basename ${PWD})
ORGA=${ORGA_DIR%-orga}


${KAZ_BIN_DIR}/manageWiki.sh --reload $ORGA