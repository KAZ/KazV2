#!/bin/bash

#rq fab le 08/08/2024 : ne sert plus à rien à priori

KAZ_ROOT=$(cd $(dirname $0)/../..; pwd)
. "${KAZ_ROOT}/bin/.commonFunctions.sh"
setKazVars

. "${DOCKERS_ENV}"

cd $(dirname $0)
PWD=$(pwd)
ORGA_DIR=$(basename ${PWD})

if [[ "${ORGA_DIR}" != *"-orga" ]]
then
    printKazError "it's not an orga dir"
    exit
fi
ORGA=${ORGA_DIR%-orga}
if [[ -z "${ORGA}" ]]
then
    printKazError "it's not an orga dir"
    exit
fi

printKazMsg "init paheko ${ORGA}"

#${KAZ_COMP_DIR}/paheko/installPlugins.sh ${ORGA}
