#!/bin/bash

#docker network create postfix_mailNet

#{{db
docker volume create --name=orga_${orga}orgaDB
#}}
#{{agora
docker volume create --name=orga_${orga}matterConfig
docker volume create --name=orga_${orga}matterData
docker volume create --name=orga_${orga}matterLogs
docker volume create --name=orga_${orga}matterPlugins
docker volume create --name=orga_${orga}matterClientPlugins
docker volume create --name=matterIcons
#}}
#{{cloud
docker volume create --name=orga_${orga}cloudMain
docker volume create --name=orga_${orga}cloudData
docker volume create --name=orga_${orga}cloudConfig
docker volume create --name=orga_${orga}cloudApps
docker volume create --name=orga_${orga}cloudCustomApps
docker volume create --name=orga_${orga}cloudThemes
docker volume create --name=orga_${orga}cloudPhp
chown 33:33 /var/lib/docker/volumes/orga_${orga}cloud*/_data
#}}
#{{wiki
docker volume create --name=orga_${orga}wikiData
docker volume create --name=orga_${orga}wikiConf
docker volume create --name=orga_${orga}wikiPlugins
docker volume create --name=orga_${orga}wikiLibtpl
docker volume create --name=orga_${orga}wikiLogs
#}}
#{{wp
docker volume create --name=orga_${orga}wordpress
#}}
#{{castopod
docker volume create --name=orga_${orga}castopodCache
docker volume create --name=orga_${orga}castopodMedia
#}}
